<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'DashboardController@index');


use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function(){
	Route::get('/', 'AdminController@index')->name('dashboard');
	Route::get('/getadd', 'AdminController@getadd')->name('getadd');
	Route::post('/add', 'AdminController@add')->name('add');


});

Route::group([
	'namespace' =>'Admin',
	'prefix'=>'admin',
	'as'	=> 'admin.',
	'middleware'=>['web','checkAdmin']
], function(){
    $website_language = session()->get('website_language');
    if ($website_language == null){
        session()->put('website_language', 'vi');
    }
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('blogs', 'BlogController');
    Route::resource('topics', 'TopicController');
    Route::resource('tags', 'TagController');
    Route::resource('blog_banner', 'BlogBannerController');
    Route::post('/hot_news', 'BlogController@hotNews')->name('hotNews');
    Route::post('/active', 'BlogBannerController@active')->name('activeBanner');
	//package
	Route::group([
		'prefix'=>'package',
		'as'	=> 'package.'

	], function(){
		Route::get('/', 'PackagesController@showDataPackage')->name('showPackage');
		Route::get('/viewPackage/{id}', 'PackagesController@showDetailPackage')->name('detailPackage');
		Route::get('/addPackage', 'PackagesController@showFormAddPackage')->name('showAddPackage');
		Route::post('/postPackage', 'PackagesController@addPackage')->name('addPackage');
		Route::get('/editPackage/{id}', 'PackagesController@showFormEditPackage')->name('ShowEditPackage');
		Route::post('/update/{id}', 'PackagesController@update')->name('update');
		Route::get('/destroy/{id}', 'PackagesController@destroy')->name('destroy');
	});

	//Service
	Route::group([
		'as'	=> 'service.',
		'prefix' => 'service',
	], function(){
		Route::get('/', 'ServicesController@index')->name('dashboard');
		Route::get('/edit/{id}', 'ServicesController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'ServicesController@Update')->name('update');
		Route::get('/addservice', 'ServicesController@addService')->name('addService');
		Route::post('/add', 'ServicesController@add')->name('add');
		Route::get('/destroy/{id}', 'ServicesController@destroy')->name('destroy');
		Route::get('/show/{id}', 'ServicesController@show')->name('show');
	});


	//services detail
	Route::group([
		'as'	=> 'about_service.',
		'prefix' => 'about_service',
	], function(){
		Route::get('/', 'AboutServiceController@index')->name('dashboard');
		Route::get('/edit/{id}', 'AboutServiceController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'AboutServiceController@Update')->name('update');
		Route::get('/showadd', 'AboutServiceController@showAdd')->name('showadd');
		Route::post('/add', 'AboutServiceController@add')->name('add');
		Route::get('/destroy/{id}', 'AboutServiceController@destroy')->name('destroy');
		Route::get('/show/{id}', 'AboutServiceController@show')->name('show');
	});

	//services detail
	Route::group([
		'as'	=> 'services_detail.',
		'prefix' => 'services_detail',
	], function(){
		Route::get('/', 'Services_detailController@index')->name('dashboard');
		Route::get('/edit/{id}', 'Services_detailController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'Services_detailController@Update')->name('update');
		Route::get('/addservices_detail/{id}', 'Services_detailController@addServices_detail')->name('addServices_detail');
		Route::post('/add/{id}', 'Services_detailController@add')->name('add');
		Route::get('/destroy/{id}', 'Services_detailController@destroy')->name('destroy');
		Route::get('/show/{id}', 'Services_detailController@show')->name('show');
		Route::get('/list/{id}', 'Services_detailController@list')->name('list');
	});

	//User
	Route::group([
		'as'	=>'user.',
		'prefix'=>'user',
	], function(){
		Route::get('/', 'UserController@index')->name('dashboard');
		Route::get('/addUser', 'UserController@showFormAddUser')->name('showAddUser');
		Route::post('/postUser', 'UserController@addUser')->name('addUser');
		Route::get('/edit/{id}', 'UserController@showFormEditUser')->name('edit');
		Route::post('/update{id}', 'UserController@Update')->name('update');
		Route::get('/destroy/{id}', 'UserController@destroy')->name('destroy');
	});

	//about
	Route::group([
		'as'	=> 'about.',
		'prefix' => 'about',
	], function(){
		Route::get('/', 'AboutController@index')->name('dashboard');
		Route::get('/show/{id}', 'AboutController@show')->name('show');
		Route::get('/edit/{id}', 'AboutController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'AboutController@Update')->name('update');
		Route::get('/addabouts', 'AboutController@addAbout')->name('addAbout');
		Route::post('/add', 'AboutController@add')->name('add');
		Route::get('/destroy/{id}', 'AboutController@destroy')->name('destroy');
	});

	//slide
	Route::group([
		'as'	=> 'slide.',
		'prefix' => 'slide',
	], function(){
		Route::get('/', 'SlideController@index')->name('dashboard');
		Route::get('/edit/{id}', 'SlideController@showFormEditSlide')->name('edit');
		Route::post('/update{id}', 'SlideController@UpdateSlide')->name('update');
		Route::get('/addSlide', 'SlideController@addSlide')->name('addSlide');
		Route::post('/add', 'SlideController@add')->name('add');
		Route::get('/destroy/{id}', 'SlideController@destroy')->name('destroy');
	});



	Route::group([
		'as'	=> 'partner.',
		'prefix' => 'partner',
	], function(){
		Route::get('/', 'PartnerController@index')->name('dashboard');
		Route::get('/edit/{id}', 'PartnerController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'PartnerController@Update')->name('update');
		Route::get('/addpartner', 'PartnerController@addAbout')->name('addPartner');
		Route::post('/add', 'PartnerController@add')->name('add');
		Route::get('/destroy/{id}', 'PartnerController@destroy')->name('destroy');
		Route::get('/show/{id}', 'PartnerController@show')->name('show');
	});

	Route::group([
		'as'	=> 'ourteam.',
		'prefix' => 'ourteam',
	], function(){
		Route::get('/', 'OurteamController@index')->name('dashboard');
		Route::get('/edit/{id}', 'OurteamController@showFormEdit')->name('edit');
		Route::post('/update/{id}', 'OurteamController@Update')->name('update');
		Route::get('/addourteam', 'OurteamController@addAbout')->name('addOurteam');
		Route::post('/add', 'OurteamController@add')->name('add');
		Route::get('/destroy/{id}', 'OurteamController@destroy')->name('destroy');
		Route::get('/show/{id}', 'OurteamController@show')->name('show');
	});


	//Menu
	Route::group([
		'as'	=>'menu.',
		'prefix'=>'menu',
	], function(){
		Route::get('/', 'MenuController@index')->name('dashboard');
		Route::get('/addMenu', 'MenuController@showFormAddMenu')->name('showAddMenu');
		Route::post('/postMenu', 'MenuController@addMenu')->name('addMenu');
		Route::get('/edit/{id}', 'MenuController@showFormEditMenu')->name('edit');
		Route::post('/update{id}', 'MenuController@Update')->name('update');
		Route::get('/destroy/{id}', 'MenuController@destroy')->name('destroy');
	});

	//news
	Route::group([
		'as'	=> 'news.',
		'prefix' => 'news',
	], function(){
		Route::get('/', 'NewsController@index')->name('dashboard');
		Route::get('/edit/{id}', 'NewsController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'NewsController@Update')->name('update');
		Route::get('/addnews', 'NewsController@addNews')->name('addNews');
		Route::post('/add', 'NewsController@add')->name('add');
		Route::get('/destroy/{id}', 'NewsController@destroy')->name('destroy');
		Route::get('/show/{id}', 'NewsController@show')->name('show');
	});

	//Library
		Route::group([
		'as'	=> 'library.',
		'prefix' => 'library',
	], function(){
		Route::get('/', 'LibraryController@index')->name('dashboard');
		Route::get('/edit/{id}', 'LibraryController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'LibraryController@Update')->name('update');
		Route::get('/addlibrary', 'LibraryController@addLibrary')->name('addLibrary');
		Route::post('/add', 'LibraryController@add')->name('add');
		Route::get('/destroy/{id}', 'LibraryController@destroy')->name('destroy');
		Route::get('/show/{id}', 'LibraryController@show')->name('show');
		Route::get('/download/{id}', 'LibraryController@getDownload')->name('download');
	});

	//
	Route::group([
		'as'	=>'aboutpackage.',
		'prefix'=>'aboutpackage',
	], function(){
		Route::get('/', 'AboutPackageController@index')->name('dashboard');
		Route::get('/addaboutpackage', 'AboutPackageController@showFormAdd')->name('showAdd');
		Route::post('/postaboutpackage', 'AboutPackageController@add')->name('add');
		Route::get('/edit/{id}', 'AboutPackageController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'AboutPackageController@Update')->name('update');
		Route::get('/destroy/{id}', 'AboutPackageController@destroy')->name('destroy');
	});
	Route::group([
		'as'	=>'customercomment.',
		'prefix'=>'customercomment',
	], function(){
		Route::get('/', 'CustomerCommentController@index')->name('dashboard');
		Route::get('/addcustomercomment', 'CustomerCommentController@showFormAdd')->name('showAdd');
		Route::post('/postcustomercomment', 'CustomerCommentController@add')->name('add');
		Route::get('/edit/{id}', 'CustomerCommentController@showFormEdit')->name('edit');
		Route::post('/update{id}', 'CustomerCommentController@Update')->name('update');
		Route::get('/destroy/{id}', 'CustomerCommentController@destroy')->name('destroy');
	});
});


//login
Route::group([
	'namespace' => 'Admin',
	'as'	=> 'login.',
	'prefix'=> 'login',
	// 'middleware' => ['FormLogin'],
], function(){
	Route::get('/', 'AdminLoginController@showlogin')->name('showlogin');
	Route::post('/login', 'AdminLoginController@login')->name('login');
	Route::any('logout', 'AdminLoginController@logout')->name('logout');
});
//end_login




Route::group([
	'middleware' => 'locale',
	'namespace' =>'Customer',
	'prefix'=>'',
	'as'	=> 'customer.',

], function(){

	Route::get('/', 'DashboardController@index')->name('dashboard')->middleware();

	// //About
	Route::group([
		'prefix'=>'',
		'as'	=> 'about.',

	], function(){
		Route::get('about', 'AboutController@showDataAbout')->name('showAbout');
	});

	//services
	Route::group([
		'prefix'=>'',
		'as'	=> 'services.',

	], function(){
		Route::get('/services', 'AboutController@showDataServices')->name('showServices');
	});

	//packages
	Route::group([
		'prefix'=>'',
		'as'	=> 'packages.',

	], function(){
		Route::get('/packages', 'AboutController@showDataPackages')->name('showPackages');
	});

	//partners
	Route::group([
		'prefix'=>'',
		'as'	=> 'partners.',

	], function(){
		Route::get('/partners', 'AboutController@showDataPartners')->name('showPartners');
		Route::get('/partners_detail', 'AboutController@showDataPartners_detail')->name('showPartners_detail');
	});
	//partners_detail

	Route::group([
		'prefix'=>'',
		'as'	=> 'about.',

	], function(){
		Route::get('/about_detail', 'AboutController@showDataAbout_detail')->name('showAbout_detail');
	});

	//Procedure
	Route::group([
		'prefix'=>'',
		'as'	=> 'procedure.',

	], function(){
		Route::get('/procedure', 'AboutController@showDataProcedure')->name('showProcedure');
	});

	//library
	Route::group([
		'prefix'=>'',
		'as'	=> 'library.',

	], function(){
		Route::get('/library', 'AboutController@showDataLibrary')->name('showLibrary');
		Route::get('/download/{id}', 'AboutController@getDownload')->name('download');
	});
	//library
	Route::group([
		'prefix'=>'',
		'as'	=> 'faqs.',

	], function(){
		Route::get('/faqs', 'AboutController@showDataFaqs')->name('showFaqs');
	});

	//news
	Route::group([
		'prefix'=>'',
		'as'	=> 'news.',

	], function(){
		Route::get('/news', 'AboutController@showDataNews')->name('showNews');
	});


	Route::group([
		'prefix'=>'',
		'as'	=> 'footer.',

	], function(){
		Route::get('/footer', 'AboutController@showDataFooter')->name('showFooter');
	});

    Route::get('/contact', 'AboutController@showContact')->name('showContact');

    Route::post('/contact', 'AboutController@sendContact');

    Route::get('/bemer', 'AboutController@showBemer');
    Route::get('/dr.lodi', 'AboutController@showDrLodi');
    Route::get('/dr.naim', 'AboutController@showDrNaim');
    Route::get('/blogs', 'BlogCustomerController@list')->name('listBlog');
    Route::get('/blogs/{id}', 'BlogCustomerController@detail')->name('detailBlog');

});
Route::group([
	'namespace'=>'Customer',
	'middleware' => 'locale',

],
	function() {
	Route::get('change-language/{language}', 'DashboardController@changeLanguage')
	->name('user.change-language');
});
