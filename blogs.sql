ALTER TABLE `news` ADD PRIMARY KEY(`id`);
ALTER TABLE `news` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `news` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
UPDATE `menus` SET `name` = 'BLOG', `links` = '/blogs', `updated_at` = NULL WHERE `menus`.`id` = 41;
UPDATE `menus` SET `name` = 'BLOG', `links` = '/blogs', `updated_at` = NULL WHERE `menus`.`id` = 35;


CREATE TABLE `heath_care`.`blogs` (
`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`title` VARCHAR(255) NOT NULL ,
`sumary` TEXT NULL ,
`image_preview` TEXT NULL ,
`topic_id` INT(11) NOT NULL ,
`content` TEXT NOT NULL ,
`view` INT(11) NOT NULL DEFAULT '0',
`hot_new` TINYINT(1) NOT NULL DEFAULT '0',
`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
`updated_at` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `heath_care`.`topics` (
`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name` VARCHAR(50) NOT NULL ,
`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
`updated_at` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `heath_care`.`tags` (
`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name` VARCHAR(50) NOT NULL ,
`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
`updated_at` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `heath_care`.`blog_tag` (
`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`blog_id` INT(11) NOT NULL ,
`tag_id` INT(11) NOT NULL ,
`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
`updated_at` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `heath_care`.`blog_banner` (
`id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
`content` TEXT NOT NULL ,
`status` TINYINT(2) NOT NULL DEFAULT '0' ,
`created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
`updated_at` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
