-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 27, 2019 lúc 07:21 AM
-- Phiên bản máy phục vụ: 10.4.8-MariaDB
-- Phiên bản PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `healthcare`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `links` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuid` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `content`, `image`, `links`, `menuid`, `created_at`, `updated_at`) VALUES
(6, 'Tầm nhìn - sứ mệnh - giá trị cốt lõi', '<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"/ckfinder/userfiles/images/triet-li-kinh-doanh.png\" style=\"height:980px; width:870px\" /></p>\r\n\r\n<p>&nbsp;</p>', '1.3_Tam nhin su menh.jpg', 'ssssssssss', 23, '2019-10-28 19:19:47', NULL),
(8, 'Tại sao lựa chọn IHC ?', '', '', 'tai-sao', 23, NULL, NULL),
(9, 'Đội ngũ', '', '', 'doi-ngu', 23, NULL, NULL),
(10, 'Cảm nhận của khách hàng', '', '', 'cam-nhan', 23, '2019-11-03 17:00:00', NULL),
(16, 'Những điều cần lưu ý', '<p>Newton interprets 2D composition layers as rigid bodies interacting in a real environment. Newton provides many simulation controllers such as body properties (type, density, friction, bounciness, velocity, etc), global properties (gravity, solver), and allows the creation of joints between bodies. Once simulation is completed, animation is recreated in After Effects with standard keyframes.</p>', '5.5_Hinh anh.jpg', '123456', 23, '2019-11-07 08:48:09', NULL),
(18, 'Cảm nhận của Khách hàng', '<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', '5.2_Tu van suc khoe.jpg', '123456', 23, '2019-11-18 09:28:47', NULL),
(24, 'Kinh nghiệm của chúng tôi', '<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>', '1.2_Cam ket cua chung toi.jpg', 'kinhnghiemcuachungtoi', 23, '2019-11-22 07:30:00', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `about_packages`
--

CREATE TABLE `about_packages` (
  `id` int(11) NOT NULL,
  `packagesid` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `about_packages`
--

INSERT INTO `about_packages` (`id`, `packagesid`, `name`, `banner`, `created_at`, `updated_at`) VALUES
(21, 63, 'Gói trị liệu Campuchia', '2.1 Goi chua benh tai Nhat Ban.jpg', '2019-11-22 06:50:41', NULL),
(36, 66, 'Gói trị liệu tại Nhật Bản', '2.3 Goi chua benh tai Thai Lan.jpg', '2019-11-22 09:57:23', NULL),
(39, 61, 'Gói chữa bệnh tại Thụy Sỹ', '2.1 Goi chua benh tai Thuy Sy.jpg', '2019-11-22 09:57:07', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banners`
--

INSERT INTO `banners` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(5, 'Gioi Thieu', 'sv.png', '2019-10-21 20:46:13', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_comments`
--

CREATE TABLE `customer_comments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer_comments`
--

INSERT INTO `customer_comments` (`id`, `name`, `position`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Trần Ngọc Hiếu', 'Giám đốc chánh án của làng xóm sông', '<p>Tôi năm nay 56t, có tiền sử tiểu đường, axit uric, men gan và mỡ máu cao. Tôi là một người sinh hoạt lành mạnh và rất quan tâm đến sức khỏe. Tôi thường xuyên kiểm tra, làm các xét nghiệm, khám sức khỏe định kỳ và uống thuốc đầy đủ theo chỉ định của bác sĩ. Tuy nhiên, do tính chất công việc, tôi vẫn phải uống rượu và đi công tác xa nên tôi cảm nhận thấy các chỉ số máu của mình vẫn không được cải thiện theo phác đồ điều trị hiện tại.&nbsp;</p>', '2019-11-22 02:47:11', NULL),
(10, 'Lương Sơn Bá', 'Giám đốc Công nghệ thông tin', '<p>Tôi năm nay 56t, có tiền sử tiểu đường, axit uric, men gan và mỡ máu cao. Tôi là một người sinh hoạt lành mạnh và rất quan tâm đến sức khỏe. Tôi thường xuyên kiểm tra, làm các xét nghiệm, khám sức khỏe định kỳ và uống thuốc đầy đủ theo chỉ định của bác sĩ. Tuy nhiên, do tính chất công việc, tôi vẫn phải uống rượu và đi công tác xa nên tôi cảm nhận thấy các chỉ số máu của mình vẫn không được cải thiện theo phác đồ điều trị hiện tại.</p>', '2019-11-04 04:38:33', NULL),
(11, 'nguyen', 'Giám đốc Nasa', '<p>Tôi năm nay 56t, có tiền sử tiểu đường, axit uric, men gan và mỡ máu cao. Tôi là một người sinh hoạt lành mạnh và rất quan tâm đến sức khỏe. Tôi thường xuyên kiểm tra, làm các xét nghiệm, khám sức khỏe định kỳ và uống thuốc đầy đủ theo chỉ định của bác sĩ. Tuy nhiên, do tính chất công việc, tôi vẫn phải uống rượu và đi công tác xa nên tôi cảm nhận thấy các chỉ số máu của mình vẫn không được cải thiện theo phác đồ điều trị hiện tại.</p>', '2019-10-31 23:50:06', '2019-10-31 23:50:06'),
(12, 'Vũ Đức Nguyên', 'Giám đốc Amazon', '<p>Tôi năm nay 56t, có tiền sử tiểu đường, axit uric, men gan và mỡ máu cao. Tôi là một người sinh hoạt lành mạnh và rất quan tâm đến sức khỏe. Tôi thường xuyên kiểm tra, làm các xét nghiệm, khám sức khỏe định kỳ và uống thuốc đầy đủ theo chỉ định của bác sĩ. Tuy nhiên, do tính chất công việc, tôi vẫn phải uống rượu và đi công tác xa nên tôi cảm nhận thấy các chỉ số máu của mình vẫn không được cải thiện theo phác đồ điều trị hiện tại.</p>', '2019-10-31 23:50:51', '2019-10-31 23:50:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `links` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `name`, `links`, `image`, `created_at`, `updated_at`) VALUES
(23, 'Giới thiệu', '/customer/about/about', 'Gioi thieu.jpg', '2019-11-22 02:29:18', NULL),
(29, 'Dịch vụ', '/customer/services/services', 'Dich vu.jpg', '2019-10-28 01:43:48', NULL),
(30, 'Gói trị liệu', '/customer/packages/packages', 'Goi tri lieu.jpg', '2019-10-28 01:44:06', NULL),
(34, 'Đối tác', '/customer/partners/partners', 'Doi tac.jpg', '2019-10-28 01:44:21', NULL),
(35, 'Thủ tục', '/customer/procedure/procedure', 'Thu tuc.jpg', '2019-10-28 01:44:47', NULL),
(36, 'Thư viện', '/customer/library/library', 'Thu vien.jpg', '2019-11-22 02:23:02', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_18_085232_create_abouts_table', 1),
(4, '2019_10_18_091215_create_banners_table', 1),
(5, '2019_10_18_091407_create_menus_table', 1),
(6, '2019_10_18_091536_create_ourteams_table', 1),
(7, '2019_10_18_091900_create_packages_table', 1),
(8, '2019_10_21_010944_create_services_table', 1),
(9, '2019_10_21_011742_create_partners_table', 1),
(10, '2019_10_21_011932_create_slides_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ourteams`
--

CREATE TABLE `ourteams` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ourteams`
--

INSERT INTO `ourteams` (`id`, `content`, `name`, `position`, `image`, `created_at`, `updated_at`) VALUES
(2, '<p>Tiến sĩ Jean-Pierre Naim (quốc tịch Thụy Sỹ) l&agrave; một trong những chuy&ecirc;n gia quốc tế rất nổi tiếng trong lĩnh vực chống l&atilde;o h&oacute;a.</p>\r\n\r\n<p>&Ocirc;ng cũng l&agrave; người s&aacute;ng lập v&agrave; chủ tịch của Hiệp hội Thụy Sĩ về tuổi thọ, thẩm mỹ v&agrave; t&aacute;i sinh (S.S.L.A.R.M.) v&agrave; l&agrave; Ph&oacute; tổng thư k&yacute; của Hiệp hội chống l&atilde;o h&oacute;a ch&acirc;u &Acirc;u (S.E.C.L.A.R.M.). &Ocirc;ng đồng thời l&agrave; th&agrave;nh vi&ecirc;n của Học viện Y học chống l&atilde;o h&oacute;a Hoa Kỳ v&agrave; l&agrave; gi&aacute;m đốc của Học viện Y học chống l&atilde;o h&oacute;a thế giới (W.A.A.A.M.)&nbsp;</p>\r\n\r\n<p>Tiến sĩ Jean-Pierre Naim chuy&ecirc;n nghi&ecirc;n cứu về c&aacute;c loại sản phẩm bổ sung v&agrave; đ&atilde; kết hợp c&aacute;c kỹ thuật điều trị tự nhi&ecirc;n, dinh dưỡng, từ y học t&iacute;ch hợp, y học t&aacute;i tạo, chống l&atilde;o h&oacute;a trong nghi&ecirc;n cứu của m&igrave;nh hơn 32 năm.&nbsp;</p>\r\n\r\n<p>Ngo&agrave;i ra, &ocirc;ng c&ograve;n xuất bản rất nhiều t&aacute;c phẩm li&ecirc;n quan đến y học chống l&atilde;o h&oacute;a v&agrave; đặc biệt l&agrave; c&aacute;c phương ph&aacute;p gi&uacute;p k&eacute;o d&agrave;i tuổi thọ, thay thế hormon bằng c&aacute;ch sử dụng c&aacute;c hormon sinh học tương tự cho người gi&agrave; v&agrave; phụ nữ.</p>\r\n\r\n<p>&nbsp;</p>', 'Nguyễn Trần Tuấn', 'Cố vấn y khoa - Tiến sĩ', 'Dung Ayun.jpg', '2019-11-22 07:34:30', NULL),
(11, '<p>Internet Protocol (IP) l&agrave; một giao thức li&ecirc;n mạng hoạt động ở tầng Network trong m&ocirc; h&igrave;nh OSI. IP quy định c&aacute;ch thức định địa chỉ c&aacute;c m&aacute;y t&iacute;nh v&agrave; c&aacute;ch thức chuyển tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng. IP được đặc tả trong bảng b&aacute;o c&aacute;o kỹ thuật c&oacute; t&ecirc;n Request For Comments (RFC). IP c&oacute; hai chức năng ch&iacute;nh: cung cấp dịch vụ truyền tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng, ph&acirc;n mảnh v&agrave; hợp lại c&aacute;c g&oacute;i tin. C&aacute;c g&oacute;i dữ liệu xuất ph&aacute;t từ tầng Application, đến tầng Network được th&ecirc;m v&agrave;o một cấu tr&uacute;c IP Header. G&oacute;i dữ liệu sau khi được th&ecirc;m v&agrave;o cấu tr&uacute;c IP Header th&igrave; được gọi l&agrave;&nbsp;IP Diagram&nbsp;(Packet). Hiện nay, c&oacute; hai phi&ecirc;n bản IP l&agrave; IP Version 4 (IPv4) v&agrave; IP Version 6 (IPv6), do đ&oacute; c&oacute; 2 cấu tr&uacute;c tương ứng l&agrave; IP Header Version 4 v&agrave; IP header Version 6.</p>', 'Cung Leáccas', 'Chủ tịch', 'Loan.jpg', '2019-11-21 03:02:12', NULL),
(14, '<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Ayun c&oacute; &nbsp;11 năm kinh nghiệm l&agrave;m việc cũng như giữ nhiều vị tr&iacute; quản l&yacute; cấp cao trong lĩnh vực t&agrave;i ch&iacute;nh, ng&acirc;n h&agrave;ng v&agrave; gi&aacute;o dục tại c&aacute;c tổ chức uy t&iacute;n như ANZ, Standard Chartered, Apollo. &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Trong nhiều năm l&agrave;m việc tại ng&acirc;n h&agrave;ng, Ayun đ&atilde; gặp rất nhiều kh&aacute;ch h&agrave;ng c&oacute; vấn đề về sức khỏe v&agrave; thậm ch&iacute; kh&ocirc;ng thể mua được bảo hiểm v&igrave; c&aacute;c vấn đề sức khỏe của họ. Tại Infinity Healthcare, Ayun mong muốn bằng những kiến thức v&agrave; kinh nghiệm của m&igrave;nh, đ&oacute;ng g&oacute;p cho sức khỏe cộng đồng.&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;Ayun tốt nghiệp Cử nh&acirc;n Đại học Văn Lang năm 2008.&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;</p>', 'Nông Văn Dềncacs', 'Giám đốc Công nghệ thông tin', 'Dung Ayun.jpg', '2019-11-21 03:01:32', NULL),
(16, '<p>Dũng c&oacute; 18 năm kinh nghiệm trong nhiều lĩnh vực như T&agrave;i ch&iacute;nh, Bất động sản v&agrave; Thương mại tại nhiều tổ chức uy t&iacute;n như ACB, AIA, Capitalland.</p>\r\n\r\n<p>Bằng kinh nghiệm v&agrave; sự tinh tế trong thấu hiểu t&acirc;m l&yacute; kh&aacute;ch h&agrave;ng, Dũng phụ tr&aacute;ch mảng Truyền th&ocirc;ng v&agrave; ph&aacute;t triển thương hiệu với vai tr&ograve; Gi&aacute;m đốc Truyền th&ocirc;ng, đồng h&agrave;nh c&ugrave;ng Infinity viết tiếp những th&ocirc;ng điệp y&ecirc;u thương lan tỏa đến cộng đồng.</p>\r\n\r\n<p>Dũng tốt nghiệp Cử nh&acirc;n Đại học Quản trị Kinh doanh v&agrave;o năm 2001.</p>', 'Vũ Đức Nguyên', 'Chủ tịch Nasa', 'Dung Ayun.jpg', '2019-10-31 18:51:46', NULL),
(17, '<p>\r\n									Loan có 12 năm kinh nghiệm làm việc trong lĩnh vực tài chính, và từng đảm nhiệm các vị trí quan trọng như Giám đốc tài chính (CFO), Phó Tổng giám đốc, thành viên Ban Kiểm Soát của một số công ty lớn tại Việt Nam: Earnst & Young, Công ty Đầu tư Tài chính nhà nước TP. HCM, Công ty Cổ phần Sài Gòn Kim Cương.\r\n								</p>\r\n								\r\n								<p>\r\n									Loan hiện giữ vị trí Giám đốc tài chính tại IHC và quyết tâm đồng hành cùng IHC trong việc kết nối người Việt Nam đến với những phương pháp điều trị hiện đại nhằm nâng cao sức khỏe, kéo dài tuổi thọ cho cộng đồng và cùng chung tay đóng góp những giá trị tốt đẹp nhằm xây dựng một cộng đồng sống vui, sống khỏe, sống văn minh. \r\n								</p>\r\n								<p>\r\n									Loan tốt nghiệp Cử nhân Kế Toán - Kiểm Toán tại Đại Học Kinh tế TP. HCM vào năm 2007, và tốt nghiệp Thạc Sỹ Quản lý tài chính tại Đại học Northumbia, Vương Quốc Anh năm 2009.\r\n								</p>', 'Trần Duy Hưng', 'Bác sĩ', 'Quyen.jpg', '2019-10-31 18:52:58', NULL),
(18, '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>', 'Nguyễn Ngọc Ngân', 'Nhân viên kinh doanh', 'Ngan.jpg', '2019-11-21 03:05:03', NULL),
(19, '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>', 'Nguyễn Ngọc Kim', 'Nhân viên kinh doanh', 'Quyen.jpg', '2019-11-21 09:31:01', NULL),
(20, '<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.&nbsp;</p>', 'Test Mệt Rồi', 'Nhân viên kinh doanh', 'James.jpg', '2019-11-22 02:09:17', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `services_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuid` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `packages`
--

INSERT INTO `packages` (`id`, `services_id`, `name`, `address`, `content`, `date`, `menuid`, `created_at`, `updated_at`) VALUES
(60, 43, 'Gói trị liệu tại Nhật Bản', 'Nhật Bản', '<p>Kiến nghị là một chuyện nhưng qua thực thi vẫn còn khá chậm. Từ đó đến nay dẫu đã qua 6 năm, tuy việc dồn điền đổi thửa có tiến triển tốt nhưng hiện tượng người dân bỏ ruộng vẫn diễn ra.</p>', '20 ngày', 30, '2019-11-22 10:01:20', NULL),
(61, 41, 'Gói trị liệu Trung Quốc', 'Trung Quốc', '<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.&nbsp;</p>', '5 ngày', 30, '2019-11-22 03:13:04', '2019-11-22 03:13:04'),
(66, 41, 'Gói chữa bệnh tại na UY', 'Na uy', '<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>', '5 ngày', 30, '2019-11-22 07:48:15', '2019-11-22 07:48:15'),
(67, 43, 'Gói trị liệu tại Hàn Quốc', '24 Dong Bat, My Dinh, Nam Tu Liem', '<p>rrrrrrrrrrrrrrrrrrrrr</p>', '10 ngày', 30, '2019-11-22 07:49:11', '2019-11-22 07:49:11'),
(68, 42, 'Gói chữa bệnh tại Thụy Sỹ', '24 Dong Bat', '<p>ggggggggggggggggggggggggggggggggggggggggg</p>', '20 ngày', 30, '2019-11-22 07:50:09', '2019-11-22 07:50:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuid` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partners`
--

INSERT INTO `partners` (`id`, `home_title`, `title`, `content`, `image`, `menuid`, `created_at`, `updated_at`) VALUES
(12, 'Đối tác tại Nhật Bản', 'BỆNH VIỆN ĐẠI HỌC TEIKYO - Nhật Bản', '<p>&nbsp;Internet Protocol (IP) l&agrave; một giao thức li&ecirc;n mạng hoạt động ở tầng Network trong m&ocirc; h&igrave;nh OSI. IP quy định c&aacute;ch thức định địa chỉ c&aacute;c m&aacute;y t&iacute;nh v&agrave; c&aacute;ch thức chuyển tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng. IP được đặc tả trong bảng b&aacute;o c&aacute;o kỹ thuật c&oacute; t&ecirc;n Request For Comments (RFC). IP c&oacute; hai chức năng ch&iacute;nh: cung cấp dịch vụ truyền tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng, ph&acirc;n mảnh v&agrave; hợp lại c&aacute;c g&oacute;i tin. C&aacute;c g&oacute;i dữ liệu xuất ph&aacute;t từ tầng Application.</p>', 'img1.png', 34, '2019-11-22 03:46:10', NULL),
(13, 'Đối tác tại Nhật Bản', 'Bệnh viện Tokyo - Nhật Bản', '<p>&nbsp;Internet Protocol (IP) l&agrave; một giao thức li&ecirc;n mạng hoạt động ở tầng Network trong m&ocirc; h&igrave;nh OSI. IP quy định c&aacute;ch thức định địa chỉ c&aacute;c m&aacute;y t&iacute;nh v&agrave; c&aacute;ch thức chuyển tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng. IP được đặc tả trong bảng b&aacute;o c&aacute;o kỹ thuật c&oacute; t&ecirc;n Request For Comments (RFC). IP c&oacute; hai chức năng ch&iacute;nh: cung cấp dịch vụ truyền tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng, ph&acirc;n mảnh v&agrave; hợp lại c&aacute;c g&oacute;i tin. C&aacute;c g&oacute;i dữ liệu xuất ph&aacute;t từ tầng Application, đến tầng Network được th&ecirc;m v&agrave;o một cấu tr&uacute;c IP Header. G&oacute;i dữ liệu sau khi được th&ecirc;m v&agrave;o cấu tr&uacute;c IP Header th&igrave; được gọi l&agrave;&nbsp;IP Diagram&nbsp;(Packet).&nbsp;</p>', 'img2.png', 34, '2019-11-22 03:47:12', NULL),
(14, 'Đối tác Thụy Sĩ', 'TẬP ĐOÀN BỆNH VIỆN HIRSLANDEN - THỤY SĨ', '<p>&nbsp;Internet Protocol (IP) l&agrave; một giao thức li&ecirc;n mạng hoạt động ở tầng Network trong m&ocirc; h&igrave;nh OSI. IP quy định c&aacute;ch thức định địa chỉ c&aacute;c m&aacute;y t&iacute;nh v&agrave; c&aacute;ch thức chuyển tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng. IP được đặc tả trong bảng b&aacute;o c&aacute;o kỹ thuật c&oacute; t&ecirc;n Request For Comments (RFC). IP c&oacute; hai chức năng ch&iacute;nh: cung cấp dịch vụ truyền tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng, ph&acirc;n mảnh v&agrave; hợp lại c&aacute;c g&oacute;i tin. C&aacute;c g&oacute;i dữ liệu xuất ph&aacute;t từ tầng Application, đến tầng Network được th&ecirc;m v&agrave;o một cấu tr&uacute;c IP Header. G&oacute;i dữ liệu sau khi được th&ecirc;m v&agrave;o cấu tr&uacute;c IP Header th&igrave; được gọi l&agrave;&nbsp;IP Diagram&nbsp;(Packet). Hiện nay, c&oacute; hai phi&ecirc;n bản IP l&agrave; IP Version 4 (IPv4) v&agrave; IP Version 6 (IPv6), do đ&oacute; c&oacute; 2 cấu tr&uacute;c tương ứng l&agrave; IP Header Version 4 v&agrave; IP header Version 6</p>', 'img3.png', 34, '2019-11-07 02:13:31', NULL),
(15, 'Đối tác tại Đức', 'Bệnh viện HANAU CHLB ĐỨC', '<p>&nbsp;Internet Protocol (IP) l&agrave; một giao thức li&ecirc;n mạng hoạt động ở tầng Network trong m&ocirc; h&igrave;nh OSI. IP quy định c&aacute;ch thức định địa chỉ c&aacute;c m&aacute;y t&iacute;nh v&agrave; c&aacute;ch thức chuyển tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng. IP được đặc tả trong bảng b&aacute;o c&aacute;o kỹ thuật c&oacute; t&ecirc;n Request For Comments (RFC). IP c&oacute; hai chức năng ch&iacute;nh: cung cấp dịch vụ truyền tải c&aacute;c g&oacute;i tin qua một li&ecirc;n mạng, ph&acirc;n mảnh v&agrave; hợp lại c&aacute;c g&oacute;i tin. C&aacute;c g&oacute;i dữ liệu xuất ph&aacute;t từ tầng Application, đến tầng Network được th&ecirc;m v&agrave;o một cấu tr&uacute;c IP Header. G&oacute;i dữ liệu sau khi được th&ecirc;m v&agrave;o cấu tr&uacute;c IP Header th&igrave; được gọi l&agrave;&nbsp;IP Diagram&nbsp;(Packet). Hiện nay, c&oacute; hai phi&ecirc;n bản IP l&agrave; IP Version 4 (IPv4) v&agrave; IP Version 6 (IPv6), do đ&oacute; c&oacute; 2 cấu tr&uacute;c tương ứng l&agrave; IP Header Version 4 v&agrave; IP header Version 6</p>', 'img4.png', 34, '2019-11-07 02:18:16', NULL),
(17, 'Đối tác lớn đến từ Trung Quốc', 'Đối tác đến từ Trung Quốc', '<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.&nbsp;</p>', '2.3 Goi chua benh tai Thai Lan.jpg', 34, '2019-11-22 02:12:01', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menuid` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services`
--

INSERT INTO `services` (`id`, `title`, `icon`, `link`, `menuid`, `created_at`, `updated_at`) VALUES
(40, 'Nâng cao sức khỏe', '2.1_Nang cao suc khoe.jpg', 'nangcaosuckheo', 29, '2019-11-22 10:12:00', NULL),
(41, 'Tối ưu hóa điều trị bệnh mãn tính cơ thể', '2.3_Dieu tri bang cong nghe cao.jpg', 'toiuuhoadieutribenhmantinhcothe', 29, '2019-11-20 07:15:58', NULL),
(42, 'Ứng dụng y học công nghệ cao vào thực tế', '2.3_Dieu tri bang cong nghe cao.jpg', 'ungdungcongnghecaovaothucte', 29, '2019-11-20 07:16:54', NULL),
(43, 'Giải phẫu thẩm mỹ', '2.4_Phau thuat tham my.jpg', 'giaiphauthammy', 29, '2019-11-22 01:46:34', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services_detail`
--

CREATE TABLE `services_detail` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_con` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `links` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services_detail`
--

INSERT INTO `services_detail` (`id`, `title_con`, `content`, `links`, `services_id`, `created_at`, `updated_at`) VALUES
(38, 'Sức khỏe tốt', '<p>The biggest difference between the Internet and intranet is that the Internet can be accessed by anyone from anywhere, whereas the intranet can only be accessed by a specific group of people who possess an authenticated login and are connected to the required LAN or VPN. Beyond that, there are several more simple distinctions, such as</p>', 'suckhoe', 40, '2019-11-15 02:13:53', '2019-11-20 07:19:03'),
(39, 'Tầm soát chuyên sâu', '<p><img alt=\"Cấu trúc gói PPTP\" src=\"https://vnpro.vn/upload/user/images/Th%C6%B0%20Vi%E1%BB%87n/403-2.jpg\" style=\"height:86px; width:643px\" /></p>\r\n\r\n<p><em>Cấu tr&uacute;c g&oacute;i PPTP</em></p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i khung PPP. Phần tải PPP ban đầu được mật m&atilde; v&agrave; đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề PPP để tạo ra khung PPP. Sau đ&oacute;, khung PPP được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề của phi&ecirc;n bản sửa đổi giao thức GRE. Đối với PPTP, phần ti&ecirc;u đề của GRE được sử đổi một số điểm sau:</p>\r\n\r\n<p>- Một bit x&aacute;c nhận được sử dụng để khẳng định sự c&oacute; mặt của trường x&aacute;c nhận 32 bit.</p>\r\n\r\n<p>- Trường Key được thay thế bằng trường độ d&agrave;i Payload 16bit v&agrave; trường nhận dạng cuộc gọi 16 bit. Trường nhận dạng cuộc goi Call ID được thiết lập bởi PPTP client trong qu&aacute; tr&igrave;nh khởi tạo đường hầm PPTP.</p>\r\n\r\n<p>- Một trường x&aacute;c nhận d&agrave;i 32 bit được th&ecirc;m v&agrave;o. GRE l&agrave; giao thức cung cấp cơ chế chung cho ph&eacute;p đ&oacute;ng g&oacute;i dữ liệu để gửi qua mạng IP.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i c&aacute;c g&oacute;i GRE Tiếp đ&oacute;, phần tải PPP đ&atilde; được m&atilde; ho&aacute; v&agrave; phần ti&ecirc;u đề GRE được đ&oacute;ng g&oacute;i với một ti&ecirc;u đề IP chứa th&ocirc;ng tin địa chỉ nguồn v&agrave; đ&iacute;ch cho PPTP client v&agrave; PPTP server.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i lớp li&ecirc;n kết dữ liệu Do đường hầm của PPTP hoạt động ở lớp 2 - Lớp li&ecirc;n kết dữ liệu trong m&ocirc; h&igrave;nh OSI n&ecirc;n lược đồ dữ liệu IP sẽ được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề (Header) v&agrave; phần kết th&uacute;c (Trailer) của lớp li&ecirc;n kết dữ liệu.</p>\r\n\r\n<p>V&iacute; dụ, Nếu IP datagram được gửi qua giao diện Ethernet th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer Ethernet. Nếu IP datagram được gửi th&ocirc;ng qua đường truyền WAN điểm tới điểm th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer của giao thức PPP.</p>', 'tamsoatchuyensau', 40, '2019-11-15 03:19:55', '2019-11-15 03:19:55'),
(40, 'Thải độc', '<p><img alt=\"Cấu trúc gói PPTP\" src=\"https://vnpro.vn/upload/user/images/Th%C6%B0%20Vi%E1%BB%87n/403-2.jpg\" style=\"height:86px; width:643px\" /></p>\r\n\r\n<p><em>Cấu tr&uacute;c g&oacute;i PPTP</em></p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i khung PPP. Phần tải PPP ban đầu được mật m&atilde; v&agrave; đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề PPP để tạo ra khung PPP. Sau đ&oacute;, khung PPP được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề của phi&ecirc;n bản sửa đổi giao thức GRE. Đối với PPTP, phần ti&ecirc;u đề của GRE được sử đổi một số điểm sau:</p>\r\n\r\n<p>- Một bit x&aacute;c nhận được sử dụng để khẳng định sự c&oacute; mặt của trường x&aacute;c nhận 32 bit.</p>\r\n\r\n<p>- Trường Key được thay thế bằng trường độ d&agrave;i Payload 16bit v&agrave; trường nhận dạng cuộc gọi 16 bit. Trường nhận dạng cuộc goi Call ID được thiết lập bởi PPTP client trong qu&aacute; tr&igrave;nh khởi tạo đường hầm PPTP.</p>\r\n\r\n<p>- Một trường x&aacute;c nhận d&agrave;i 32 bit được th&ecirc;m v&agrave;o. GRE l&agrave; giao thức cung cấp cơ chế chung cho ph&eacute;p đ&oacute;ng g&oacute;i dữ liệu để gửi qua mạng IP.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i c&aacute;c g&oacute;i GRE Tiếp đ&oacute;, phần tải PPP đ&atilde; được m&atilde; ho&aacute; v&agrave; phần ti&ecirc;u đề GRE được đ&oacute;ng g&oacute;i với một ti&ecirc;u đề IP chứa th&ocirc;ng tin địa chỉ nguồn v&agrave; đ&iacute;ch cho PPTP client v&agrave; PPTP server.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i lớp li&ecirc;n kết dữ liệu Do đường hầm của PPTP hoạt động ở lớp 2 - Lớp li&ecirc;n kết dữ liệu trong m&ocirc; h&igrave;nh OSI n&ecirc;n lược đồ dữ liệu IP sẽ được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề (Header) v&agrave; phần kết th&uacute;c (Trailer) của lớp li&ecirc;n kết dữ liệu.</p>\r\n\r\n<p>V&iacute; dụ, Nếu IP datagram được gửi qua giao diện Ethernet th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer Ethernet. Nếu IP datagram được gửi th&ocirc;ng qua đường truyền WAN điểm tới điểm th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer của giao thức PPP.</p>', 'thaidoc', 40, '2019-11-15 03:21:18', '2019-11-15 03:21:18'),
(41, 'Chống lão hóa', '<p><img alt=\"Cấu trúc gói PPTP\" src=\"https://vnpro.vn/upload/user/images/Th%C6%B0%20Vi%E1%BB%87n/403-2.jpg\" style=\"height:86px; width:643px\" /></p>\r\n\r\n<p><em>Cấu tr&uacute;c g&oacute;i PPTP</em></p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i khung PPP. Phần tải PPP ban đầu được mật m&atilde; v&agrave; đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề PPP để tạo ra khung PPP. Sau đ&oacute;, khung PPP được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề của phi&ecirc;n bản sửa đổi giao thức GRE. Đối với PPTP, phần ti&ecirc;u đề của GRE được sử đổi một số điểm sau:</p>\r\n\r\n<p>- Một bit x&aacute;c nhận được sử dụng để khẳng định sự c&oacute; mặt của trường x&aacute;c nhận 32 bit.</p>\r\n\r\n<p>- Trường Key được thay thế bằng trường độ d&agrave;i Payload 16bit v&agrave; trường nhận dạng cuộc gọi 16 bit. Trường nhận dạng cuộc goi Call ID được thiết lập bởi PPTP client trong qu&aacute; tr&igrave;nh khởi tạo đường hầm PPTP.</p>\r\n\r\n<p>- Một trường x&aacute;c nhận d&agrave;i 32 bit được th&ecirc;m v&agrave;o. GRE l&agrave; giao thức cung cấp cơ chế chung cho ph&eacute;p đ&oacute;ng g&oacute;i dữ liệu để gửi qua mạng IP.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i c&aacute;c g&oacute;i GRE Tiếp đ&oacute;, phần tải PPP đ&atilde; được m&atilde; ho&aacute; v&agrave; phần ti&ecirc;u đề GRE được đ&oacute;ng g&oacute;i với một ti&ecirc;u đề IP chứa th&ocirc;ng tin địa chỉ nguồn v&agrave; đ&iacute;ch cho PPTP client v&agrave; PPTP server.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i lớp li&ecirc;n kết dữ liệu Do đường hầm của PPTP hoạt động ở lớp 2 - Lớp li&ecirc;n kết dữ liệu trong m&ocirc; h&igrave;nh OSI n&ecirc;n lược đồ dữ liệu IP sẽ được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề (Header) v&agrave; phần kết th&uacute;c (Trailer) của lớp li&ecirc;n kết dữ liệu.</p>\r\n\r\n<p>V&iacute; dụ, Nếu IP datagram được gửi qua giao diện Ethernet th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer Ethernet. Nếu IP datagram được gửi th&ocirc;ng qua đường truyền WAN điểm tới điểm th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer của giao thức PPP.</p>', 'chonglaohoa', 40, '2019-11-15 03:22:01', '2019-11-15 03:22:01'),
(42, 'Bệnh tiểu đường', '<p><img alt=\"Cấu trúc gói PPTP\" src=\"https://vnpro.vn/upload/user/images/Th%C6%B0%20Vi%E1%BB%87n/403-2.jpg\" style=\"height:86px; width:643px\" /></p>\r\n\r\n<p><em>Cấu tr&uacute;c g&oacute;i PPTP</em></p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i khung PPP. Phần tải PPP ban đầu được mật m&atilde; v&agrave; đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề PPP để tạo ra khung PPP. Sau đ&oacute;, khung PPP được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề của phi&ecirc;n bản sửa đổi giao thức GRE. Đối với PPTP, phần ti&ecirc;u đề của GRE được sử đổi một số điểm sau:</p>\r\n\r\n<p>- Một bit x&aacute;c nhận được sử dụng để khẳng định sự c&oacute; mặt của trường x&aacute;c nhận 32 bit.</p>\r\n\r\n<p>- Trường Key được thay thế bằng trường độ d&agrave;i Payload 16bit v&agrave; trường nhận dạng cuộc gọi 16 bit. Trường nhận dạng cuộc goi Call ID được thiết lập bởi PPTP client trong qu&aacute; tr&igrave;nh khởi tạo đường hầm PPTP.</p>\r\n\r\n<p>- Một trường x&aacute;c nhận d&agrave;i 32 bit được th&ecirc;m v&agrave;o. GRE l&agrave; giao thức cung cấp cơ chế chung cho ph&eacute;p đ&oacute;ng g&oacute;i dữ liệu để gửi qua mạng IP.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i c&aacute;c g&oacute;i GRE Tiếp đ&oacute;, phần tải PPP đ&atilde; được m&atilde; ho&aacute; v&agrave; phần ti&ecirc;u đề GRE được đ&oacute;ng g&oacute;i với một ti&ecirc;u đề IP chứa th&ocirc;ng tin địa chỉ nguồn v&agrave; đ&iacute;ch cho PPTP client v&agrave; PPTP server.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i lớp li&ecirc;n kết dữ liệu Do đường hầm của PPTP hoạt động ở lớp 2 - Lớp li&ecirc;n kết dữ liệu trong m&ocirc; h&igrave;nh OSI n&ecirc;n lược đồ dữ liệu IP sẽ được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề (Header) v&agrave; phần kết th&uacute;c (Trailer) của lớp li&ecirc;n kết dữ liệu.</p>\r\n\r\n<p>V&iacute; dụ, Nếu IP datagram được gửi qua giao diện Ethernet th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer Ethernet. Nếu IP datagram được gửi th&ocirc;ng qua đường truyền WAN điểm tới điểm th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer của giao thức PPP.</p>', 'tieuduong', 41, '2019-11-15 03:22:40', '2019-11-15 03:22:40'),
(43, 'Công nghệ tế bào gốc', '<p><img alt=\"Cấu trúc gói PPTP\" src=\"https://vnpro.vn/upload/user/images/Th%C6%B0%20Vi%E1%BB%87n/403-2.jpg\" style=\"height:86px; width:643px\" /></p>\r\n\r\n<p><em>Cấu tr&uacute;c g&oacute;i PPTP</em></p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i khung PPP. Phần tải PPP ban đầu được mật m&atilde; v&agrave; đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề PPP để tạo ra khung PPP. Sau đ&oacute;, khung PPP được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề của phi&ecirc;n bản sửa đổi giao thức GRE. Đối với PPTP, phần ti&ecirc;u đề của GRE được sử đổi một số điểm sau:</p>\r\n\r\n<p>- Một bit x&aacute;c nhận được sử dụng để khẳng định sự c&oacute; mặt của trường x&aacute;c nhận 32 bit.</p>\r\n\r\n<p>- Trường Key được thay thế bằng trường độ d&agrave;i Payload 16bit v&agrave; trường nhận dạng cuộc gọi 16 bit. Trường nhận dạng cuộc goi Call ID được thiết lập bởi PPTP client trong qu&aacute; tr&igrave;nh khởi tạo đường hầm PPTP.</p>\r\n\r\n<p>- Một trường x&aacute;c nhận d&agrave;i 32 bit được th&ecirc;m v&agrave;o. GRE l&agrave; giao thức cung cấp cơ chế chung cho ph&eacute;p đ&oacute;ng g&oacute;i dữ liệu để gửi qua mạng IP.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i c&aacute;c g&oacute;i GRE Tiếp đ&oacute;, phần tải PPP đ&atilde; được m&atilde; ho&aacute; v&agrave; phần ti&ecirc;u đề GRE được đ&oacute;ng g&oacute;i với một ti&ecirc;u đề IP chứa th&ocirc;ng tin địa chỉ nguồn v&agrave; đ&iacute;ch cho PPTP client v&agrave; PPTP server.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i lớp li&ecirc;n kết dữ liệu Do đường hầm của PPTP hoạt động ở lớp 2 - Lớp li&ecirc;n kết dữ liệu trong m&ocirc; h&igrave;nh OSI n&ecirc;n lược đồ dữ liệu IP sẽ được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề (Header) v&agrave; phần kết th&uacute;c (Trailer) của lớp li&ecirc;n kết dữ liệu.</p>\r\n\r\n<p>V&iacute; dụ, Nếu IP datagram được gửi qua giao diện Ethernet th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer Ethernet. Nếu IP datagram được gửi th&ocirc;ng qua đường truyền WAN điểm tới điểm th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer của giao thức PPP.</p>', 'tebaogoc', 42, '2019-11-15 03:23:27', '2019-11-15 03:23:27'),
(44, 'Phẫu thuật mặt', '<p><img alt=\"Cấu trúc gói PPTP\" src=\"https://vnpro.vn/upload/user/images/Th%C6%B0%20Vi%E1%BB%87n/403-2.jpg\" style=\"height:86px; width:643px\" /></p>\r\n\r\n<p><em>Cấu tr&uacute;c g&oacute;i PPTP</em></p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i khung PPP. Phần tải PPP ban đầu được mật m&atilde; v&agrave; đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề PPP để tạo ra khung PPP. Sau đ&oacute;, khung PPP được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề của phi&ecirc;n bản sửa đổi giao thức GRE. Đối với PPTP, phần ti&ecirc;u đề của GRE được sử đổi một số điểm sau:</p>\r\n\r\n<p>- Một bit x&aacute;c nhận được sử dụng để khẳng định sự c&oacute; mặt của trường x&aacute;c nhận 32 bit.</p>\r\n\r\n<p>- Trường Key được thay thế bằng trường độ d&agrave;i Payload 16bit v&agrave; trường nhận dạng cuộc gọi 16 bit. Trường nhận dạng cuộc goi Call ID được thiết lập bởi PPTP client trong qu&aacute; tr&igrave;nh khởi tạo đường hầm PPTP.</p>\r\n\r\n<p>- Một trường x&aacute;c nhận d&agrave;i 32 bit được th&ecirc;m v&agrave;o. GRE l&agrave; giao thức cung cấp cơ chế chung cho ph&eacute;p đ&oacute;ng g&oacute;i dữ liệu để gửi qua mạng IP.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i c&aacute;c g&oacute;i GRE Tiếp đ&oacute;, phần tải PPP đ&atilde; được m&atilde; ho&aacute; v&agrave; phần ti&ecirc;u đề GRE được đ&oacute;ng g&oacute;i với một ti&ecirc;u đề IP chứa th&ocirc;ng tin địa chỉ nguồn v&agrave; đ&iacute;ch cho PPTP client v&agrave; PPTP server.</p>\r\n\r\n<p>+ Đ&oacute;ng g&oacute;i lớp li&ecirc;n kết dữ liệu Do đường hầm của PPTP hoạt động ở lớp 2 - Lớp li&ecirc;n kết dữ liệu trong m&ocirc; h&igrave;nh OSI n&ecirc;n lược đồ dữ liệu IP sẽ được đ&oacute;ng g&oacute;i với phần ti&ecirc;u đề (Header) v&agrave; phần kết th&uacute;c (Trailer) của lớp li&ecirc;n kết dữ liệu.</p>\r\n\r\n<p>V&iacute; dụ, Nếu IP datagram được gửi qua giao diện Ethernet th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer Ethernet. Nếu IP datagram được gửi th&ocirc;ng qua đường truyền WAN điểm tới điểm th&igrave; sẽ được đ&oacute;ng g&oacute;i với phần Header v&agrave; Trailer của giao thức PPP.</p>', 'phauthuatmat', 43, '2019-11-15 03:24:09', '2019-11-15 03:24:09'),
(45, 'Chăm sóc da mặt', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>', 'chamsocdamat', 44, '2019-11-18 03:59:11', '2019-11-18 03:59:11'),
(46, 'Sức khẻo mạnh khẻo', '<p>The biggest difference between the Internet and intranet is that the Internet can be accessed by anyone from anywhere, whereas the intranet can only be accessed by a specific group of people who possess an authenticated login and are connected to the required LAN or VPN. Beyond that, there are several more simple distinctions, such as</p>', 'suckheomanhkheo', 40, '2019-11-18 06:45:16', '2019-11-18 06:45:16'),
(53, 'Điều trị bệnh ho', '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>', '12345', 40, '2019-11-18 08:21:11', '2019-11-18 08:21:11'),
(54, 'Sức khẻo tốtss', '<p>sss</p>', 'suckheoss', 44, '2019-11-19 07:44:09', '2019-11-19 07:44:09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `service_homes`
--

CREATE TABLE `service_homes` (
  `id` int(11) NOT NULL,
  `services_id` int(11) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `service_homes`
--

INSERT INTO `service_homes` (`id`, `services_id`, `image`, `content`, `created_at`, `updated_at`) VALUES
(40, 40, '4.3 Ung dung cong nghe cao.jpg', '<p>Trong khi đ&oacute;, ch&iacute;nh s&aacute;ch cụ thể t&aacute;c động v&agrave;o đối tượng người bỏ ruộng th&igrave; chưa ph&aacute;t huy t&aacute;c dụng. Điều đ&aacute;ng n&oacute;i, từ đ&oacute;, Cục Kinh tế hợp t&aacute;c v&agrave; Ph&aacute;t triển n&ocirc;ng th&ocirc;n kh&ocirc;ng tiếp tục c&oacute; những nghi&ecirc;n cứu, khảo s&aacute;t vấn đề n&ocirc;ng d&acirc;n bỏ ruộng để c&oacute; th&ecirc;m kiến nghị x&aacute;c thực. Ngay như Trung ương Hội N&ocirc;ng d&acirc;n Việt Nam đến nay vẫn chưa c&oacute; đ&aacute;nh gi&aacute; tổng thể về lĩnh vực n&agrave;y. Đối tượng trực tiếp bị ảnh hưởng l&agrave; người n&ocirc;ng d&acirc;n vẫn loay hoay giữa bỏ một phần ruộng v&agrave; t&igrave;m một c&ocirc;ng việc c&oacute; thu nhập cao hơn, hoặc tiếp tục &ldquo;&ocirc;m&rdquo; ruộng v&agrave; &ldquo;&ocirc;m&rdquo; ngh&egrave;o&rdquo;.</p>', '2019-11-22 07:41:45', '2019-11-22 07:41:45'),
(41, 41, '4.4 Giai phau tham my.jpg', '<p>You may use the&nbsp;<code>where</code>&nbsp;method on a query builder instance to add&nbsp;<code>where</code>&nbsp;clauses to the query. The most basic call to&nbsp;<code>where</code>&nbsp;requires three arguments. The first argument is the name of the column. The second argument is an operator, which can be any of the database&#39;s supported operators. Finally, the third argument is the value to evaluate against the column.</p>\r\n\r\n<p>For example, here is a query that verifies the value of the &quot;votes&quot; column is equal to 100:</p>', '2019-11-22 07:42:12', '2019-11-22 07:42:12'),
(42, 43, '4.1 Nang cao suc khoe.jpg', '<p>You may use the&nbsp;<code>where</code>&nbsp;method on a query builder instance to add&nbsp;<code>where</code>&nbsp;clauses to the query. The most basic call to&nbsp;<code>where</code>&nbsp;requires three arguments. The first argument is the name of the column. The second argument is an operator, which can be any of the database&#39;s supported operators. Finally, the third argument is the value to evaluate against the column.</p>\r\n\r\n<p>For example, here is a query that verifies the value of the &quot;votes&quot; column is equal to 100:</p>', '2019-11-22 07:42:32', '2019-11-22 07:42:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `links` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slides`
--

INSERT INTO `slides` (`id`, `name`, `image`, `title`, `links`, `created_at`, `updated_at`) VALUES
(3, 'Infinity healthcare Việt Nam 2019', '1. Hinh anh tren cung (banner)01.jpg', 'TRẢI NGHIỆM DỊCH VỤ Y KHOA HÀNG ĐẦU', '123456', '2019-11-22 07:32:32', NULL),
(10, 'PHẪU THUẬT THẨM MỸ', 'BG.jpg', 'Các bệnh thường gặp', '123456', '2019-11-14 07:11:13', '2019-11-14 07:11:13'),
(13, 'Chương trình khuyến mãi tháng 12', '4.3 Ung dung cong nghe cao.jpg', 'Nhiều ưu đãi đặc biệt cho cuối năm', '123456', '2019-11-21 09:11:43', '2019-11-21 09:11:43'),
(14, 'Các gói trị liệu của chúng tôi', 'BG.jpg', 'các gói trong và ngoài nước của chúng tôi', '123456', '2019-11-22 02:03:55', '2019-11-22 02:03:55');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'nguyen', 'nguyen@gmail.com', NULL, '$2y$10$t0N9/mFjKujnWWvf1RGIKuQpwA/rkECMKplGVYlEHhmPt.92j5jqm', 'MCfZ8WkbeRCz4KD7Ghe2jwlPJH9zZHlMUylMRIyVxsARoksqTAYtkdqmPSQR', '2019-10-22 18:33:03', '2019-10-22 18:33:03'),
(4, 'tuan', 'tuan@gmail.com', NULL, '$2y$10$tAH.P2Uxq9QLn6BE4F0DPOr/y4B25FXr5PkNtI1fRhODDJRj34U8S', 'BjCJfmgqaufm5tlLljZfF7ej1TXQrJsDVwNxOT2G6TsLzue9dTdUfgmjCzer', '2019-10-22 18:37:30', '2019-10-22 18:37:30'),
(9, 'tran ngoc hieu', 'duong@gmail.com', NULL, '$2y$10$9qKiQbrXZJgQ.RHvzJ1weu0YOsK0XC3ybJOb5.n5dbBzk2tj7WnEK', NULL, '2019-11-05 03:40:30', '2019-11-05 03:40:30');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `about_packages`
--
ALTER TABLE `about_packages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer_comments`
--
ALTER TABLE `customer_comments`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ourteams`
--
ALTER TABLE `ourteams`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `services_detail`
--
ALTER TABLE `services_detail`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `service_homes`
--
ALTER TABLE `service_homes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `about_packages`
--
ALTER TABLE `about_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT cho bảng `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `customer_comments`
--
ALTER TABLE `customer_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `ourteams`
--
ALTER TABLE `ourteams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT cho bảng `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT cho bảng `services_detail`
--
ALTER TABLE `services_detail`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT cho bảng `service_homes`
--
ALTER TABLE `service_homes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT cho bảng `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
