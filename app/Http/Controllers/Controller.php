<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;

class Controller extends BaseController
{
	public $langs = ['vi'=>'VI','en'=>'EN'];
	public $lang_code = ['VI'=>'Việt Nam','EN'=>'Tiếng Anh'];
    public function __construct() 
    {
        View::share('lang_code', $this->lang_code);
    }	

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
