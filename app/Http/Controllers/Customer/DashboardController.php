<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Partner;
use App\Models\Service;
use App\Models\Services_detail;
use Illuminate\Support\Facades\DB;
use App\Models\Slide;
use App\Models\Menu;
use App\Models\OurTeam;
use App\Models\About;
use App\Models\AboutPackage;
use App\Models\AboutService;
use App\Models\CustomerComments;
class DashboardController extends Controller
{
    public function index(){
         
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }
    	$package = AboutPackage::where('lang_code',$lang)->paginate(3);
    	$partner = Partner::where('lang_code',$lang)->get();

    	$slide = Slide::where('lang_code',$lang)->get();
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

        $image = Menu::whereId(23)->get();

        $dv = DB::table('service_homes')
            ->where('service_homes.lang_code',$lang)
            ->join('services', 'service_homes.services_id', '=', 'services.id')
            ->select('services.title', 'services.link', 'service_homes.content', 'service_homes.image')
            ->paginate(4);
        $services_details = Services_detail::where('lang_code',$lang)->get();
           

        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        
    	$service = Service::where('lang_code',$lang)->paginate(4);
        $comment = CustomerComments::where('lang_code',$lang)->get();
    	return view('customer.dashboard.dashboard', compact('package', 'partner', 'service', 'slide', 'data','title','dv','image','doingu', 'comment', 'services_details'));

    }
    public function changeLanguage($language)
    {

        \Session::put('website_language', $language);

        return redirect()->back();
    }
}
