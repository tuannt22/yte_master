<?php

namespace App\Http\Controllers\Customer;

use App\Models\About;
use App\Models\BlogBanner;
use App\Models\Blogs;
use App\Models\Menu;
use App\Models\OurTeam;
use App\Models\Package;
use App\Models\Partner;
use App\Models\Tags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BlogCustomerController extends Controller
{
    public function list(Request $request) {
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

        $image = Menu::whereId(36)->get();


        $dv = DB::table('services')
            ->where('services.lang_code',$lang)
            ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
            ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
            ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        $where = [];
        if ($request->tag) {
            $where['blog_tag.tag_id'] = $request->tag;
        }
        if ($request->topic_id) {
            $where['blogs.topic_id'] = $request->topic_id;
        }
        $blogs = DB::table('blogs')->select('blogs.*', 'topics.name as topic_name')->join('blog_tag', 'blog_tag.blog_id', '=', 'blogs.id')
                    ->join('topics', 'topics.id', '=', 'blogs.topic_id')
                    ->where($where)->groupBy('blogs.id')->orderBy('blogs.id','desc')->get();
        $blogMost = Blogs::limit(3)->orderBy('view', 'desc')->get();
        $idBlogMost = array_map(function ($item) {
            return $item->id;
        }, $blogMost->all());

        $hotNews = Blogs::whereNotIn('id', $idBlogMost)->limit(3)->orderBy('id')->get();
        $tags = Tags::limit(20)->get();

        return view('customer.blogs.list', compact('data','title','blogs','dv','image','doingu','packages','partners', 'tags', 'blogMost', 'hotNews'));
    }

    public function detail($id) {
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();/*dd($data);*/

        $image = Menu::whereId(36)->get();


        $dv = DB::table('services')
            ->where('services.lang_code',$lang)
            ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
            ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
            ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        $blog = Blogs::with('topics', 'blogTag')->find($id);
        Blogs::where('id', '=', $id)->update(['view' => $blog->view + 1]);
        $blogTopic = Blogs::where([['topic_id', '=', $blog->topic_id], ['id', '<>', $blog->id]])->limit(6)->get();
        $blogMost = Blogs::limit(3)->orderBy('view', 'desc')->get();
        $idBlogMost = array_map(function ($item) {
            return $item->id;
        }, $blogMost->all());

        $hotNews = Blogs::whereNotIn('id', $idBlogMost)->limit(3)->orderBy('id')->get();
        $blogBanner = BlogBanner::where('status', '=', 1)->limit(1)->first();
        return view('customer.blogs.detail', compact('data','title','dv','image','doingu','packages','partners', 'blog', 'blogTopic', 'blogMost', 'hotNews', 'blogBanner'));
    }
}
