<?php

namespace App\Http\Controllers\Customer;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Menu;
use App\Models\Services_detail;
use App\Models\Service;
use Illuminate\Support\Facades\DB;
use App\Models\OurTeam;
use App\Models\Package;
use App\Models\Partner;
use App\Models\News;
use App\Models\Library;

use App\Models\CustomerComments;
use Illuminate\Support\Facades\Mail;

class AboutController extends Controller
{
    public function showDataAbout(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }        

        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

    	$image = Menu::whereId(23)->get();

    	$dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();

        $comment = CustomerComments::where('lang_code',$lang)->get();
        return view('customer.about',compact('data','title','dv','image','doingu', 'comment', 'lang'));
    }
    public function showDataServices(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        } 

        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

    	$image = Menu::whereId(29)->get();

    	$dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.*', 'services_detail.*')
        ->distinct()
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();

        return view('customer.services',compact('data','title','dv','image','doingu'));
    }
    public function showDataPackages(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }

        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

    	$image = Menu::whereId(30)->get();

    	$dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon', 'services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();

        $packages = DB::table('packages')
        ->where('packages.lang_code',$lang)
        ->join('services', 'services.id', '=', 'packages.services_id')
        ->select('packages.*', 'services.link')
        ->distinct()
        ->get();

        return view('customer.packages',compact('data','title','dv','image','doingu','packages'));
    }


    public function showDataPartners(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        } 
         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

    	$image = Menu::whereId(34)->get();

    	$dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();    
        return view('customer.partners',compact('data','title','dv','image','doingu','packages','partners'));
    }
    public function showDataPartners_detail(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        } 
         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

    	$image = Menu::whereId(34)->get();

    	$dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        return view('customer.partners_detail',compact('data','title','dv','image','doingu','packages','partners'));
    }
    public function showDataAbout_detail(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }
         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

        $image = Menu::whereId(34)->get();

        $dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        return view('customer.about_detail',compact('data','title','dv','image','doingu','packages','partners','lang'));
    }
    public function showDataProcedure(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }
         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

        $image = Menu::whereId(35)->get();

        $dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        return view('customer.procedure',compact('data','title','dv','image','doingu','packages','partners'));
    }
    public function showDataLibrary(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }        
        $pagi = library::where('lang_code',$lang)->paginate(5);
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();
        $doc = Library::where('lang_code',$lang)->get();
        $image = Menu::whereId(36)->get();

        $dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        return view('customer.library',compact('data','title','dv','image','doingu','packages','partners','doc','pagi'));
    }


    public function getDownload($id){

        $download = Library::findOrFail($id);

        $document= $download->document;

        //PDF file is stored under project/public/download/info.pdf
        $file_path = public_path().'/uploads/library/'.$document;

        return response()->download($file_path);
    }






    public function showDataFaqs(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

        $image = Menu::whereId(36)->get();

        $dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        return view('customer.faqs',compact('data','title','dv','image','doingu','packages','partners'));
    }

    public function showDataNews(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

        $image = Menu::whereId(36)->get();

        $new = News::where('lang_code',$lang)->get();

        $dv = DB::table('services')
        ->where('services.lang_code',$lang)
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::where('lang_code',$lang)->get();
        $title = About::where('lang_code',$lang)->get();
        $packages = Package::where('lang_code',$lang)->get();
        $partners = Partner::where('lang_code',$lang)->get();
        return view('customer.news',compact('data','title','new','dv','image','doingu','packages','partners'));
    }


    public function showDataFooter(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        } 
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();

        $image = Menu::whereId(36)->get();

        $dv = DB::table('services')
        ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
        ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.content','services.icon','services_detail.links')
        ->get();
        $doingu = OurTeam::all();
        $title = About::all();
        $packages = Package::all();
        $partners = Partner::all();
        return view('customer.footer',compact('data','title','dv','image','doingu','packages','partners'));
    }
    public function changeLanguage($language)
    {

        \Session::put('website_language', $language);

        return redirect()->back();
    }

    public function showContact(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();
        $image = Menu::whereId(36)->get();
        $dv = DB::table('services')
            ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
            ->select('services.title', 'services_detail.title_con','services_detail.services_id','services_detail.links')
            ->get();
        return view('customer.contact', compact('data', 'image', 'dv'));
    }

    public function sendContact(ContactRequest $request)
    {
        $data = $request->all();
        Mail::to(env('MAIL_TO'))->send(new ContactEmail($data));
        return redirect()->back()->with('notification', __('home.sunmitted_successfully'));
    }

    public function showBemer(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();
        $image = Menu::whereId(36)->get();
        return view('customer.bemer', compact('data', 'image'));
    }

    public function showDrLodi(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();
        $image = Menu::whereId(36)->get();
        return view('customer.bemer', compact('data', 'image'));
    }

    public function showDrNaim(){
        if (session()->exists('website_language')) {
            $lang = $this->langs[session()->get('website_language')];
        }else{
            $lang = $this->langs['vi'];
        }         
        $data = Menu::with(['abouts' => function ($query) use ($lang) {
            $query->where('lang_code', $lang);
        }])->where('lang_code',$lang)->get();
        $image = Menu::whereId(36)->get();
        return view('customer.bemer', compact('data', 'image'));
    }
}
