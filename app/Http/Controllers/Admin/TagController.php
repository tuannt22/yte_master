<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogTag;
use App\Models\Tags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tags::paginate(15);
        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255'
        ],
            [
                'required' => 'The :attribute field is required.',
            ]);
        $updateData = $request->all();
        unset($updateData['_method']);
        unset($updateData['_token']);
        if (Tags::insert($updateData)) {
            return redirect()->route('admin.tags.index')->with('message', 'Thêm mới thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tags::find($id);
        return view('admin.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255'
        ],
            [
                'required' => 'The :attribute field is required.',
            ]);
        $updateData = $request->all();
        unset($updateData['_method']);
        unset($updateData['_token']);
        if (Tags::where('id', '=', $id)->update($updateData)) {
            return redirect()->route('admin.tags.index')->with('message', 'Chỉnh sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Tags::find($id)->delete()) {
            BlogTag::where('tag_id', '=', $id)->delete();
            return redirect()->route('admin.tags.index')->with('message', 'Xoá thành công');
        } else {
            return redirect()->route('admin.tags.index')->with('error', 'Đã có lỗi xảy ra');
        }
    }
}
