<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blogs;
use App\Models\BlogTag;
use App\Models\Tags;
use App\Models\Topics;
use Cassandra\Date;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blogs::with('topics')->paginate(15);
        return view('admin.blogs.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topics = Topics::all();
        $tags = Tags::all();
        return view('admin.blogs.create', compact( 'topics', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'title' => 'required|max:255',
            'sumary' => 'required',
            'topic_id' => 'required|nullable',
            'content' => 'required',
        ],
            [
                'required' => 'The :attribute field is required.',
                'max' => 'The :attribute field max :max.'
            ]
        );
        $dataUpdate = $request->all();
        if($request->hasFile('image_preview') && preg_match('/^image\//', $request->file('image_preview')->getMimeType())){
            $file = $request->file('image_preview');
            $now = \date('YmdHis');
            $name = $file->getClientOriginalName();
            $dataUpdate['image_preview'] = $name;
            $file->move(public_path('uploads/blogs/'),$name);
        }
        unset($dataUpdate['_token']);
        unset($dataUpdate['tag']);
        $blog = Blogs::insertGetId($dataUpdate);
        Blogs::find($blog)->blogTag()->sync($request->tag);
//        if (Blogs::insert($dataUpdate)) {
            return redirect()->route('admin.blogs.index')->with('message', 'Tạo mới thành công');
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blogs::with('topics', 'blogTag')->find($id);
        $topics = Topics::all();
        $tags = Tags::all();
        $idTagArr = array_map(function ($tag) {
            return $tag->id;
        },$blog->blogTag->all());

        return view('admin.blogs.edit', compact('blog', 'topics', 'tags', 'idTagArr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'sumary' => 'required',
            'topic' => 'required',
            'content' => 'required',
        ],
        [
            'required' => 'The :attribute field is required.',
            'max' => 'The :attribute field max :max.'
        ]
        );
        $dataUpdate = [
            'title' => $request->title,
            'sumary' => $request->sumary,
            'topic_id' => $request->topic,
            'content' => $request->content
        ];
        if($request->hasFile('image_preview') && preg_match('/^image\//', $request->file('image_preview')->getMimeType())){
            $file = $request->file('image_preview');
            $now = \date('YmdHis');
            $name = $file->getClientOriginalName();
            $dataUpdate['image_preview'] = $name;
            $file->move(public_path('uploads/blogs/'),$name);
        }
        $blog = Blogs::find($id);
        if (Blogs::where('id', '=', $id)->update($dataUpdate)) {
//            $blog->updated();
            $blog->blogTag()->sync($request->tag);
            return redirect()->route('admin.blogs.index')->with('message', 'Chỉnh sửa thành công');
        } else {
            return redirect()->route('admin.blogs.index')->with('errors', 'Chỉnh sửa thất bại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Blogs::find($id)->delete()) {
            return redirect()->route('admin.blogs.index')->with('message', 'Xoá thành công');
        }
    }

    public function hotNews (Request $request) {
        if ($request->id) {
            $blog = Blogs::select('hot_new')->where('id', '=', $request->id)->first();
            if ($blog->hot_new == 0){
                Blogs::where('id', '=', $request->id)->update(['hot_new' => 1]);
            } elseif ($blog->hot_new == 1) {
                Blogs::where('id', '=', $request->id)->update(['hot_new' => 0]);
            }
            return 'success';
        }
    }
}
