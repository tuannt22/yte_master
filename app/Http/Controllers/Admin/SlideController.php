<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slide;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\AddSlideRequest;
use App\Http\Requests\EditSlideRequest;

class SlideController extends Controller
{
    public function index(){
    	$data = Slide::paginate(5);
    	return view('admin.slide.index', compact('data'));

    }
   public function addSlide(){
    	return view('admin.slide.add');
   }
    
    public function add(AddSlideRequest $request){
    	$dataInsert = new Slide;
	    $dataInsert->name = $request->name;  
	    $dataInsert->title = $request->title; 
	    $dataInsert->links = $request->links;
        $dataInsert->lang_code = $request->lang_code;
	    $checkUpload = false;
	    $namefile = '';

        if($request->hasFile('image')){
            $file = $request->file('image');
            // lay ten file
            $namefile = $file->getClientOriginalName();

            if($file->getError() == 0){
                // upload
                if($file->move(public_path('uploads/slides/'),$namefile)){
                    $checkUpload = true;
                }
            }
        }
        if(!$checkUpload && $namefile == ''){
            return redirect()->route('admin.slide.dashboard')->with('fail', 'Error');
        } else {
            // insert data
            $dataInsert->image = $namefile;
            
            if($dataInsert->save()){
                return redirect()->route('admin.slide.dashboard')->with('message', 'Thêm thành công');
            } else {
                return redirect()->route('admin.slide.addSlide')->with('fail', 'Thêm mới thất bại');
            }
        }

    }


    public function destroy($id){
        $delete = Slide::find($id);
        $namefile= $delete->image;
        $file_path = public_path().'/uploads/slides/'.$namefile;
        if ($delete->delete()&& unlink($file_path)) {
            return redirect()->route('admin.slide.dashboard')->with('message', 'Đã xóa thành công');
        }
        else{
            return view('admin.page.dashboard')->with('fail', 'Thêm mới thất bại');
        }


    }
    public function showFormEditSlide($id){
       $data = Slide::find($id);
       return view('admin.slide.edit', compact('data'));
    }

    public function UpdateSlide($id, EditSlideRequest $request){
        $name = $request->name;
        $image = $request->image;
        $title = $request->title;
        $links = $request->links;
        $lang_code = $request->lang_code;
        $checkUpload = false;
        $dataUp = [
                'name' => $name,
                'title' => $title,
                'links' => $links,
                'lang_code' => $lang_code,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ];
         if($request->hasFile('image')){
            $file = $request->file('image');
            // lay ten file
            $namefile = $file->getClientOriginalName();
            if(!empty($namefile)){
                $dataUp['image'] = $namefile;
                if($file->getError() == 0){
                    // upload
                    if($file->move(public_path('uploads/slides/'),$namefile)){
                        $checkUpload = true;
                    }
                }
            }
            
        }
            
            if(DB::table('slides')->where('id', $id)->update($dataUp)){
                return redirect()->route('admin.slide.dashboard')->with('message', 'Sửa thành công');
            
        }
    }

 
}
