<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blogs;
use App\Models\Topics;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topics::paginate(15);
        return view('admin.topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255'
        ],
            [
                'required' => 'The :attribute field is required.',
            ]);
        $updateData = $request->all();
        unset($updateData['_method']);
        unset($updateData['_token']);
        if (Topics::insert($updateData)) {
            return redirect()->route('admin.topics.index')->with('message', 'Thêm mới thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = Topics::find($id);
        return view('admin.topics.edit', compact('topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255'
        ],
        [
            'required' => 'The :attribute field is required.',
        ]);
        $updateData = $request->all();
        unset($updateData['_method']);
        unset($updateData['_token']);
        if (Topics::where('id', '=', $id)->update($updateData)) {
            return redirect()->route('admin.topics.index')->with('message', 'Chỉnh sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Topics::find($id)->delete()) {
            Blogs::where('topic_id', '=', $id)->delete();
            return redirect()->route('admin.topics.index')->with('message', 'Xoá thành công');
        }
    }
}
