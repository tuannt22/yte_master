<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomerComments;
use App\Http\Requests\AddCustomerComment;
use App\Http\Requests\EditCustomerComment;
use Illuminate\Support\Facades\DB;
class CustomerCommentController extends Controller
{
    public function index(){
    	$data = CustomerComments::all();
    	return view('admin.customercomment.index', compact('data'));
    }

    public function showFormAdd(){
    	return view('admin.customercomment.add');
    }

    public function add(AddCustomerComment $request){
    	$dataInsert = new CustomerComments;
        $dataInsert->name = $request->name; 
        $dataInsert->position = $request->position; 
        $dataInsert->lang_code = $request->lang_code; 
        $dataInsert->content = $request->content; 
              
        if ($dataInsert->save()) {
            return redirect()->back()->with('message', 'Sửa thành công');
        }
        else{
             return view('admin.customercomment.index')->with('fail', 'TSửa thất bại');
        }   
        
    }

    public function showFormEdit($id){
    	$data = CustomerComments::findOrFail($id);

    	return view('admin.customercomment.edit', compact('data'));
    }
    public function update($id, EditCustomerComment $request){

        $name = $request->name; 
        $position = $request->position; 
        $content = $request->content; 

        $update = [
            'name' => $name,
            'position' => $position,
            'content'   =>$content,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => null
        ];
          
        if(DB::table('customer_comments')->where('id', $id)->update($update)){
                // dd('thanh cong');
            return redirect()->route('admin.customercomment.dashboard')->with('message', 'Update thành công');
        } else {
            return redirect()->route('admin.package.edit')->with('fail', 'thất bại');
        }

    }
    public function destroy($id){
        $delete = CustomerComments::find($id);

        if ($delete->delete()) {

            return redirect()->back()->with('message', 'Đã xóa thành công');
        }
        else{
            return view('admin.package.index')->with('fail', 'Thêm mới thất bại');
        }
    }
}
