<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddPartnerRequest;
use App\Http\Requests\EditPartnerRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Partner;
use App\Models\Menu;
class PartnerController extends Controller
{
	public function index(){
		$data = Partner::all();

		return view('admin.partner.index', compact('data'));

	}

	public function show($id){
		$show = Partner::findOrFail($id);
		return view('admin.partner.show', compact('show'));
	}
	public function showFormEdit($id){
		$data = Partner::find($id);
		$menu = Menu::whereIn('id', [34])->get();
		return view('admin.partner.edit', compact('data','menu'));
	}

	public function Update($id, EditPartnerRequest $request)
	{
		$find = Partner::find($id);
		$image= $find->image;
		$file_path = public_path().'/uploads/partners/'.$image;
		$hometitle = $request->hometitle;
		$lang_code = $request->lang_code;
		$title = $request->title;
		$image = $request->image;
		$contentPd = $request->content;
		$menu_id = $request->menu_id;
		$checkUpload = false;
		if($lang_code == 'EN'){$menuid = 37;}else{$menuid = 23;}
		$update = [
			'title' => $title,
			'content' => $contentPd,
			'home_title' => $hometitle,
			'lang_code' => $lang_code,

			'menuid' =>$menuid,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => null
		];
        // dd($request->file('image'));
		if($request->hasFile('image')){
			$file = $request->file('image');
            // lay ten file
			$namefile = $file->getClientOriginalName();
			if(!empty($namefile)){
				$update['image'] = $namefile;
				if($file->getError() == 0){
                // upload
					if($file->move(public_path('uploads/partners/'),$namefile)){
						$checkUpload = true;
					}
				}

			}


		}

			if(DB::table('partners')->where('id', $id)->update($update)){
				return redirect()->route('admin.partner.dashboard')->with('message', 'Update thành công');
			} 
		}

	
	public function addAbout()
	{
		$data = Menu::whereIn('id', [34])->get();
		return view('admin.partner.add',compact('data'));
	}

	public function add(AddPartnerRequest $request){
		$title = $request->title;
		$image = $request->image;
		$hometitle = $request->hometitle;
		$contentPd = $request->content;
		$menu_id = $request->menu_id;
		$lang_code = $request->lang_code;
		$checkUpload = false;
		$namefile = '';
        // dd($request->file('image'));
		if($request->hasFile('image')){
			$file = $request->file('image');
            // lay ten file
			$namefile = $file->getClientOriginalName();

			if($file->getError() == 0){
                // upload
				if($file->move(public_path('uploads/partners/'),$namefile)){
					$checkUpload = true;
				}
			}
		}

		if(!$checkUpload && $namefile == ''){

			return redirect()->route('admin.about.dashboard')->with('fail', 'Moi chon lai file');
		} else {
            // insert data
            if($lang_code == 'EN'){$menuid = 37;}else{$menuid = 23;}
			$dataInsert = [
				'title' => $title,
				'home_title' => $hometitle,
				'content' => $contentPd,
				'image' => $namefile,
				'menuid' =>$menuid,
				'lang_code' =>$lang_code,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => null
			];

			if(DB::table('partners')->insert($dataInsert)){
                // dd('thanh cong');
				return redirect()->route('admin.partner.dashboard')->with('message', 'Thêm thành công');
			} else {
				return redirect()->route('admin.partner.addAbout')->with('fail', 'Thêm mới thất bại');
			}
		}
	}
	public function destroy($id){
		$delete = Partner::find($id);
		$image= $delete->image;
		$file_path = public_path().'/uploads/partners/'.$image;
		if ($delete->delete()&& unlink($file_path)) {

			return redirect()->back()->with('message', 'Đã xóa thành công');
		}
		else{
			return view('admin.page.dashboard')->with('fail', 'Thêm mới thất bại');

		}
	}
}
