<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AboutPackage;
use Illuminate\Support\Facades\DB;	
use App\Http\Requests\AddAboutPackage;	
use App\Http\Requests\EditAboutPackage;	
use App\Models\Package;
class AboutPackageController extends Controller
{
    public function index()
    {
    	$data = AboutPackage::all();
        $packages = Package::all();
        return view('admin.aboutpackage.index', compact('data','packages'));
    }
    public function showFormAdd()
    {
        $packages = Package::all();
        $count = Package::count();
        if($count === 0){
            return redirect()->route('admin.package.showAddPackage')->with('fail', 'Bạn chưa có gói trị liệu');
        }
        else
        {
            return view('admin.aboutpackage.add', compact('packages'));
        }
    }
    public function add(AddAboutPackage $request)
    {
    	$dataInsert = new AboutPackage;
        $dataInsert->name = $request->name; 
        $dataInsert->banner = $request->image;   
        $dataInsert->packagesid = $request->packages_id;
        $dataInsert->lang_code = $request->lang_code;
        $checkUpload = false;
        $namefile = '';
        // dd($request->file('image'));
        if($request->hasFile('image')){
            $file = $request->file('image');
            // lay ten file
            $namefile = $file->getClientOriginalName();

            if($file->getError() == 0){
                // upload
                if($file->move(public_path('uploads/packages/'),$namefile)){
                    $checkUpload = true;
                }
            }
        }
        
        if(!$checkUpload && $namefile == ''){

            return redirect()->route('admin.aboutpackage.dashboard')->with('fail', 'Chọn lại file');
        } else {
            // insert data
         $dataInsert->banner = $namefile;
         if ($dataInsert->save()) {
            return redirect()->route('admin.aboutpackage.dashboard')->with('success', 'Thêm thành công');
        }
        else{
            return view('admin.aboutpackage.dashboard')->with('fail', 'Thêm mới thất bại');
        }   
    }

}

public function showFormEdit($id){
   $data = AboutPackage::findOrFail($id);
   $packages = Package::all();
   $count = Package::count();

   if ($count === 0) {

    return redirect()->route('admin.package.showAddPackage')->with('fail', 'Bạn chưa có gói trị liệu');
}
else{
    return view('admin.aboutpackage.edit', compact('data','packages'));
}
}

public function Update($id, EditAboutPackage $request){
   $find = AboutPackage::findOrFail($id);

   $name = $request->name;
   $image= $find->banner;
   $packages_id = $request->packages_id;
   $lang_code = $request->lang_code;

   $file_path = public_path().'/uploads/packages/'.$image;
   $checkUpload = false;
   $update = [
    'name' => $name,
    'packagesid' => $packages_id,
    'lang_code' => $lang_code,
    'created_at' => date('Y-m-d H:i:s'),
    'updated_at' => null
];
if($request->hasFile('image')){
    $file = $request->file('image');
            // lay ten file
    $namefile = $file->getClientOriginalName();
    if (!empty($namefile)) {
        $update['banner'] = $namefile;
        if($file->getError() == 0){
                // upload
            if($file->move(public_path('uploads/packages/'),$namefile)){
                $checkUpload = true;
            }
        }
    }

}

if(DB::table('about_packages')->where('id', $id)->update($update)){
                // dd('thanh cong');
    return redirect()->route('admin.aboutpackage.dashboard')->with('success', 'Sửa thành công');

}
}
public function destroy($id){
    $delete = AboutPackage::find($id);

    $image= $delete->banner;

    $count = DB::table('about_packages')->where('banner', '=', $image)->count();
    $file_path = public_path().'/uploads/packages/'.$image;

    if ($count>1) {
     if($delete->delete()){
        return redirect()->back()->with('success', 'Đã xóa thành công');
    }
}
else {
    if ($delete->delete() && unlink($file_path)) {

        return redirect()->back()->with('success', 'Đã xóa thành công');
    }
    else{
        return view('admin.aboutpackage.dashboard')->with('fail', 'Thêm mới thất bại');
    }
}
}
}
