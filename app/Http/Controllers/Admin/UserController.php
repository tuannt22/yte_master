<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\AddUserRequest;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
  public function index(){
   $data = User::where('id','!=', Auth::user()->id)->get();
   return view('admin.user.index',['data'=>$data]);
 }
 public function showFormAddUser()
 {
   return view('admin.user.adduser');
 }
 
 public function addUser(AddUserRequest $request){
  $dataInsert = new User;
  $dataInsert->name = $request->name; 
  $dataInsert->email = $request->email; 
  $dataInsert->password = Hash::make($request->password); 
  
  if ($dataInsert->save()) {
   return redirect()->back()->with('message', 'Thêm thành công');
 }
 else{
   return view('admin.page.login')->with('fail', 'Thêm mới thất bại');
 }   
}

public function showFormEditUser($id, Request $request){	
 $data = User::find($id);
 return view('admin.user.edit', compact('data'));
}

public function Update($id, EditUserRequest $request)
{
 $dataUp = User::find($id);
 $dataUp->name = $request->name; 
 $dataUp->email = $request->email; 
 $dataUp->password = Hash::make($request->password);

 if ($dataUp->save()) {
  return redirect()->route('admin.user.dashboard')->with('message', 'Sửa thành công');;
}
else{
 return view('admin.user.edit')->with('fail', 'Thêm mới thất bại');
}

}
public function destroy($id){
 $delete = User::find($id);
 if ($delete->delete()) {

  return redirect()->back()->with('message', 'Đã xóa thành công');
}
else{
 return view('admin.page.dashboard')->with('fail', 'Delete thất bại');
}
}
}
