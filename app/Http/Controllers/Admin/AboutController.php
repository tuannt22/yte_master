<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
use App\Http\Requests\AddAboutRequest;
use App\Http\Requests\EditAboutRequest;
use Illuminate\Support\Facades\DB;
use App\Models\Menu;
class AboutController extends Controller
{
    public function index(){
    	// $data = DB::table('services')
     //        ->join('banners', 'services.banner_id', '=', 'banners.id')
     //        ->select('services.*', 'banners.name')
     //        ->paginate(2);
        $data = DB::table('abouts')->whereNotIn('id',[8, 9, 10, 31, 32, 33])
                                    ->get();
        // dd($data);
         return view('admin.about.index', compact('data'));
         
    }

    public function show($id)
    {
        $show = About::findOrFail($id);
        return view('admin.about.show', compact('show'));
    }
    public function showFormEdit($id){
    	$data = About::find($id);
        $menu = Menu::whereIn('id', [23])->get();
    	return view('admin.about.edit', compact('data'), compact('menu'));
    }

    public function Update($id, EditAboutRequest $request)
    {
    	$title = $request->title;
        $image = $request->image;
        $links = $request->links;
        $contentPd = $request->content;
        $menu_id = $request->menu_id;
        $lang_code = $request->lang_code;
        if($lang_code == 'EN'){$menuid = 37;}else{$menuid = 23;}
        $checkUpload = false;
         $update = [
                'title' => $title,
                'content' => $contentPd,
                
                'links' => $links,
                'menuid'=>$menuid,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ];
        // dd($request->file('image'));
        if($request->hasFile('image')){
            $file = $request->file('image');
            // lay ten file
            $namefile = $file->getClientOriginalName();
            if (!empty($namefile)) {
                $update['image'] = $namefile;
                if($file->getError() == 0){
                // upload
                if($file->move(public_path('uploads/abouts/'),$namefile)){
                    $checkUpload = true;
                }
            }
            }
            
        }
            // insert data
            if(DB::table('abouts')->where('id', $id)->update($update)){
                // dd('thanh cong');
                return redirect()->route('admin.about.dashboard')->with('message', 'Sửa thành công');
        }
    	
    }
    public function addAbout()
    {
        $data = Menu::whereIn('id', [23])->get();
        return view('admin.about.add', compact('data'));
    }

    public function add(AddAboutRequest $request){

       $title = $request->title;
        $image = $request->image;
        $links = $request->links;
        $contentPd = $request->content;
        $menu_id = $request->menu_id;
        $lang_code = $request->lang_code;
        $checkUpload = false;
        $namefile = '';
        // dd($request->file('image'));
        if($request->hasFile('image')){
            $file = $request->file('image');
            // lay ten file
            $namefile = $file->getClientOriginalName();

            if($file->getError() == 0){
                // upload
                if($file->move(public_path('uploads/abouts/'),$namefile)){
                    $checkUpload = true;
                }
            }
        }

        if(!$checkUpload && $namefile == ''){
            
            return redirect()->route('admin.about.dashboard')->with('fail', 'Moi chon lai file');
        } else {
            // insert data
            if($lang_code == 'VI'){$menuid = 23;}else{$menuid = 37;}
            $dataInsert = [
                'title' => $title,
                'content' => $contentPd,
                'content' => $contentPd,
                'image' => $namefile,
                'links' => $links,
                'menuid'=>$menuid,
                'lang_code'=>$lang_code,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ];
          
            if(DB::table('abouts')->insert($dataInsert)){
                return redirect()->route('admin.about.dashboard')->with('message', 'Thêm thành công');
            } else {
                return redirect()->route('admin.about.addAbout')->with('fail', 'Thêm mới thất bại');
            }
        }
    }
    public function destroy($id){
        $delete = About::find($id);
        $image= $delete->image;
        $file_path = public_path().'/uploads/abouts/'.$image;
        if ($delete->delete()&& unlink($file_path)) {
            
            return redirect()->back()->with('message', 'Đã xóa thành công');
            }
        else{
            return view('admin.page.dashboard')->with('fail', 'Thêm mới thất bại');
        }
    }
}
