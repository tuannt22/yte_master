<?php

namespace App\Http\Controllers\Admin;

use App\Models\BlogBanner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = BlogBanner::paginate(15);
        return view('admin.blogbanner.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogbanner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'content' => 'required',
        ],
            [
                'required' => 'The :attribute field is required.',
            ]
        );
        $data = $request->all();
        unset($data['_token']);
        if (BlogBanner::insert($data)) {
            return redirect()->route('admin.blog_banner.index')->with('message', 'Thêm mới thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = BlogBanner::find($id);
        return view('admin.blogbanner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'content' => 'required',
        ],
            [
                'required' => 'The :attribute field is required.',
            ]
        );
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);
        if (BlogBanner::where('id', '=', $id)->update($data)) {
            return redirect()->route('admin.blog_banner.index')->with('message', 'Chỉnh sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (BlogBanner::find($id)->delete()) {
            return redirect()->route('admin.blog_banner.index')->with('message', 'Xoá thành công');
        }
    }

    public function active(Request $request) {
        if ($request->id) {
            $blogBanner = BlogBanner::select('status')->where('id', '=', $request->id)->first();
            if ($blogBanner->status == 0) {
                BlogBanner::where('id', '=', $request->id)->update(['status' => 1]);
                BlogBanner::where('id', '<>', $request->id)->update(['status' => 0]);
            } elseif ($blogBanner->status == 1) {
                BlogBanner::where('id', '=', $request->id)->update(['status' => 0]);
            }
            return 'success';
        }
    }
}
