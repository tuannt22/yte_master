<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Services_detail;
use App\Models\Service;
use App\Http\Requests\AddServiceDetailRequest;
use Illuminate\Support\Facades\DB;

class Services_detailController extends Controller
{
    //
	public function index(){
    	
		$data = Service::all();
        // dd($data);
		return view('admin.services_detail.index', compact('data'));

	}

	public function list($id)
	{

		$data = DB::table('services')
            ->join('services_detail', 'services_detail.services_id', '=', 'services.id')
            ->where('services.id','=',$id)
            ->get();

         $find = Service::findOrFail($id);
         
		return view('admin.services_detail.list', compact('data', 'find'));
		
	}
    public function show($id)
    {
        $show = Services_detail::findOrFail($id);
        return view('admin.services_detail.show', compact('show'));
    }
	public function showFormEdit($id){
    	// $data = Service::find($id);
		$service = Service::all();
		$data = Services_detail::findOrFail($id);
		if ($data) {

			return view('admin.services_detail.edit', compact('data', 'service'));
			
		}
		else {
			return redirect()->route('admin.service.dashboard')->with('message','Không thể sửa, gói trị liệu của bạn đã bị xoá');
		}
	}

	public function Update($id, Request $request)
	{
		$dataUp = Services_detail::find($id);
		$services_id = $dataUp->services_id;
        $dataUp->title_con = $request->title_con;
		$dataUp->content = $request->content;
		$dataUp->links = $request->links;
		$dataUp->services_id = $request->services_id;
		$dataUp->lang_code = $request->lang_code;

		 if ($dataUp->save()) {
              return redirect()->route('admin.services_detail.list',$services_id )->with('message', 'Sửa thành công');
        }
        else{
                return view('admin.services_detail.showServices_detail')->with('fail', 'Sửa thất bại');
        }  

	}
	public function addServices_detail($id)
	{
        
		$find = Service::find($id);
		if ($find) {
			return view('admin.services_detail.add', compact('find'));
		}
	}

	public function add($id, AddServiceDetailRequest $request){
    	// dd($request->all());
    	$find = Services_detail::find($id);
		$dataInsert = new Services_detail;
		$dataInsert->title_con = $request->title_con;
		$dataInsert->content = $request->content;
		$dataInsert->links = $request->links;
		$dataInsert->services_id = $request->services_id;
		$dataInsert->lang_code = $request->lang_code;

		
      if ($dataInsert->save()) {

                return redirect()->route('admin.services_detail.list',$id)->with('message', 'Thêm bài viết thành công');
            }
        else{
                return view('admin.services_detail.add')->with('message', 'Thêm mới thất bại');
        }  

		
	}
	public function destroy($id){
		$delete = Services_detail::find($id);

		if ($delete->delete()) {
			return redirect()->back()->with('message', 'Đã xóa thành công');
		}
		else{
			return view('admin.page.dashboard')->with('fail', 'Thêm mới thất bại');
		}
	}
}
