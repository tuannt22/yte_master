<?php

    namespace App\Http\Controllers\Admin;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\DB;
    use App\Models\AboutService;
    use App\Models\Service;
    use App\Http\Requests\AddAboutServiceREquest;
    use App\Http\Requests\EditAboutServiceRequest;

    class AboutServiceController extends Controller
        {
           public function index()
           {
            $count = AboutService::count();

            $data = DB::table('service_homes')
            ->join('services', 'service_homes.services_id', '=', 'services.id')
            ->select('services.*', 'service_homes.*')
            ->get();
                    // dd($data);
            	// $data = AboutService::all();

            	// dd($data);

                 // $find = Service::findOrFail($id);

            	// return view('admin.aboutservice.index', compact('data', 'find'));
            return view('admin.aboutservice.index', compact('data', 'count'));
        }

        public function show($id)
        {

           $show = AboutService::findOrFail($id);

           return view('admin.aboutservice.show', compact('show'));
        }
        public function showFormEdit($id){

            	//$find = Service::findOrFail($id);
           $services = Service::all();

           $data = AboutService::findOrFail($id);

           if (!$services) {
             return redirect()->route('admin.aboutservice.dashboard')->with('message','Không thể sửa, dịch vụ của bạn của bạn đang trống');
         }
         else {
             return view('admin.aboutservice.edit', compact('data', 'services'));
         }
        }           


        public function Update($id, EditAboutServiceRequest $request)
        {

          $dataUp = AboutService::find($id);
          $dataUp->services_id = $request->optServies;
          $dataUp->content = $request->content;
          $dataUp->lang_code = $request->lang_code;
          $checkUpload = false;

          if($request->hasFile('image')){
            $file = $request->file('image');
            $namefile = $file->getClientOriginalName();

            if (!empty($namefile)) {
                $dataUp->image = $namefile;
                if($file->getError() == 0){
                        // upload
                    if($file->move(public_path('uploads/about_service/'),$namefile)){
                        $checkUpload = true;
                    }
                }
            }

        }
          $dataUp->save();
          return redirect()->route('admin.about_service.dashboard')->with('message', 'Sửa thành công');


        }
        public function showAdd()
        {

          $service = Service::all();
          if ($service) {
             return view('admin.aboutservice.add', compact('service'));
         }
         else{
            dd('Bạn cần có tạo tiêu đề bài viết trước.');
            return redirect()->back();
        }

        }

        public function add( AddAboutServiceREquest $request){
            	// dd($request->all());
          $dataInsert = new AboutService;
          $dataInsert->image = $request->image;
          $dataInsert->services_id = $request->services_id;
          $dataInsert->lang_code = $request->lang_code;
        		// $dataInsert->title = $request->title;
          //       $dataInsert->link = $request->link;
          $dataInsert->content = $request->content;


          $checkUpload = false;
          $namefile = '';
                // dd($request->file('image'));
          if($request->hasFile('image')){
            $file = $request->file('image');
                    // lay ten file
            $namefile = $file->getClientOriginalName();

            if($file->getError() == 0){
                        // upload
                if($file->move(public_path('uploads/about_service/'),$namefile)){
                    $checkUpload = true;
                }
            }
        }
        if(!$checkUpload && $namefile == '')
        {
                    // dd('that bai');
            $request->session()->flash('errUpload', 'Vui long chon file upload');
            return redirect()->route('admin.about_service.dashboard',['state'=>'fail']);
        } 
        else 
        {
          $dataInsert->image = $namefile;
          if ($dataInsert->save()) {
           return redirect()->route('admin.about_service.dashboard')->with('message', 'Thêm thành công');
        }
        else
        {
           return view('admin.about_service.add')->with('message', 'Thêm mới thất bại');
        }             
        }


        }
        public function destroy($id){
          $delete = AboutService::find($id);
          if ($delete->delete()) {
             return redirect()->back()->with('message', 'Đã xóa thành công');
         }
         else{
             return view('admin.about_service.dashboard')->with('fail', 'Thêm mới thất bại');
         }
        }
        }
