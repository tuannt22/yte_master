<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;

class NewsController extends Controller
{
    //
    public function index(){
    	
		$data = News::all();
        // dd($data);	
		return view('admin.news.index', compact('data'));
	}

	    public function show($id)
    {
        $show = News::findOrFail($id);
        return view('admin.news.show', compact('show'));
    }

	public function showFormEdit($id){
    	// $data = Service::find($id);
		$news = News::all();
		$data = News::findOrFail($id);
		if ($data) {

			return view('admin.news.edit', compact('data', 'news'));
			
		}
		else {
			return redirect()->route('admin.news.dashboard')->with('message','Error');
		}
	}
		public function Update($id, Request $request)
	{
		$dataUp = News::find($id);
        $dataUp->title = $request->title;
		$dataUp->content = $request->content;
		$dataUp->lang_code = $request->lang_code;

		 if ($dataUp->save()) {
              return redirect()->route('admin.news.show',$id)->with('message', 'Sửa thành công');
        }
        else{
                return redirect()->route('admin.news.dashboard')->with('fail', 'Sửa thất bại');
        }  

	}


	 public function addNews(){

        return view('admin.news.add');
    }

		public function add(Request $request){
		$dataInsert = new News;
		$dataInsert->title = $request->title;
		$dataInsert->content = $request->content;
		$dataInsert->lang_code = $request->lang_code;

      if ($dataInsert->save()) {

                return redirect()->route('admin.news.dashboard')->with('message', 'Thêm bài viết thành công');
            }
        else{
                return view('admin.news.add')->with('message', 'Thêm mới thất bại');
        }  

		
	}

	public function destroy($id){
	$delete = News::find($id);

	if ($delete->delete()) {
		return redirect()->route('admin.news.dashboard')->with('message', 'Đã xóa thành công');
	}
	else{
			return view('admin.news.dashboard')->with('fail', 'Không thể xoá');
		}
	}
}
