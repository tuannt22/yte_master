<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Http\Requests\AddPackagePost;
use App\Http\Requests\EditPackagePost;
use Illuminate\Support\Facades\DB;
use App\Models\Menu;
use App\Models\Service;

class PackagesController extends Controller
{
    public function showDataPackage(){
        $data = Package::paginate(5);
        $count = Package::count();
        $menus = Package::with('menus')->get();

        return view('admin.package.index', compact('data','menus','count'));
    }
    public function showDetailPackage($id){
        $data = Package::find($id);
        // $data1 = Package::find($id);
        return view('admin.package.detail', compact('data'));
    }
    public function showFormAddPackage()
    {
        $data = Menu::whereIn('id', [30])->get();
        $service = Service::all();
        return view('admin.package.addpackage',compact('data', 'service'));
    }
    public function addPackage(AddPackagePost $request){
        $dataInsert = new Package;
        $dataInsert->name = $request->name; 
        $dataInsert->lang_code = $request->lang_code; 
        $dataInsert->address = $request->address; 
        $dataInsert->services_id = $request->services_id; 
        $dataInsert->content = $request->content; 
        $dataInsert->date = $request->date;
        $dataInsert->menuid = $request->menu_id;
        
       
          
        if ($dataInsert->save()) {
            return redirect()->route('admin.package.showPackage')->with('message', 'Thêm thành công');
        }
        else{
             return view('admin.page.login')->with('fail', 'Thêm mới thất bại');
        }   
        
       
    }

    public function showFormEditPackage($id){ 
        $data = Package::findOrFail($id);
        $menu = Menu::whereIn('id', [30])->get();
        $service = Service::all();
        return view('admin.package.edit', compact('data','service'), compact('menu'));
    }

    public function update($id, EditPackagePost $request)
    {
        $find = Package::find($id);
        $name = $request->name; 
        $services_id = $request->services_id; 
        $address = $request->address; 
        $content = $request->content; 
        $lang_code = $request->lang_code; 
        $date = $request->date;
        $menuid = $request->menu_id;

       
        
        $update = [
            'name' => $name,
            'content' => $content,
            'address' => $address,
            'date' => $date,
            'services_id' => $services_id,
            'lang_code' => $lang_code,
            'menuid' => $menuid,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => null
        ];
          
        if(DB::table('packages')->where('id', $id)->update($update)){
                // dd('thanh cong');
            return redirect()->route('admin.package.showPackage')->with('message', 'Update thành công');
        } else {
            return redirect()->route('admin.package.edit')->with('fail', 'thất bại');
        }

    }

    public function destroy($id){
        $delete = Package::find($id);

        if ($delete->delete()) {

            return redirect()->route('admin.package.showPackage')->with('message', 'Đã xóa thành công');
        }
        else{
            return view('admin.package.dashboard')->with('fail', 'Thêm mới thất bại');
        }
    }

}
