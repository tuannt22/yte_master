<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Library;
use DB;

use App\Http\Requests\AddLibraryRequest;
use App\Http\Requests\EditLibraryRequest;

class LibraryController extends Controller
{
    //
    public function index(){
		$data = Library::paginate(6);
        // dd($data);	
		return view('admin.library.index', compact('data'));
	}

	public function addLibrary(){
		$data = Library::all();
		return view('admin.library.add', compact('data'));
	}

	public function add(AddLibraryRequest $request){
		$data = new Library;
        $data->name = $request->name;  
        $data->document = $request->document; 
        $data->lang_code = $request->lang_code; 
        $data->sharers = $request->sharers;
        $checkUpload = false;
        $namefile = '';

        if($request->hasFile('document')){
            $file = $request->file('document');
            // lay ten file
            $namefile = $file->getClientOriginalName();

            if($file->getError() == 0){
                // upload
                if($file->move(public_path('uploads/library/'),$namefile)){
                    $checkUpload = true;
                }
            }
        }

        if(!$checkUpload && $data == ''){

            return redirect()->back()->with('fail', 'Error');
        } else {
            // insert data
         $data->document = $namefile;

         if($data->save()){
            return redirect()->route('admin.library.dashboard')->with('message', 'Thêm thành công');
        } else {
            return redirect()->route('admin.library.add')->with('fail', 'Thêm mới thất bại');
        }
    }	
    }

    public function showFormEdit($id){
		$library = Library::all();
		$data = Library::findOrFail($id);
		if ($data) {
			return view('admin.library.edit', compact('data', 'library'));
		}
		else {
			return redirect()->route('admin.library.dashboard')->with('message','Error');
		}
	}

    public function Update($id, EditLibraryRequest $request)
    {
    	$name = $request->name;
        $document = $request->document;
        $lang_code = $request->lang_code;
        $sharers = $request->sharers;
        $checkUpload = false;
        $update = [
                'name' => $name,
                'document' => $document,
                'sharers' => $sharers,
                'lang_code' => $lang_code,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => null
            ];

        if($request->hasFile('document')){
            $file = $request->file('document');
            // lay ten file
            $namefile = $file->getClientOriginalName();
            if (!empty($namefile)) {
                $update['document'] = $namefile;
                if($file->getError() == 0){
                // upload
                if($file->move(public_path('uploads/library/'),$namefile)){
                    $checkUpload = true;
                }
            }
            }
            
        }
            // insert data
            if(DB::table('library')->where('id', $id)->update($update)){
                // dd('thanh cong');
                return redirect()->route('admin.library.dashboard')->with('message', 'Sửa thành công');
        }
    	
    }

    public function destroy($id){
        $delete = Library::find($id);
        $document= $delete->document;
        $file_path = public_path().'/uploads/library/'.$document;
        if ($delete->delete()) {
            
            return redirect()->back()->with('message', 'Đã xóa thành công');
            }
        else{
            return view('admin.library.dashboard')->with('fail', 'Xoá thất bại');
        }
    }

    public function getDownload($id){

    	$download = Library::findOrFail($id);

        $document= $download->document;

        //PDF file is stored under project/public/download/info.pdf
        $file_path = public_path().'/uploads/library/'.$document;

        return response()->download($file_path);
	}
    
}
