<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Banner;
use DB;
use App\Http\Requests\AddMenuRequest;
use App\Http\Requests\EditMenuRequest;
class MenuController extends Controller
{
    public function index(){
    	
        $data = Menu::all();
        
            //dd($data);
        return view('admin.menu.index', compact('data'));
    }

    public function showFormAddMenu(){

        return view('admin.menu.add');
    }
    public function addMenu(AddMenuRequest $request){
        $data = new Menu;
        $data->name = $request->name;  
        $data->links = $request->links; 
        $data->image = $request->image;
        $data->lang_code = $request->lang_code;
        $checkUpload = false;
        $namefile = '';

        if($request->hasFile('image')){
            $file = $request->file('image');
            // lay ten file
            $namefile = $file->getClientOriginalName();

            if($file->getError() == 0){
                // upload
                if($file->move(public_path('uploads/menus/'),$namefile)){
                    $checkUpload = true;
                }
            }
        }

        if(!$checkUpload && $data == ''){

            return redirect()->back()->with('fail', 'Error');
        } else {
            // insert data
         $data->image = $namefile;

         if($data->save()){
            return redirect()->route('admin.menu.dashboard')->with('message', 'Thêm thành công');
        } else {
            return redirect()->route('admin.menu.add')->with('fail', 'Thêm mới thất bại');
        }
    }
}



public function showFormEditMenu($id){
   $data = Menu::find($id);   
   return view('admin.menu.edit', compact('data'));
}
public function Update(EditMenuRequest $request, $id)
{
    $name = $request->name;
    $links = $request->links;
    $image = $request->image;
    $lang_code = $request->lang_code;

    $checkUpload = false;
    $update = [
            'name' => $name,
            'links' => $links,
            'lang_code' => $lang_code,           
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => null
        ];
        //dd($request->file('image'));
    if($request->hasFile('image')){
        $file = $request->file('image');
            // lay ten file
        $namefile = $file->getClientOriginalName();
        if (!empty($namefile)) {
            $update['image'] = $namefile;
            if($file->getError() == 0){
                // upload
            if($file->move(public_path('uploads/menus/'),$namefile)){
                $checkUpload = true;
            }
        }
        }
        
    }
        if(DB::table('menus')->where('id', $id)->update($update)){
                // dd('thanh cong');
            return redirect()->route('admin.menu.dashboard')->with('message', 'Sửa thành công');
        } 
    }






public function destroy($id){
   $delete = Menu::find($id);
   if($delete->delete()){
      return redirect()->back()->with('message','Xoá thành công');
  }
  else{
      return view('admin.menu.dashboard')->with('fail','Xoá thất bại, thử lại');
  }
}
}
