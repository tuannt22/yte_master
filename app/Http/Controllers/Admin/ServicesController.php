<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Package;
use App\Models\Services_detail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\EditServiceRequest;
use App\Http\Requests\AddServiceRequest;
use Illuminate\Support\Facades\Storage;
use File;
use App\Models\Menu;
class ServicesController extends Controller
{
    public function index(){
        $data = Service::all();
        return view('admin.services_detail.index', compact('data'));

    }

    public function show($id)
    {
        $show = Service::findOrFail($id);
        return view('admin.service.show', compact('show'));
    }
    public function showFormEdit($id){
    	// $data = Service::find($id);
    	$package = Package::all();
        $menu = Menu::whereIn('id', [29])->get();
        $data = Service::findOrFail($id);
        if ($data) {


            return view('admin.service.edit', compact('data', 'package', 'menu'));
            return redirect()->back();
        }
        else {
            return redirect()->route('admin.service.dashboard')->with('message','Không thể sửa, gói trị liệu của bạn đã bị xoá');
        }
    }

    public function Update($id, EditServiceRequest $request)
    {
    	$titlePd = $request->title_pa;
        $package_id = $request->package_id;     
        $menu_id = $request->menu_id;
        $link = $request->link;
        $content = $request->content;
        $lang_code = $request->lang_code;
        if($lang_code == 'EN'){$menuid = 37;}else{$menuid = 23;}
        $checkUpload = false;
        $dataUp = [
            'title' => $titlePd,
            'menuid' =>$menuid,
            'link' =>$link,
            'content' =>$content,
            'lang_code' =>$lang_code,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => null
        ];
        // dd($request->file('image'));
        if($request->hasFile('image')){
            $file = $request->file('image');
            // lay ten file
            $namefile = $file->getClientOriginalName();
            if (!empty($namefile)) {
                $dataUp['icon'] = $namefile;
                if($file->getError() == 0){
                // upload
                    if($file->move(public_path('uploads/services/'),$namefile)){
                        $checkUpload = true;
                    }
                }
            }
            
        }
        
        if(DB::table('services')->where('id', $id)->update($dataUp)){
                // dd('thanh cong');
            return redirect()->route('admin.service.dashboard')->with('message', 'Sửa thành công');
        } 
    }


public function addService()
{

    $package = Package::all();
    $data = Menu::whereIn('id', [29])->get();
    return view('admin.service.add', compact('package','data'));
}

public function add(AddServiceRequest $request){
   $titlePd = $request->title_pa;
     // $namefile = $request->image;   
   $package_id = $request->package_id;
   $menu_id = $request->menu_id;
   $link = $request->link;
   $content = $request->content;
   $lang_code = $request->lang_code;

   $checkUpload = false;
   $namefile = '';
        // dd($request->file('image'));
   if($request->hasFile('image')){
    $file = $request->file('image');
            // lay ten file
    $namefile = $file->getClientOriginalName();

    if($file->getError() == 0){
                // upload
        if($file->move(public_path('uploads/services/'),$namefile)){
            $checkUpload = true;
        }
    }
}
if(!$checkUpload && $namefile == ''){

    return redirect()->route('admin.service.dashboard')->with('fail', 'Moi chon lai file');
} else {
            // insert data
    if($lang_code == 'EN'){$menuid = 38;}else{$menuid = 29;}
    $dataInsert = [
        'title' => $titlePd,
        'icon' => $namefile,         
        'menuid' => $menuid,
        'link' => $link,
        'content' => $content,
        'lang_code' => $lang_code,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => null
    ];
            // dd($dataInsert);
            // die();
    if(DB::table('services')->insert($dataInsert)){
                // dd('thanh cong');
        return redirect()->route('admin.services_detail.dashboard')->with('message', 'Thêm thành công');
    } else {
                // dd('that bai');
        return redirect()->route('admin.service.addService')->with('fail', 'Thêm mới thất bại');
    }
}
}
public function destroy($id){
    $deleteService = Service::find($id);
    $find = Services_detail::where('services_id', '=',  $id)->count();
    $image= $deleteService->icon;
    $file_path = public_path().'/uploads/services/'.$image;

    if($find === 0){
        if ($deleteService->delete() &&unlink($file_path)) {
            return redirect()->back()->with('message', 'Đã xóa thành công');
        }
        else{
            return view('admin.page.dashboard')->with('fail', 'Thêm mới thất bại');
        }
    }
    else{
     return redirect()->back()->with('fail', 'Bạn phải xóa bài viết trước rồi mới xóa tiêu đề');
 }
}
}
