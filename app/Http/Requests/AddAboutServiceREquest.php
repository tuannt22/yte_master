<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddAboutServiceREquest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpeg,bmp,png',
            'content' =>'required',
            // 'title' => 'required',
            'services_id' => 'required',
        ];
    }
    public function messages(){
        return [
            'image.required' => 'Chưa có ảnh',
            'image.mimes' => 'Chưa đúng định dạng ảnh',
            'content.required' => 'Bạn chưa nhập nội dung miêu tả',
            
        ];
    }
}
