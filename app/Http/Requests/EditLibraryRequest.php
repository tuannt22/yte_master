<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditLibraryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'document' => 'required|mimes:csv,doc,docx,djvu,odp,ods,odt,pps,ppsx,ppt,pptx,p df,ps,eps,rtf,txt,wks,wps,xls,xlsx,xps ',
            'sharers' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên tài liệu',
            'document.required'   => 'Yêu cầu chọn lại file',
            'document.mimes'  => 'File chưa đúng định dạng, mời bạn chọn lại',
            'sharers.required' => 'bạn chưa nhập tên người đăng tài liệu'
        ];
    }
}
