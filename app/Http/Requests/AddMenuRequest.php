<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'links' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png',
        ];
    }

    public function messages(){
       return [
            'name.required' => 'Bạn chưa nhập tên menu',
            'links.required'    => 'Bạn chưa nhâp link',
            'image.required' => 'Bạn chọn ảnh',
            'image.mimes'   =>'Chưa đúng định dạng ảnh mời chọn lại',
       ];
    }
}
