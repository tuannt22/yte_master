<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddAboutPackage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên gói điều trị',
            'image.required'   => 'Bạn chưa chọn file',
            'image.mimes'  => 'File chưa đúng định dạng, mời bạn nhập lại',
        ];
    }
}
