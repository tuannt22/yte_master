<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditPackagePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name' => 'required|min:3|max:60',
            'address' => 'required|min:3',
            'date' => 'required|min:3',
            'content' =>'required',
            'menu_id' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'title_pa.min' => 'Tiêu đề phải 3 ký tự trở lên',
            'title_pa.required' => 'Tiêu đề không được để trống',
            'package_id' =>'Bạn chưa chọn gói điều trị',
            'content.required' => 'Bạn chưa có nội dụng miêu tả',
            'content.min' => 'Nội dung cần 3 ký tự trở lên',
            'menu_id' =>'Bạn chưa chọn tên menu ',
        ];
    }
}
