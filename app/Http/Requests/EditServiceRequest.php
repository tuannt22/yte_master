<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_pa' => 'required|min:3',
            'image' => 'nullable|image',
            'menu_id' => 'required',
        ];
    }

    public function messages(){
        return[
            'title_pa.min' => 'Tieu de cha phai lon hon 3 kys tu',
            'title_pa.required' => 'Tieu de khong duoc de trong',
           
            'menu_id.requuired' => 'Bạn chưa chọn menu',
        ];
    }
}
