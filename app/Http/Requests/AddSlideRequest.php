<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|mimes:jpeg,bmp,png',
            'name' => 'required',
            'title' => 'required',
            'links' => 'required',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Bạn chưa có tên silde',
            'title.required' => 'Bạn chưa nhập tiêu đề',
            'image.required' => 'Bạn chưa chọn ảnh',
            'image.mimes'   => 'Định dạng ảnh không đúng mời chọn lại',
            'links.required' => 'Bạn chưa nhập link',
        ];
    }
}
