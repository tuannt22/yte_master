<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'tel' => 'required|numeric',
            'subject' => 'required',
        ];
    }

    public function messages()
    {
        return [
          'name.required' => __('home.name_required'),
          'email.required' => __('home.email_required'),
          'email.email' => __('home.email_email'),
          'tel.required' => __('home.tel_required'),
          'tel.numeric' => __('home.tel_numeric'),
          'subject.required' => __('home.subject_required')
        ];
    }
}
