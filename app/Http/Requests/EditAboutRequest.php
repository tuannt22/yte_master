<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditAboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'image' => 'nullable|image',
            'links' => 'required',
            'content' =>'required',
             'menu_id' => 'required',
        ];
    }

    public function messages(){
        return [
            'title.min' => 'Tiêu đề phải 3 ký tự trở lên',
            'title.required' => 'Tiêu đề không được để trống',
           
            'links.required' =>'Link không được để trống',
            'content.required' => 'Bạn chưa nhập nội dung miêu tả',
            'menu_id' =>'Bạn chưa chọn menu',
        ];
    }
}
