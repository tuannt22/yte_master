<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_pa' => 'required|min:3',
            'image' => 'required|mimes:jpeg,bmp,png',
            'menu_id' => 'required',
        ];
    }
    public function messages(){
        return[
            'title_pa.min' => 'Tiêu đề phải 3 ký tự trở lên',
            'title_pa.required' => 'Tiêu đề không được để trống',
            'image.required' => 'Chưa có ảnh',
            'image.mimes' => 'Chua dung dinh dang anh',
            'menu_id.required' =>'Bạn chưa chọn tên menu ',
        ];
    }
}
