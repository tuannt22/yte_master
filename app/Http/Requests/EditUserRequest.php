<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:5|',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên người dùng',
            'email.email' => 'Không đúng định dạng email',
            'email.unique' => 'Đã có người dùng email này rồi',
            'password.required' => 'Bạn chưa nhập mật khẩu',
            'password.min'  => 'Mật khẩu phải 5 ký tự trở lên',
        ];
    }
}
