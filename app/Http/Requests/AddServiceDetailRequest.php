<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddServiceDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_con' => 'required',
            'content' => 'required',
            'links' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title_con'=> 'Bạn chưa có tiêu đề',
            'content'  => 'Bạn chưa nhập nội dung',
            'links' => 'Bạn chưa nhập link',
            
        ];
    }
}
