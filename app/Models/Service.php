<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table='services';
    
    public function packages(){
    	return $this->hasOne('App\Models\Package', 'package_id', 'id');
    }

   
    public function menus(){
        return $this->belongsTo('App\Models\Menu', 'menuid', 'id');
    }

    public function services_detail(){
        return $this->belongsTo('App\Models\Services_detail', 'services_id', 'id');
    }
     public function about_service(){
        return $this->belongsTo('App\Models\AboutService', 'services_id', 'id');
    }


}
