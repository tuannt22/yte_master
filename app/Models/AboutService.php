<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutService extends Model
{
    protected $table = 'service_homes';
    
    public function services(){
        return $this->hasMany('App\Models\Service', 'services_id', 'id');
    }

}
