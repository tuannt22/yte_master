<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table ='menus';
     
    public function abouts(){
    	return $this->hasMany('App\Models\About','menuid','id');
    }
    public function packages(){
    	return $this->hasMany('App\Models\Package','menuid','id');
    }
    public function partners(){
    	return $this->hasMany('App\Models\Partner','menuid','id');
    }
    public function services(){
    	return $this->hasMany('App\Models\Service','menuid','id');
    }
    
}
