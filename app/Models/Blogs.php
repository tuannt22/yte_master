<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blogs extends Model
{
    protected $table = 'blogs';

    public function topics () {
        return $this->belongsTo('App\Models\Topics', 'topic_id', 'id');
    }

    public function blogTag() {
        return$this->belongsToMany('App\Models\Tags', 'blog_tag', 'blog_id', 'tag_id');
    }
}
