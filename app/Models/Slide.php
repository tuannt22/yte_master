<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
     protected $table ='slides';

     public function packages(){
    	return $this->belongsTo('App\Models\Package','slideid','id');
    }
}
