<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services_detail extends Model
{
    //
    protected $table = 'services_detail';

    public function services(){
        return $this->HasMany('App\Models\Service', 'services_id', 'id');
    }
}
