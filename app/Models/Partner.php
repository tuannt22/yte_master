<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
     protected $table ='partners';

     
    public function menus(){
    	return $this->belongsTo('App\Models\Menu','menuid','id');
    }
}
