<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogBanner extends Model
{
    protected $table = 'blog_banner';
}
