<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AboutPackage extends Model
{
    protected $table = 'about_packages';

     public function packages(){
    	return $this->belongsTo('App\Models\Package','packagesid','id');
    }
}
