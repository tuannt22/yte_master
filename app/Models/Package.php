<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Service;
use App\Models\Slide;
class Package extends Model
{
     protected $table ='packages';
     protected $guarded  = ['id'];
     
    public function services(){
    	return $this->belongsTo(Service::class);
    }

    public function menus(){
    	return $this->belongsTo('App\Models\Menu','menuid','id');    
    }

    public function packages(){
    	return $this->hasOne('App\Models\Package','packagesid','id');
    }
}
