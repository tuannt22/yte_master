<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerComments extends Model
{
    protected $table = 'customer_comments';
}
