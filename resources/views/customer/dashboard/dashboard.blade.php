<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-language" content="jp">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{asset('')}}">
    <title> Infinity Healthcare Vietnam</title>
    <link rel="shortcut icon" type="image/x-icon" href="content/img/favicon.ico">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700&amp;subset=vietnamese"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700&display=swap"
          rel="stylesheet">
    <script type="text/javascript " src="plugins/jquery/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="plugins/boostrap/bootstrap.min.css">
    <script type="text/javascript" src="plugins/boostrap/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="plugins/menu/styles.css">
    <script type="text/javascript" src="plugins/menu/script.js"></script>
    <script type="text/javascript" src="plugins/owl-carausel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="content/js/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="plugins/owl-carausel/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="plugins/owl-carausel/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="content/css/styles.css">
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5d635b0877aa790be330b171/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <style>
        body {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .fixed-footer{
            position:fixed;
            bottom: 0;
            width: 100%;
            z-index: 10
        }
    </style>


    <script type="text/javascript">
        //<=!=[=C=D=A=T=A=[
        document.onkeypress = function (event) {
            event = (event || window.event);
            if (event.keyCode === 123) {
                //alert('No F-12');
                return false;
            }
        };
        document.onmousedown = function (event) {
            event = (event || window.event);
            if (event.keyCode === 123) {
                //alert('No F-keys');
                return false;
            }
        };
        document.onkeydown = function (event) {
            event = (event || window.event);
            if (event.keyCode === 123) {
                //alert('No F-keys');
                return false;
            }
        };

        function contentprotector() {
            return false;
        }

        function mousehandler(e) {
            var myevent = (isNS) ? e : event;
            var eventbutton = (isNS) ? myevent.which : myevent.button;
            if ((eventbutton === 2) || (eventbutton === 3))
                return false;
        }

        document.oncontextmenu = contentprotector;
        document.onmouseup = contentprotector;
        var isCtrl = false;
        window.onkeyup = function (e) {
            if (e.which === 17)
                isCtrl = false;
        }

        window.onkeydown = function (e) {
            if (e.which === 17)
                isCtrl = true;
            if (((e.which === 85) || (e.which === 65) || (e.which === 88) || (e.which === 67) || (e.which === 86) || (e.which === 83)) && isCtrl === true) {
                return false;
            }
        }
        isCtrl = false;


        //]=]=> </script>
</head>
<body>
<header class="container-header" id="header">
    <div class="container hidden-xs">
        <div class="row ">
            <div class="header-top">
                <div class="col-md-3 col-sm-3 col-xs-6 col-md">
                    <div class="logo">
                        <a href="/">
                            <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                            <img class="img-logo" src="content/img/color_logo_2.png">
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9" style="padding-left: 0;padding-right: 0">
                    <div class="col-sm-3 hidden-xs hidden-lg hidden-md" style="padding: 0">
                        <div class="hotline">
                        <span class="montserrat">
                            <img src="content/img/telephone.png" class="tel">
                            <img src="content/img/telephone (1).png" class="tel1">
                            <a href="tel:19003053"> 1900 3053</a>
                        </span>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-9 col-sm-9 ms-right" style="padding: 15px 0 0">
                        <div class="menu-top hide-fix">
                            <ul>
                                <li><a href="/news">{{__('menus.news')}}</a></li>
                                <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.faq')}}</a></li>
                                <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                <li class="social">
                                    <a style="padding: 5px 10px;" href="https://www.facebook.com/infinityhealthcarevietnam/"><i
                                            class="fa fa-facebook-f"></i></a>
                                    <a style="padding: 5px 8px;" href="https://www.youtube.com/channel/UCgacemtU2bpQLcWTsTZhYWg/featured?view_as=subscriber"><i
                                            class="fa fa-youtube"></i></a>
                                    <a style="padding: 5px ;"
                                       href="{!! route('user.change-language', ['vi']) !!}">VN</a>
                                    <a style="padding: 5px 6px;"
                                       href="{!! route('user.change-language', ['en']) !!}">EN</a>
                                <!-- <a style="padding: 5px 6px;" href="{!! route('user.change-language', ['ja']) !!}">JA</a> -->
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-12" style="padding-left: 0;padding-right: 0">
                            <div id="sub-menu">
                                <div id="cssmenu">
                                    <ul class="nav-mb">
                                        <li><a href="/">{{__('menus.home')}}</a></li>

                                        @foreach($data as $menus)
                                            <li class="has-sub ">
                                                <a href="{{ $menus->links}}" class="mobile-a"
                                                   id="{{$menus->id}}">{{__($menus->name)}}
                                                </a>
                                                <ul>
                                                    @if($menus->links == '/about')
                                                        @foreach($menus->abouts as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.about.showAbout')}}#{{ $titles->links }}">{{$titles->title}}</a>
                                                            </li>

                                                        @endforeach
                                                        <li>
                                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                                            </a>
                                                        </li>
                                                    @endif

                                                    @if($menus->links == '/partners')
                                                        @foreach($menus->partners as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.partners.showPartners_detail')}}#{{$titles->id}}">{{$titles->title}}</a>
                                                            </li>
                                                        @endforeach
                                                    @endif

                                                    @if($menus->links == '/services')
                                                        @foreach($menus->services as $titles)
                                                            <li class="has-sub">
                                                                <a class="mobile-a">{{$titles->title}}</a>

                                                                <ul>

                                                                    @foreach($services_details as $tt)
                                                                        @if($titles->id == $tt->services_id)
                                                                            <a
                                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                                        @endif
                                                                    @endforeach

                                                                </ul>

                                                            </li>
                                                        @endforeach
                                                    @endif

                                                </ul>

                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 hidden-xs hidden-sm" style="padding: 0">
                            <div class="hotline">
                            <span class="montserrat">
                                <img src="content/img/telephone.png" class="tel">
                                <img src="content/img/telephone (1).png" class="tel1">
                                <a href="tel:19003053"> 1900 3053</a>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-lg hidden-sm hidden-md">
        <div class="header"></div>
        <div class="logo-mobile">
            <a href="/">
                <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                <img class="img-logo" src="content/img/color_logo_2.png">
            </a>
        </div>
        <input type="checkbox" class="openSidebarMenu" id="openSidebarMenu"/>
        <label for="openSidebarMenu" class="sidebarIconToggle">
            <div class="spinner diagonal part-1"></div>
            <div class="spinner horizontal"></div>
            <div class="spinner diagonal part-2"></div>
        </label>
        <div id="sidebarMenu">
            <nav id="sidebar-mobile" class="nav">
                <ul class="sidebarMenuInner list-unstyled components">
                    <li><a href="/">{{__('menus.home')}}</a></li>
                    @foreach($data as $menus)
                        <li>
                            @if(in_array($menus->id, [23,29]))
                                <a type="button" data-toggle="collapse" data-target="#homeSubmenu{{$menus->id}}"
                                   style="display: block;">
                                    {{__($menus->name)}}
                                    <i class="fas fa-angle-right"></i>
                                </a>
                                <ul class="collapse list-unstyled" id="homeSubmenu{{$menus->id}}">
                                    @foreach($menus->abouts as $title)
                                        <li>
                                            <a href="{{ url('/about') }}#{{ $title->links }}">{{$title->title}}</a>
                                        </li>
                                    @endforeach
                                    @if($menus->id === 23)
                                        <li>
                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                            </a>
                                        </li>
                                    @endif
                                    @foreach($menus->services as $title)
                                        <li>
                                            <a data-target="#homeSubmenu{{$title->id}}" data-toggle="collapse"
                                               type="button">{{$title->title}}
                                                <i class="fas fa-angle-right"></i>
                                            </a>
                                            <ul class="collapse list-unstyled" id="homeSubmenu{{$title->id}}">
                                                @foreach($services_details as $tt)
                                                    @if($title->id == $tt->services_id)
                                                        <li>
                                                            <a
                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <a href="{{$menus->links}}" style="display: block;">{{__('menus.'.$menus->name)}}</a>
                            @endif
                        </li>
                    @endforeach
                    <li><a href="/faqs">{{__('menus.faq')}}</a></li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <div class="mobile-menu">
                            <div class="menu-top-mobile">
                                <ul class="eg">
                                    <li><a href="/news">{{__('menus.news')}}</a></li>
                                    <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.Hỏi đáp')}}</a></li>
                                    <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                    <li class="">
                                        <a data-target="#lang" data-toggle="collapse" type="button">English</a>
                                        <ul class="collapse list-unstyled" id="lang">
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.html">VN</a>
                                            </li>
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.en.html">EN</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>

                        </div>
                    </li>
                </ul>
            </nav>
            <div class="social social-mobile">
                <a><i class="fa fa-facebook-f"></i></a>
                <a><i class="fa fa-youtube"></i></a>
            </div>
        </div>
    </div>
</header>
<section class="slider">

    <div class="sliderbar owl-carousel owl-theme ">
        @foreach($slide as $val)
            <div class="item">
                <img class="hidden-xs" src="uploads/slides/{{$val->image}}" style="width: 1920px; height: 950px">
                <img class="img hidden-lg hidden-md hidden-sm" src="uploads/slides/{{$val->image}}">
                <div class="text-slider ">
                    <h2 class="montserrat animatable moveUp">{{$val->name}}</h2>
                    <h4 class="animatable moveUp hidden-xs">{{$val->title}}</h4>
                    <div class="mrg-30 position ">
                        <a href="{{route('customer.about.showAbout_detail')}}"
                           class="btn btn-more">{{__('home.more')}}</a>
                    </div>
                </div>
            </div>

        @endforeach
    </div>

    <script>
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            nav: true,
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            items: 1,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: false,
        });

    </script>
</section>
<section class="mrg-list">
    <div class="container">
        <div class="row">
            <div class="section-2-list">
                @foreach($package as $val)
                    <div class="col-md-4 col-sm-4">
                        <div class="item">
                            <a href="http://doctor-thomaslodi.com/">
                            <!-- <a href="{{route('customer.packages.showPackages')}}#{{$val->packagesid}}"> -->
                                <div class="img-ss2">
                                    <img class="hidden-xs" src="uploads/packages/{{$val->banner}}">
                                    <img class="img hidden-lg hidden-md hidden-sm"
                                         src="uploads/packages/{{$val->banner}}">
                                </div>
                                <div class="content-ss2">
                                    <h4>{{$val->name}}</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<section class="mrg-80 ">
    <div class="about">
        <div class="container">
            <div class="row">
                <img class="logo-about" src="content/img/color_logo_3 (1).png">
                <div class="title-h2 text-center title-about animatable moveUp">
                    <h2 class="montserrat">{{ __('menus.EVERYDAY HEALTHY LIVING') }}</h2>
                </div>
                <!-- <div class="des montserrat animatable moveUp">
                        trung thực. tận tình. chu đáo.</br>
                        chúng tôi là infinityhealthcare vietnam
                    </div> -->
                <div class="content-home animatable moveUp text-left">
                    {!! __('menus.EVERYDAY HEALTHY LIVING CONTENT') !!}
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mrg-80 section-2" style="overflow:hidden">
    <div class="hidden-lg hidden-md hidden-sm mobile-img">
        <img src="content/img/mobile/banner/3. Trai nghiem dich vu hang dau (banner).jpg"/>
    </div>
    <!-- <img src="content/img/Homepage/3. Trai nghiem dich vu hang dau (banner).png"> -->
    <div class="server ">
        <div class="animatable moveUp position">
            <h2 class="montserrat ">{{ __('menus.EXPERIENCE THE TOP CLASS SERVICES') }}</h2>
        </div>
        <div class="text-center mrg-30 position ">
            <a class="btn btn-more" href="{{route('customer.services.showServices')}}">{{ __('menus.ReadMore') }}</a>
        </div>
    </div>
</section>
<section class="mrg-120 overflow ">
    <div class="service-medical">
        <div class="container">
            <div class="row">
                <div class="title-h2 text-center animatable moveUp">
                    <h2 class="montserrat ">{{ __('menus.MEDICAL SERVICES') }}</h2>
                </div>
                <div class="list-service">
                    <?php $count = 1 ?>
                    @foreach($dv as $val)
                        @if($count %2 != 0)
                            <div class="item ">
                                <div class="mrg-bgr">
                                    <div class="col-md-6 col-sm-6 hidden-lg hidden-md hidden-sm pd-0">
                                        <div class="bgr-img right">
                                            <img class="img hidden-xs" src="uploads/about_service/{{$val->image}}">
                                            <img class="img hidden-lg hidden-md hidden-sm"
                                                 src="uploads/about_service/{{$val->image}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 ">
                                        <div class="number montserrat animatable moveUp right">0{{$count}}</div>
                                        <div class="des-position text-right">
                                            <div class="title-h3 ">
                                                <h3 class="montserrat animatable moveUp">{{$val->title}}</h3>
                                            </div>
                                            <div class="des-service animatable moveUp hidden-xs hidden-md hidden-sm">
                                                {!! $val->content !!}
                                            </div>
                                            <div class="des-service animatable moveUp hidden-lg ">
                                                {!! $val->content !!}
                                            </div>
                                            <div class="buttom ">
                                                <a href="{{route('customer.services.showServices')}}#{{$val->link}}">{{ __('menus.ReadMore') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 hidden-xs">
                                        <div class="bgr-img right">
                                            <img class="img" src="uploads/about_service/{{$val->image}}">
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>

                        @else
                        <!-- item -->
                            <div class="item ">
                                <div class="mrg-bgr">
                                    <div class="col-md-6 col-sm-6 pd-0">
                                        <div class="bgr-img left">
                                            <img class="img hidden-xs" src="uploads/about_service/{{$val->image}}">
                                            <img class="img hidden-lg hidden-md hidden-sm"
                                                 src="uploads/about_service/{{$val->image}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 ">
                                        <div class="number montserrat left animatable moveUp">0{{$count}}</div>
                                        <div class="des-position text-left">
                                            <div class="title-h3 animatable moveUp">
                                                <h3 class="montserrat ">{{$val->title}}</h3>
                                            </div>
                                            <div class="des-service pull-left animatable moveUp">
                                                {!! $val->content !!}
                                            </div>
                                            <div class="buttom ">
                                                <a href="{{route('customer.services.showServices')}}#{{$val->link}}">{{ __('menus.ReadMore') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                            <!-- item -->

                        @endif
                    <!-- item -->
                        <?php $count++ ?>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</section>
<section class="mrg-120 bgr-brow">
    <div class="container">
        <div class="row">
            <div class="title-h2 text-center animatable moveUp" id="tai-sao">
                <h2 class="montserrat hidden-xs">{{ __('menus.WHY CHOOSE IHC') }}</h2>
                <h2 class="montserrat hidden-lg hidden-md hidden-sm">{!! __('menus.WHY CHOOSE IHC LG') !!}</h2>
            </div>
            <div class="mrg-40">
                <div class="list-ihc">
                    <div class="col-md-4 col-sm-4 ">
                        <div class="item text-center">
                            <div class="img-br">
                                <img src="content/img/iconfinder_247_24-7_phone_service_support_call_4014702.png">
                            </div>
                            <div class="title-ihc animatable moveUp">
                                <h3>{{ __('menus.COMPREHENSIVE ADVICES') }}</h3>
                            </div>
                            <div class="des-ihc animatable moveUp hidden-xs ">
                                {!! __('menus.COMPREHENSIVE ADVICES CONTENT') !!}
                            </div>
                            <div class="des-ihc animatable moveUp hidden-lg hidden-md hidden-sm">
                                {!! __('menus.COMPREHENSIVE ADVICES CONTENT') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <div class="item text-center">
                            <div class="img-br">
                                <img src="content/img/Group (1).png">
                            </div>

                            <div class="title-ihc animatable moveUp">
                                <h3>{{ __('menus.TOP QUALITY') }}</h3>
                            </div>
                            <div class="des-ihc animatable moveUp">
                                {{ __('menus.TOP QUALITY CONTENT') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <div class="item text-center">
                            <div class="img-br">
                                <img src="content/img/icon.png">
                            </div>
                            <div class="title-ihc animatable moveUp">
                                <h3>{{ __('menus.PROFESSIONAL SERVICES') }}</h3>
                            </div>
                            <div class="des-ihc animatable moveUp">
                                {{ __('menus.PROFESSIONAL SERVICES CONTENT') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="buttom">
                    <a href="{{route('customer.about.showAbout')}}#tai-sao">{{ __('menus.ReadMore') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mrg-120">
    <div class="container">
        <div class="row">
            <div class="title-h2 text-center animatable moveUp">
                <h2 class="montserrat">{{__('menus.PARTNERS') }}</h2>
            </div>
            <div class="list-partner list-owl-partner">
                @foreach($partner as $val)
                    <div class="col-md-4 col-sm-4">
                        <div class="item text-center">
                            <div class="trans ">
                                <img src="uploads/partners/{{$val->image}}" class="img-ihc">
                            </div>
                            <div class="postion-part">
                                <div class="title-partner animatable moveUp">
                                    <h3><a href="/partners_detail#{{$val->id}}">{{$val->home_title}}
                                        </a></h3>
                                </div>
                                <!-- <div class="des-partner animatable moveUp">
                                    Lorem ipsum dolor sit amet conse ctetur adipisicing elit eiusmod
                                </div> -->
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="buttom">
                <a href="{{route('customer.partners.showPartners')}}">{{ __('menus.ReadMore') }}</a>
            </div>
        </div>
    </div>
</section>
<section class="mrg-120 bgr-brow">
    <div class="container">
        <div class="row">
            <div class="title-h2 text-center animatable moveUp">
                <h2 class="montserrat">{{__('menus.PROCEDURES') }}</h2>
            </div>
            <div class="mrg-40">
                <div class="list-ihc">
                    <div class="col-md-3 col-sm-3 ">
                        <div class="item text-center">
                            <div class="img-br">
                                <img src="content/img/Homepage/Tu van truoc dieu tri.png">
                            </div>
                            <div class="title-ihc  animatable moveUp">
                                <h3>{{__('menus.PRE-TREATMENT CONSULTANCY') }}</h3>
                            </div>
                            <div class="des-ihc  animatable moveUp text-left" style="padding-left:35px ">
                                {!!__('menus.PRE-TREATMENT CONSULTANCY CONTENT') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <div class="item text-center">
                            <div class="img-br">
                                <img src="content/img/Vector (1).png">
                            </div>
                            <div class="title-ihc  animatable moveUp">
                                <h3>{{__('menus.PRE-DEPARTURE PREPARATION') }}</h3>
                            </div>
                            <div class="des-ihc  animatable moveUp text-left" style="padding-left:15px ">
                                {!!__('menus.PRE-DEPARTURE PREPARATION CONTENT') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <div class="item text-center">
                            <div class="img-br">
                                <img src="content/img/Group (1).png">
                            </div>
                            <div class="title-ihc  animatable moveUp">
                                <h3>{{__('menus.OVERSEAS TREATMENT') }}</h3>
                            </div>
                            <div class="des-ihc  animatable moveUp text-left" style="padding-left:30px ">
                                {!!__('menus.OVERSEAS TREATMENT CONTENT') !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <div class="item text-center">
                            <div class="img-br">
                                <img src="content/img/Group (2).png">
                            </div>
                            <div class="title-ihc  animatable moveUp">
                                <h3>{{__('menus.POST-TREATMENT MONITORING') }}</h3>
                            </div>
                            <div class="des-ihc  animatable moveUp text-left" style="padding-left:38px ">
                                {!!__('menus.POST-TREATMENT MONITORING CONTENT') !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="buttom">
                    <a href="{{route('customer.procedure.showProcedure')}}">{{ __('menus.ReadMore') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mrg-120">
    <div class="journeys">
        <div class="container">
            <div class="row">
                <div class="title-h2 text-center animatable moveUp">
                    <h2 class="montserrat hidden-xs">{{__('menus.TESTIMONIALS') }}</h2>
                    <h2 class="montserrat hidden-lg hidden-md hidden-sm">{!!__('menus.TESTIMONIALS LG') !!}</h2>
                </div>
            </div>
        </div>
        <div class="hidden-lg hidden-md hidden-sm mobile-img">
            <img src="content/img/mobile/banner/5. Cam nhan cua khach hang (banner).jpg"/>
        </div>
        <!-- <img  src="content/img/Homepage/5. Cam nhan cua khach hang (banner).png"> -->
        <div class="server">
            <div class="custom1 owl-carousel owl-theme">
                @foreach($comment as $val)
                    <div class="item text-center  ">
                        <div class="des-jour animatable moveUp">
                            <p class="more">{!!(strlen($val->content) > 300) ? mb_substr($val->content, 0, 300) : $val->name!!}
                                ....</p>
                        </div>
                        <h4 class="montserrat name animatable moveUp">{{ $val->name }}</h4>
                        <p class="text-jour animatable moveUp">{{ $val->position }}</p>
                        <div class="text-center mrg-80 position ">
                            <a class="btn btn-more "
                               href="{{route('customer.about.showAbout')}}#cam-nhan">{{ __('menus.ReadMore') }}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<div class="fixed-footer" id="info">
    <a href="http://tangcuonghemiendich.net/" target="_blank"><img src="content/img/comp.gif" style="width: 100%; height: auto"></a>
</div>
<footer id="footer">
    <div class="footer">
        <div class="container">
            <div class="row flex-ft">
                <div class="col-md-6 col-xs-12 pd-0" style="margin-right: 30px">
                    <div class="img-logo-footer">
                        <a href="/">
                            <img src="content/img/color_logo_2.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="row flex-ft">
                <div class="col-md-6 col-xs-12 pd-0" style="margin-right: 30px">

                    <h3 class="title-footer text-capitalize title-main-footer">{{ __('home.company_name') }}</h3>
                    <div class="add clickin">
                        <p class="mst">{{ __('home.tax_code') }}: 0315190682</p>
                    </div>
                    <div class="add clickin">
                        <p>{{ __('home.head_office') }}:</p>
                        <p>{{ __('home.head_office_address') }}</p>
                    </div>
                    <div class="add clickin">
                        <p>{{ __('home.hanoi_office') }}:</p>
                        <p>{{ __('home.hanoi_office_address') }}</p>
                    </div>
                </div>
                <div class="flex-menu col-md- col-xs-12">
                    <h3 class="title-footer text-uppercase mbx-15">Menu</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="list-footer clickin">
                                <ul>
                                    <li><a href="/">{{ __('menus.home') }}</a></li>
                                    <li><a href="/about" class="mobile-a" id="23">{{__('menus.Giới thiệu')}}</a></li>
                                    <li><a href="/services" class="mobile-a" id="29">{{__('menus.Dịch vụ')}}</a></li>
                                    <li><a href="/packages" class="mobile-a" id="30">{{__('menus.Gói trị liệu')}}</a>
                                    </li>
                                    <li><a href="/partners" class="mobile-a" id="34">{{__('menus.Đối tác')}}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-footer clickin">
                                <ul>
                                    <li><a href="/news" class="mobile-a" id="23">{{__('menus.Tin tức')}}</a></li>
                                    <li><a href="/procedure" class="mobile-a" id="35">{{__('menus.Thủ tục')}}</a></li>
                                    <li><a href="/library" class="mobile-a" id="36">{{__('menus.Thư viện')}}</a></li>
                                    <li><a href="/faqs#cau-hoi-thuong-gap"
                                           class="mobile-a">{{__('menus.Hỏi đáp')}}</a>
                                    <li><a href="/contact" class="mobile-a">{{__('menus.Liên hệ')}}</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <h3 class="title-footer text-uppercase mbx-15" id="lien-he">{{ __('home.information') }}</h3>
                    <div class="form-footer clickin" style="color: #acbaca">
                        <p><i class="fa fa-clock-o" style="margin-right: 10px" aria-hidden="true"></i> 8:00 -
                            17:00 {{ __('home.monday') }} - {{ __('home.saturday') }}</p>
                        <p><i class="fa fa-phone" style="margin-right: 10px" aria-hidden="true"></i> 1900 3053</p>
                        <p><i class="fa fa-envelope-o" style="margin-right: 10px" aria-hidden="true"></i>
                            info@infinityhealthcare.vn</p>
                    </div>
                    <div>
                        <ul class="ul-footer">
                            <li class="follow text-uppercase" style="color: #a9bcd2">{{ __('home.social_media') }}</li>
                            <li class="social" style="min-width:100px;">
                                <a class="socail-icon" href="https://www.facebook.com/infinityhealthcarevietnam/" target="_blank"><i class="fa fa-facebook-f"></i></a>
                                <a class="socail-icon" href="https://www.youtube.com/channel/UCgacemtU2bpQLcWTsTZhYWg" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="text-left pull-left  color-22">
                    <p>© {{__('home.copyright')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="back-to-top" class="back-to-top" data-toggle="tooltip" data-placement="left" title="Trở lên đầu trang">
        <i class="fa fa-angle-up"></i>
    </div>
    <div class="hidden-lg hidden-md hidden-sm phone-gif">
        <a href="tel:19003053"><img src="content/img/Homepage/icon-phone.gif"></a>
    </div>
</footer>
<!-- Load Facebook SDK for JavaScript -->
<!-- <div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v4.0'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script> -->

<!-- Your customer chat code -->
<!-- <div class="fb-customerchat"
    attribution=setup_tool
    page_id="704668339994191"
    logged_in_greeting="Xin chào! Chúng tôi có thể giúp gì cho sức khỏe của bạn?"
    logged_out_greeting="Xin chào! Chúng tôi có thể giúp gì cho sức khỏe của bạn?">
</div> -->

</body>
<script>
    $(document).ready(function () {
        // Configure/customize these variables.
        var showChar = 400;  // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "Xem thêm";
        var lesstext = "Rút gọn";


        $('.more').each(function () {
            var content = $(this).html();

            if (content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });
    });
    $(window).on('scroll', function () {
        if($(window).scrollTop() + $(window).height() > $(document).height() - $('footer').height()) {
            $('#info').removeClass('fixed-footer');
        }else{
            $('#info').addClass('fixed-footer');
        }
    });
</script>
</html>
