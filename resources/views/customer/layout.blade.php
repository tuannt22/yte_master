<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-language" content="jp">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Infinity Health Care</title>
	<base href="{{asset('customer/')}}">
	<link rel="shortcut icon" type="image/x-icon" href="content/img/favicon.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" >
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700&amp;subset=vietnamese" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700&display=swap" rel="stylesheet">
	<script type="text/javascript " src="plugins/jquery/jquery-3.4.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="plugins/boostrap/bootstrap.min.css">
	<script type="text/javascript" src="plugins/boostrap/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="plugins/menu/styles.css">
	<script type="text/javascript" src="plugins/menu/script.js"></script>
	<script type="text/javascript" src="plugins/owl-carausel/owl.carousel.min.js"></script>
	<script type="text/javascript" src="content/js/jquery.js?v=20200423"></script>
	<script type="text/javascript" src="content/js/about.js"></script>
	<link rel="stylesheet" type="text/css" href="plugins/owl-carausel/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="plugins/owl-carausel/owl.theme.default.css">
	<link rel="stylesheet" type="text/css" href="content/css/styles.css?v=20200423">
	<link rel="stylesheet" type="text/css" href="content/css/about.css">
	<script>
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
			var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
			s1.async=true;
			s1.src='https://embed.tawk.to/5d635b0877aa790be330b171/default';
			s1.charset='UTF-8';
			s1.setAttribute('crossorigin','*');
			s0.parentNode.insertBefore(s1,s0);
		})();
	</script>
	<style>
		body {
			-webkit-touch-callout: none;
			-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}
	</style>


	{{--<script type="text/javascript">
			//<=!=[=C=D=A=T=A=[
			document.onkeypress = function(event) {
				event = (event || window.event);
				if (event.keyCode === 123) {
			//alert('No F-12');
			return false;
		}
	};
	document.onmousedown = function(event) {
		event = (event || window.event);
		if (event.keyCode === 123) {
			//alert('No F-keys');
			return false;
		}
	};
	document.onkeydown = function(event) {
		event = (event || window.event);
		if (event.keyCode === 123) {
			//alert('No F-keys');
			return false;
		}
	};

	function contentprotector() {
		return false;
	}
	function mousehandler(e) {
		var myevent = (isNS) ? e : event;
		var eventbutton = (isNS) ? myevent.which : myevent.button;
		if ((eventbutton === 2) || (eventbutton === 3))
			return false;
	}
	document.oncontextmenu = contentprotector;
	document.onmouseup = contentprotector;
	var isCtrl = false;
	window.onkeyup = function(e)
	{
		if (e.which === 17)
			isCtrl = false;
	}

	window.onkeydown = function(e)
	{
		if (e.which === 17)
			isCtrl = true;
		if (((e.which === 85) || (e.which === 65) || (e.which === 88) || (e.which === 67) || (e.which === 86) || (e.which === 83)) && isCtrl === true)
		{
			return false;
		}
	}
	isCtrl = false;
	document.ondragstart = contentprotector;
	//]=]=> </script>--}}
</head>
<body>


	<header class="container-header" id="header">
		@yield('header')
	</header>

	<section class="slider">
		@yield('menu')
	</section>
	<section class="mrg-120">

			@yield('content')

	</section>
		@include('customer.footer')
		<style>
			.morecontent span {
				display: none;
			}
			.morelink {
				display: block;
			}
		</style>
		<script>
			$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 400;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Xem thêm";
    var lesstext = "Rút gọn";


    $('.more').each(function() {
    	var content = $(this).html();

    	if(content.length > showChar) {

    		var c = content.substr(0, showChar);
    		var h = content.substr(showChar, content.length - showChar);

    		var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

    		$(this).html(html);
    	}

    });

    $(".morelink").click(function(){
    	if($(this).hasClass("less")) {
    		$(this).removeClass("less");
    		$(this).html(moretext);
    	} else {
    		$(this).addClass("less");
    		$(this).html(lesstext);
    	}
    	$(this).parent().prev().toggle();
    	$(this).prev().toggle();
    	return false;
    });
});
</script>
</body>
</html>
