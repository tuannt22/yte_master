<footer id="footer">
    <div class="footer">
        <div class="container">
            <div class="row flex-ft">
                <div class="col-md-6 col-xs-12 pd-0" style="margin-right: 30px">
                    <div class="img-logo-footer">
                        <a href="/">
                            <img src="content/img/color_logo_2.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="row flex-ft">
                <div class="col-md-6 col-xs-12 pd-0" style="margin-right: 30px">

                    <h3 class="title-footer text-capitalize title-main-footer">{{ __('home.company_name') }}</h3>
                    <div class="add clickin">
                        <p class="mst">{{ __('home.tax_code') }}: 0315190682</p>
                    </div>
                    <div class="add clickin">
                        <p>{{ __('home.head_office') }}:</p>
                        <p>{{ __('home.head_office_address') }}</p>
                    </div>
                    <div class="add clickin">
                        <p>{{ __('home.hanoi_office') }}:</p>
                        <p>{{ __('home.hanoi_office_address') }}</p>
                    </div>
                </div>
                <div class="flex-menu col-md- col-xs-12">
                    <h3 class="title-footer text-uppercase mbx-15">Menu</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="list-footer clickin">
                                <ul>
                                    <li><a href="/">{{ __('menus.home') }}</a></li>
                                    <li><a href="/about" class="mobile-a" id="23">{{__('menus.Giới thiệu')}}</a></li>
                                    <li><a href="/services" class="mobile-a" id="29">{{__('menus.Dịch vụ')}}</a></li>
                                    <li><a href="/packages" class="mobile-a" id="30">{{__('menus.Gói trị liệu')}}</a>
                                    </li>
                                    <li><a href="/partners" class="mobile-a" id="34">{{__('menus.Đối tác')}}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="list-footer clickin">
                                <ul>
                                    <li><a href="/news" class="mobile-a" id="23">{{__('menus.Tin tức')}}</a></li>
                                    <li><a href="/procedure" class="mobile-a" id="35">{{__('menus.Thủ tục')}}</a></li>
                                    <li><a href="/library" class="mobile-a" id="36">{{__('menus.Thư viện')}}</a></li>
                                    <li><a href="/faqs#cau-hoi-thuong-gap"
                                           class="mobile-a">{{__('menus.Hỏi đáp')}}</a>
                                    <li><a href="/contact" class="mobile-a">{{__('menus.Liên hệ')}}</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <h3 class="title-footer text-uppercase mbx-15" id="lien-he">{{ __('home.information') }}</h3>
                    <div class="form-footer clickin" style="color: #acbaca">
                        <p><i class="fa fa-clock-o" style="margin-right: 10px" aria-hidden="true"></i> 8:00 -
                            17:00 {{ __('home.monday') }} - {{ __('home.saturday') }}</p>
                        <p><i class="fa fa-phone" style="margin-right: 10px" aria-hidden="true"></i> 1900 3053</p>
                        <p><i class="fa fa-envelope-o" style="margin-right: 10px" aria-hidden="true"></i>
                            info@infinityhealthcare.vn</p>
                    </div>
                    <div>
                        <ul class="ul-footer">
                            <li class="follow text-uppercase" style="color: #a9bcd2">{{ __('home.social_media') }}</li>
                            <li class="social" style="min-width:100px;">
                                <a class="socail-icon" href="https://www.facebook.com/infinityhealthcarevietnam/" target="_blank"><i class="fa fa-facebook-f"></i></a>
                                <a class="socail-icon" href="https://www.youtube.com/channel/UCgacemtU2bpQLcWTsTZhYWg" target="_blank"><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="text-left pull-left  color-22">
                    <p>© {{__('home.copyright')}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="back-to-top" class="back-to-top" data-toggle="tooltip" data-placement="left" title="Trở lên đầu trang">
        <i class="fa fa-angle-up"></i>
    </div>
    <div class="hidden-lg hidden-md hidden-sm phone-gif">
        <a href="tel:19003053"><img src="content/img/Homepage/icon-phone.gif"></a>
    </div>
</footer>
