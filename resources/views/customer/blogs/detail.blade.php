@extends('customer.layout')

@section('header')
    <div class="container hidden-xs">
        <div class="row ">
            <div class="header-top">
                <div class="col-md-3 col-sm-3 col-xs-6 col-md">
                    <div class="logo">
                        <a href="/">
                            <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                            <img class="img-logo" src="content/img/color_logo_2.png">
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9" style="padding-left: 0;padding-right: 0">
                    <div class="col-sm-3 hidden-xs hidden-lg hidden-md" style="padding: 0">
                        <div class="hotline">
                        <span class="montserrat">
                            <img src="content/img/telephone.png" class="tel">
                            <img src="content/img/telephone (1).png" class="tel1">
                            <a href="tel:19003053"> 1900 3053</a>
                        </span>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-9 col-sm-9 ms-right" style="padding: 15px 0 0">
                        <div class="menu-top hide-fix">
                            <ul>
                                <li><a href="/news">{{__('menus.news')}}</a></li>
                                <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.faq')}}</a></li>
                                <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                <li class="social">
                                    <a style="padding: 5px 10px;" href="https://www.facebook.com/infinityhealthcarevietnam/"><i
                                            class="fa fa-facebook-f"></i></a>
                                    <a style="padding: 5px 8px;" href="https://www.youtube.com/channel/UCgacemtU2bpQLcWTsTZhYWg/featured?view_as=subscriber"><i
                                            class="fa fa-youtube"></i></a>
                                    <a style="padding: 5px ;"
                                       href="{!! route('user.change-language', ['vi']) !!}">VN</a>
                                    <a style="padding: 5px 6px;"
                                       href="{!! route('user.change-language', ['en']) !!}">EN</a>
                                <!-- <a style="padding: 5px 6px;" href="{!! route('user.change-language', ['ja']) !!}">JA</a> -->
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-12" style="padding-left: 0;padding-right: 0">
                            <div id="sub-menu">
                                <div id="cssmenu">
                                    <ul class="nav-mb">
                                        <li><a href="/">{{__('menus.home')}}</a></li>

                                        @foreach($data as $menus)
                                            <li class="has-sub ">
                                                <a href="{{ $menus->links}}" class="mobile-a"
                                                   id="{{$menus->id}}">{{__($menus->name)}}
                                                </a>

                                                <ul>
                                                    @if($menus->links == '/about')
                                                        @foreach($menus->abouts as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.about.showAbout')}}#{{ $titles->links }}">{{$titles->title}}</a>
                                                            </li>
                                                        @endforeach
                                                        <li>
                                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                                            </a>
                                                        </li>
                                                    @endif


                                                    @if($menus->links == '/partners')
                                                        @foreach($menus->partners as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.partners.showPartners_detail')}}#{{$titles->id}}">{{$titles->title}}</a>
                                                            </li>
                                                        @endforeach
                                                    @endif

                                                    @if($menus->links == '/services')
                                                        @foreach($menus->services as $titles)
                                                            <li class="has-sub">
                                                                <a class="mobile-a">{{$titles->title}}</a>

                                                                <ul>

                                                                    @foreach($dv as $tt)
                                                                        @if($titles->id == $tt->services_id)
                                                                            <a
                                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                                        @endif
                                                                    @endforeach

                                                                </ul>

                                                            </li>
                                                        @endforeach
                                                    @endif


                                                </ul>

                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 hidden-xs hidden-sm" style="padding: 0">
                            <div class="hotline">
                            <span class="montserrat">
                                <img src="content/img/telephone.png" class="tel">
                                <img src="content/img/telephone (1).png" class="tel1">
                                <a href="tel:19003053"> 1900 3053</a>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden-lg hidden-sm hidden-md">
        <div class="header"></div>
        <div class="logo-mobile">
            <a href="/">
                <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                <img class="img-logo" src="content/img/color_logo_2.png">
            </a>
        </div>
        <input type="checkbox" class="openSidebarMenu" id="openSidebarMenu"/>
        <label for="openSidebarMenu" class="sidebarIconToggle">
            <div class="spinner diagonal part-1"></div>
            <div class="spinner horizontal"></div>
            <div class="spinner diagonal part-2"></div>
        </label>
        <div id="sidebarMenu">
            <nav id="sidebar-mobile" class="nav">
                <ul class="sidebarMenuInner list-unstyled components">
                    <li><a href="/">{{__('menus.home')}}</a></li>
                    @foreach($data as $menus)
                        <li>
                            @if(in_array($menus->id, [23,29]))
                                <a type="button" data-toggle="collapse" data-target="#homeSubmenu{{$menus->id}}"
                                   style="display: block;">
                                    {{__($menus->name)}}
                                    <i class="fas fa-angle-right"></i>
                                </a>
                                <ul class="collapse list-unstyled" id="homeSubmenu{{$menus->id}}">
                                    @foreach($menus->abouts as $title)
                                        <li>
                                            <a href="{{ url('/about') }}#{{ $title->links }}">{{$title->title}}</a>
                                        </li>
                                    @endforeach
                                    @if($menus->id === 23)
                                        <li>
                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                            </a>
                                        </li>
                                    @endif
                                    @foreach($menus->services as $title)
                                        <li>
                                            <a data-target="#homeSubmenu{{$title->id}}" data-toggle="collapse"
                                               type="button">{{$title->title}}
                                                <i class="fas fa-angle-right"></i>
                                            </a>
                                            <ul class="collapse list-unstyled" id="homeSubmenu{{$title->id}}">
                                                @foreach($dv as $tt)
                                                    @if($title->id == $tt->services_id)
                                                        <li>
                                                            <a
                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <a href="{{$menus->links}}" style="display: block;">{{__('menus.'.$menus->name)}}</a>
                            @endif
                        </li>
                    @endforeach
                    <li><a href="/faqs">{{__('menus.faq')}}</a></li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <div class="mobile-menu">
                            <div class="menu-top-mobile">
                                <ul class="eg">
                                    <li><a href="/news">{{__('menus.news')}}</a></li>
                                    <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.Hỏi đáp')}}</a></li>
                                    <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                    <li class="">
                                        <a data-target="#lang" data-toggle="collapse" type="button">English</a>
                                        <ul class="collapse list-unstyled" id="lang">
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.html">VN</a>
                                            </li>
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.en.html">EN</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>

                        </div>
                    </li>
                </ul>
            </nav>
            <div class="social social-mobile">
                <a><i class="fa fa-facebook-f"></i></a>
                <a><i class="fa fa-youtube"></i></a>
            </div>
        </div>
    </div>

@endsection

@section('menu')
    <div class="item-br">

        @foreach($image as $img)
            <img class="hidden-xs" src="uploads/menus/{{$img->image}}" style="width: 1920px; height: 580px">
        @endforeach
        <img class="hidden-lg hidden-md hidden-sm" src="content/img/mobile/Gioi thieu.jpg">
        <div class="title-br hidden-lg hidden-md hidden-sm">
            <h2>{{ __('menus.BLOG') }}</h2>
        </div>
        <div class="menu-line hidden-xs">
            <div class="container">
                <div class="row">

                    <ul>
                        <li><a href="/" id="">Home</a></li>
                        <li><a href="{{ route('customer.listBlog') }}">Blogs</a></li>
                        <li>{{ $blog->title }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <style type="text/css">
        .blog{
            margin: 0;
        }
        .title{
            font-weight: bold;
            font-size: 24px;
            margin-bottom: 10px;
        }
        .sumary{
            font-weight: 400;
            color: #cccccc;
            font-style: italic;
        }
        .content {
            width: 100%;
            margin-top: 15px;
        }
        .topic{
            background: #169bd3;
            padding: 5px 0;
            color: #ffffff;
        }
    </style>
    <div class="container">
        <div id="fb-root"></div>
        <div class="pc">
            <div class="row" style="margin-top: -100px">
                <div class="col-md-3">
                    {!! $blogBanner->content ?? '' !!}
                </div>
                <div class="col-md-6">
                    <div class="blog">
                        <div class="title">{{ $blog->title }}</div>
                        <div class="sumary">{{ $blog->sumary }}</div>
                        <div class="socail">
                            <div style="display: inline-block">
                                <div class="fb-like"
                                     data-href="{{ url()->current() }}"
                                     data-layout="button_count"
                                     data-action="like"
                                     data-show-faces="true">
                                </div>
                                <div class="fb-share-button"
                                     data-href="{{ url()->current() }}"
                                     data-layout="button_count">
                                </div>
                                <div style="display: inline-block">
                                    <script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
                                    <script type="IN/Share" data-url="{{ url()->current() }}"></script>
                                </div>
                            </div>
                        </div>
                        <div class="content">{!! $blog->content !!}</div>
                        <div class="socail">
                            <div style="display: inline-block">
                                <div class="fb-like"
                                     data-href="{{ url()->current() }}"
                                     data-layout="button_count"
                                     data-action="like"
                                     data-show-faces="true">
                                </div>
                                <div class="fb-share-button"
                                     data-href="{{ url()->current() }}"
                                     data-layout="button_count">
                                </div>
                                <div style="display: inline-block">
                                    <script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
                                    <script type="IN/Share" data-url="{{ url()->current() }}"></script>
                                </div>
                            </div>
                        </div>
                        <div class="fb-comments" data-href="{{ url()->current() }}" data-numposts="4" data-width=""></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="hot-news" style="border: none; padding: 0">
                        @foreach($blogMost as $item)
                            <div class="item row">
                                <div class="col-md-4">
                                    <img src="uploads/blogs/{{ $item->image_preview }}" width="100%">
                                </div>
                                <div class="col-md-8">
                                    <a href="{{ route('customer.detailBlog', $item->id) }}"><h4 class="title">{{ $item->title }}</h4></a>
                                </div>
                            </div>
                        @endforeach
                        @foreach($hotNews as $item)
                                <div class="item row">
                                    <div class="col-md-4">
                                        <img src="uploads/blogs/{{ $item->image_preview }}" width="100%">
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ route('customer.detailBlog', $item->id) }}"><h4 class="title">{{ $item->title }}</h4></a>
                                    </div>
                                </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 30px">
                @foreach($blogTopic as $item)
                <div class="col-md-4" style="margin-bottom: 20px">
                    <div style="margin-bottom: 30px">
                        <img width="100%" src="uploads/blogs/{{ $item->image_preview }}">
                        <div class="title"><a href="{{ route('customer.detailBlog', $item->id) }}">{{ $item->title }}</a></div>
                        <p class="sumary">{{ $item->sumary }}</p>
                        <div class="action" style="overflow: hidden">
                            <div class="datetime"><i class="fa fa-clock"></i>{{ date('d/m/Y', strtotime($item->created_at)) }}</div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="sm">
            <div class="row" style="margin-top: -50px">
                <div class="topic text-center" style="margin-bottom: 30px">
                    {{ $blog->topics->name}}
                </div>
                <div class="col-xs-12">
                    <div class="title">{{ $blog->title }}</div>
                    <div class="sumary">{{ $blog->sumary }}</div>
                    <div class="socail">
                        <div style="display: inline-block">
                            <div class="fb-like"
                                 data-href="{{ url()->current() }}"
                                 data-layout="button_count"
                                 data-action="like"
                                 data-show-faces="true">
                            </div>
                            <div class="fb-share-button"
                                 data-href="{{ url()->current() }}"
                                 data-layout="button_count">
                            </div>
                            <div style="display: inline-block">
                                <script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
                                <script type="IN/Share" data-url="{{ url()->current() }}"></script>
                            </div>
                        </div>
                    </div>
                    <div class="content">{!! $blog->content !!}</div>
                    <div style="margin-bottom: 15px" class="socail">
                        <div style="display: inline-block">
                            <div class="fb-like"
                                 data-href="{{ url()->current() }}"
                                 data-layout="button_count"
                                 data-action="like"
                                 data-show-faces="true">
                            </div>
                            <div class="fb-share-button"
                                 data-href="{{ url()->current() }}"
                                 data-layout="button_count">
                            </div>
                            <div style="display: inline-block">
                                <script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
                                <script type="IN/Share" data-url="{{ url()->current() }}"></script>
                            </div>
                        </div>
                    </div>
                    <div class="fb-comments" data-href="{{ url()->current() }}" data-numposts="4" data-width=""></div>
                    <div style="margin-top: 30px">
                        @foreach($blogTopic as $blog)
                            <div class="row blog-item">
                                <div class="col-xs-8">
                                    <a href="{{route('customer.listBlog')}}?topic_id={{ $blog->topic_id }}" class="topic">{{ $blog->topic_name }}</a>
                                    <a href="{{ route('customer.detailBlog', $blog->id) }}"><h4 class="title" style="line-height: 1.2">{{ $blog->title }}</h4></a>
                                    <p class="sumary" style="font-size: 15px; -webkit-line-clamp">{{ $blog->sumary }}</p>
                                    <div class="action">
                                        <div class="datetime"><i class="fa fa-clock"></i>{{ date('d/m/Y', strtotime($blog->created_at)) }}</div>
                                        <div class="read-more"><a href="{{ route('customer.detailBlog', $blog->id) }}">Xem thêm...</a></div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <img src="uploads/blogs/{{ $blog->image_preview }}" width="100%">
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0"></script>--}}
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=184389155447039&autoLogAppEvents=1"></script>
@endsection
