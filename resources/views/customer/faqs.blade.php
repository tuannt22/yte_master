@extends('customer.layout')

@section('header')
    <div class="container hidden-xs">
        <div class="row ">
            <div class="header-top">
                <div class="col-md-3 col-sm-3 col-xs-6 col-md">
                    <div class="logo">
                        <a href="/">
                            <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                            <img class="img-logo" src="content/img/color_logo_2.png">
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9" style="padding-left: 0;padding-right: 0">
                    <div class="col-sm-3 hidden-xs hidden-lg hidden-md" style="padding: 0">
                        <div class="hotline">
                        <span class="montserrat">
                            <img src="content/img/telephone.png" class="tel">
                            <img src="content/img/telephone (1).png" class="tel1">
                            <a href="tel:19003053"> 1900 3053</a>
                        </span>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-9 col-sm-9 ms-right" style="padding: 15px 0 0">
                        <div class="menu-top hide-fix">
                            <ul>
                                <li><a href="/news">{{__('menus.news')}}</a></li>
                                <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.faq')}}</a></li>
                                <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                <li class="social">
                                    <a style="padding: 5px 10px;" href="https://www.facebook.com/infinityhealthcarevietnam/"><i
                                            class="fa fa-facebook-f"></i></a>
                                    <a style="padding: 5px 8px;" href="https://www.youtube.com/channel/UCgacemtU2bpQLcWTsTZhYWg/featured?view_as=subscriber"><i
                                            class="fa fa-youtube"></i></a>
                                    <a style="padding: 5px ;"
                                       href="{!! route('user.change-language', ['vi']) !!}">VN</a>
                                    <a style="padding: 5px 6px;"
                                       href="{!! route('user.change-language', ['en']) !!}">EN</a>
                                <!-- <a style="padding: 5px 6px;" href="{!! route('user.change-language', ['ja']) !!}">JA</a> -->
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-12" style="padding-left: 0;padding-right: 0">
                            <div id="sub-menu">
                                <div id="cssmenu">
                                    <ul class="nav-mb">
                                        <li><a href="/">{{__('menus.home')}}</a></li>

                                        @foreach($data as $menus)
                                            <li class="has-sub ">
                                                <a href="{{ $menus->links}}" class="mobile-a"
                                                   id="{{$menus->id}}">{{__($menus->name)}}
                                                </a>

                                                <ul>
                                                    @if($menus->links == '/about')
                                                        @foreach($menus->abouts as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.about.showAbout')}}#{{ $titles->links }}">{{$titles->title}}</a>
                                                            </li>
                                                        @endforeach
                                                        <li>
                                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                                            </a>
                                                        </li>
                                                    @endif


                                                    @if($menus->links == '/partners')
                                                        @foreach($menus->partners as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.partners.showPartners_detail')}}#{{$titles->id}}">{{$titles->title}}</a>
                                                            </li>
                                                        @endforeach
                                                    @endif

                                                    @if($menus->links == '/services')
                                                        @foreach($menus->services as $titles)
                                                            <li class="has-sub">
                                                                <a class="mobile-a">{{$titles->title}}</a>

                                                                <ul>

                                                                    @foreach($dv as $tt)
                                                                        @if($titles->id == $tt->services_id)
                                                                            <a
                                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                                        @endif
                                                                    @endforeach

                                                                </ul>

                                                            </li>
                                                        @endforeach
                                                    @endif


                                                </ul>

                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 hidden-xs hidden-sm" style="padding: 0">
                            <div class="hotline">
                            <span class="montserrat">
                                <img src="content/img/telephone.png" class="tel">
                                <img src="content/img/telephone (1).png" class="tel1">
                                <a href="tel:19003053"> 1900 3053</a>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden-lg hidden-sm hidden-md">
        <div class="header"></div>
        <div class="logo-mobile">
            <a href="/">
                <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                <img class="img-logo" src="content/img/color_logo_2.png">
            </a>
        </div>
        <input type="checkbox" class="openSidebarMenu" id="openSidebarMenu"/>
        <label for="openSidebarMenu" class="sidebarIconToggle">
            <div class="spinner diagonal part-1"></div>
            <div class="spinner horizontal"></div>
            <div class="spinner diagonal part-2"></div>
        </label>
        <div id="sidebarMenu">
            <nav id="sidebar-mobile" class="nav">
                <ul class="sidebarMenuInner list-unstyled components">
                    <li><a href="/">{{__('menus.home')}}</a></li>
                    @foreach($data as $menus)
                        <li>
                            @if(in_array($menus->id, [23,29]))
                                <a type="button" data-toggle="collapse" data-target="#homeSubmenu{{$menus->id}}"
                                   style="display: block;">
                                    {{__($menus->name)}}
                                    <i class="fas fa-angle-right"></i>
                                </a>
                                <ul class="collapse list-unstyled" id="homeSubmenu{{$menus->id}}">
                                    @foreach($menus->abouts as $title)
                                        <li>
                                            <a href="{{ url('/about') }}#{{ $title->links }}">{{$title->title}}</a>
                                        </li>
                                    @endforeach
                                    @if($menus->id === 23)
                                        <li>
                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                            </a>
                                        </li>
                                    @endif
                                    @foreach($menus->services as $title)
                                        <li>
                                            <a data-target="#homeSubmenu{{$title->id}}" data-toggle="collapse"
                                               type="button">{{$title->title}}
                                                <i class="fas fa-angle-right"></i>
                                            </a>
                                            <ul class="collapse list-unstyled" id="homeSubmenu{{$title->id}}">
                                                @foreach($dv as $tt)
                                                    @if($title->id == $tt->services_id)
                                                        <li>
                                                            <a
                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <a href="{{$menus->links}}" style="display: block;">{{__('menus.'.$menus->name)}}</a>
                            @endif
                        </li>
                    @endforeach
                    <li><a href="/faqs">{{__('menus.faq')}}</a></li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <div class="mobile-menu">
                            <div class="menu-top-mobile">
                                <ul class="eg">
                                    <li><a href="/news">{{__('menus.news')}}</a></li>
                                    <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.Hỏi đáp')}}</a></li>
                                    <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                    <li class="">
                                        <a data-target="#lang" data-toggle="collapse" type="button">English</a>
                                        <ul class="collapse list-unstyled" id="lang">
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.html">VN</a>
                                            </li>
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.en.html">EN</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>

                        </div>
                    </li>
                </ul>
            </nav>
            <div class="social social-mobile">
                <a><i class="fa fa-facebook-f"></i></a>
                <a><i class="fa fa-youtube"></i></a>
            </div>
        </div>
    </div>

@endsection

@section('menu')
    <div class="item-br">

        @foreach($image as $img)
            <img class="hidden-xs" src="uploads/menus/{{$img->image}}" style="width: 1920px; height: 580px">
        @endforeach
        <img class="hidden-lg hidden-md hidden-sm" src="content/img/mobile/Gioi thieu.jpg">
        <div class="title-br hidden-lg hidden-md hidden-sm">
            <h2>{{ __('menus.Hỏi đáp') }}</h2>
        </div>
        <div class="menu-line hidden-xs">
            <div class="container">
                <div class="row">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>{{ __('menus.Hỏi đáp') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- start menu bên trái -->
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 hidden-xs fadeInDown animated" id="sidebar">
                <div class="sidebar">
                    <div class="sidebar-h3">
                        <h3>{{ __('menus.Hỏi đáp') }}</h3>
                    </div>
                    <div class="list-sidebar">
                        <div class="wrapper">
                            <!-- Sidebar -->
                            <nav id="sidebar" class="nav">
                                <ul class="list-unstyled components">
                                    <li><a href="/">Trang chủ</a></li>
                                    @foreach($data as $menus)
                                        <li>
                                            <a type="button" data-toggle="collapse"
                                               data-target="#homeSubmenu{{$menus->id}}" style="display: block;"
                                               href="{{ $menus->links}}">{{$menus->name}}

                                            </a>

                                            <ul class="collapse list-unstyled" id="homeSubmenu{{$menus->id}}">

                                                @foreach($menus->abouts as $title)
                                                    <li>
                                                        <a href="#">{{$title->title}}</a>

                                                    </li>
                                                @endforeach

                                                @foreach($menus->services as $title)
                                                    <li>
                                                        <a data-target="#homeSubmenu{{$title->id}}"
                                                           data-toggle="collapse" type="button">{{$title->title}}
                                                            <i class="fas fa-plus"></i>
                                                        </a>
                                                        <ul class="collapse list-unstyled"
                                                            id="homeSubmenu{{$title->id}}">
                                                            @foreach($dv as $tt)
                                                                @if($title->id == $tt->services_id)
                                                                    <li>

                                                                        <a href="">{{$tt->title_con}}
                                                                        </a>

                                                                    </li>
                                                                @endif
                                                            @endforeach

                                                        </ul>
                                                    </li>
                                            @endforeach

                                            <!-- @foreach($menus->abouts as $title)
                                                <li>
                                                    <a href="#">{{$title->title}}</a>

													</li>
													@endforeach -->

                                            </ul>

                                        </li>
                                    @endforeach
                                    <li><a type="button" data-toggle="collapse" style="display: block;"
                                           href="/faqs">{{__('menus.faq')}}</a></li>
                                </ul>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>

            <!-- end menu bên trái -->

            <!-- Start Content -->
            <div class="col-md-9 col-sm-8 float">
                <div class="title-h2-bg" id="cau-hoi-thuong-gap">
                    <h2>{{ __('about.ANTI-AGING') }}</h2>
                </div>
                <div class="faqs-page block mgr-40 bgr-10">
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <a role="button" class="item-question" data-toggle="collapse" data-parent="#accordion2"
                               href="#collapse1a" aria-expanded="true" aria-controls="collapse1a">
                                {{ __('about.1WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse1a" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.1WHAT ANTI-AGING CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapse2a" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.2WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse2a" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.2WHAT ANTI-AGING CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapse3a" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.3WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse3a" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {{ __('about.3WHAT ANTI-AGING CONTENT') }}

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapse4a" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.4WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse4a" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.4WHAT ANTI-AGING CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapse5a" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.5WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse5a" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.5WHAT ANTI-AGING CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapse6a" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.6WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse6a" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.6WHAT ANTI-AGING CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapse7a" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.7WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse7a" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.7WHAT ANTI-AGING CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion2" href="#collapse8a" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.8WHAT ANTI-AGING') }}
                            </a>
                            <div id="collapse8a" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.8WHAT ANTI-AGING CONTENT') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="title-h2-bg">
                    <h2>{{ __('about.DETOXIFICATION') }}</h2>
                </div>
                <div class="faqs-page block mgr-40 bgr-10">
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <a role="button" class="item-question" data-toggle="collapse" data-parent="#accordion1"
                               href="#collapse1" aria-expanded="true" aria-controls="collapse1a">
                                {{ __('about.1DETOXIFICATION') }}
                            </a>
                            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.1DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse2" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.2DETOXIFICATION') }}
                            </a>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.2DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse3" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.3DETOXIFICATION') }}
                            </a>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.3DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse4" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.4DETOXIFICATION') }}
                            </a>
                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.4DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse5" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.5DETOXIFICATION') }}
                            </a>
                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.5DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse6" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.6DETOXIFICATION') }}
                            </a>
                            <div id="collapse6" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.6DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse7" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.7DETOXIFICATION') }}
                            </a>
                            <div id="collapse7" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.7DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse8" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.8DETOXIFICATION') }}
                            </a>
                            <div id="collapse8" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.8DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse9" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.9DETOXIFICATION') }}
                            </a>
                            <div id="collapse9" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.9DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse10" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.10DETOXIFICATION') }}
                            </a>
                            <div id="collapse10" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.10DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse11" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.11DETOXIFICATION') }}
                            </a>
                            <div id="collapse11" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.11DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse12" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.12DETOXIFICATION') }}
                            </a>
                            <div id="collapse12" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.12DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse13" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.13DETOXIFICATION') }}
                            </a>
                            <div id="collapse13" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.13DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse14" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.14DETOXIFICATION') }}
                            </a>
                            <div id="collapse14" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.14DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse15" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.15DETOXIFICATION') }}
                            </a>
                            <div id="collapse15" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.15DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse16" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.16DETOXIFICATION') }}
                            </a>
                            <div id="collapse16" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.16DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <a class="item-question collapsed" role="button" data-toggle="collapse"
                               data-parent="#accordion1" href="#collapse17" aria-expanded="false"
                               aria-controls="collapse2a">
                                {{ __('about.17DETOXIFICATION') }}
                            </a>
                            <div id="collapse17" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body">
                                    {!! __('about.17DETOXIFICATION CONTENT') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
