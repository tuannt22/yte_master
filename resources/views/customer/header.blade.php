<header class="container-header" id="header">
	<div class="container">
		<div class="row">
			<div class="header-top">
				<div class="col-md-3 col-sm-3 col-xs-6 col-md">
					<div class="logo">
						<a href="index.html">
							<img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
							<img class="img-logo" src="content/img/color_logo_2.png">
						</a>
					</div>
				</div>
				<div class="col-md-9 col-sm-9" style="padding-left: 0;padding-right: 0">
							<!-- <div class="col-lg-6 col-md-6 col-sm-3 hide-fix hidden-xs ms-hide" style="padding: 15px 0 0">
								<div class="logo-text">
									<span><i class="far fa-clock"></i>Chúng tôi mở cửa 24/7, bao gồm cả ngày lễ và cuối tuần</span>
								</div>
							</div> -->
							<div class="col-sm-3 hidden-xs hidden-lg hidden-md" style="padding: 0">
								<div class="hotline">
									<span class="montserrat">
										<img src="content/img/telephone.png" class="tel">
										<img src="content/img/telephone (1).png" class="tel1">
										<a href="tel:19003053">1900 3053</a>
									</span>
								</div>
							</div>
							<div class="col-lg-12 col-md-9 col-sm-9 hide-fix hidden-xs ms-right"  style="padding: 15px 0 0">
								<div class="menu-top">
									<ul>
										<li><a >Tin tức</a></li>
										<li><a href="faqs.html">Hỏi đáp</a></li>
										<li><a href="#footer">Liên hệ</a></li>
										<li class="social">
											<a style="padding: 5px 10px;"><i class="fa fa-facebook-f"></i></a>
											<a style="padding: 5px 8px;"><i class="fa fa-youtube"></i></a>
											<a style="padding: 5px ;" href="index.html">VN</a>
											<a style="padding: 5px 6px;" href="index.en.html">EN</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-10 col-md-9 col-sm-12" style="padding-left: 0;padding-right: 0">
								<div id="sub-menu">
									<div id="cssmenu">
										<ul class="nav-mb">
											<li><a href="index.html">Trang chủ</a></li>


											<li class="has-sub active">

												<a href="about.html" class="mobile-a">Giới thiệu</a>
												<ul >
													<li><a href="#thong-diep">Thông điệp từ CEO</a></li>
													<li><a href="#tai-sao">Tại sao lựa chọn IHC ?</a></li>
													<li><a href="#tam-nhin">Tầm nhìn - Sứ mệnh – Giá trị cốt lõi</a></li>

													<li><a href="#doi-ngu">Đội ngũ</a></li>
													<li><a href="#cam-nhan">Khách hàng nói về chúng tôi</a></li>
													<!-- <li><a>Liên hệ</a></li> -->
												</ul>
											</li>
											<li class="has-sub">
												<a href="service.html" class="mobile-a">Dịch vụ</a>
												<ul>
													<li class="has-sub">

														<a >Nâng cao sức khỏe</a>
														<ul>
															<li><a href="service.html#nang-cao-suc-khoe">Nâng cao sức khỏe</a></li>
															<li><a href="service.html#tam-soat-chuyen-sau">Tầm soát chuyên sâu</a></li>
															<li><a href="service.html#thaidoc">Thải độc</a></li>
															<li><a href="service.html#chong-lao-hoa">Chống lão hóa</a></li>
															<li><a href="service.html#phuc-hoi-sau-dieu-tri">Phục hồi sau điều trị</a></li>
														</ul>
													</li>
													<li class="has-sub">

														<a >Điều trị bệnh mãn tính</a>
														<ul>
															<li><a href="service.html#tieu-duong">Bệnh tiểu đường</a></li>
															<li><a href="service.html#tim-mach">Bệnh tim mạch</a></li>
															<li><a href="service.html#huyet-ap">Bệnh tăng huyết áp</a></li>
															<li><a href="service.html#ung-thu">Bệnh ưng thư</a></li>
														</ul>
													</li>
													<li class="has-sub">

														<a >Điều trị bằng công nghệ cao</a>
														<ul>
															<li><a href="service.html#lieu-phap">Công nghệ tế bào gốc</a></li>
															<li><a href="service.html#phau-thuat">Phẫu thuật khối u bằng robot Cyberknife</a></li>
														</ul>
													</li>
													<li class="has-sub">

														<a >Phẫu thuật thẩm mĩ</a>
														<ul>
															<li><a href="service.html#phau-thuat-mat">Phẫu thuật mặt</a></li>
															<li><a href="service.html#phau-thuat-nguc">Phẫu thuật ngực</a></li>
															<li><a href="service.html#hut-mo">Hút mỡ</a></li>
															<li><a href="service.html#thu-hep">Thu hẹp âm đạo</a></li>
															<li><a href="service.html#cay-toc">Cấy ghép tóc</a></li>
														</ul>
													</li>
												</ul>
											</li>
											<li><a href="goi-tri-lieu.html">Gói trị liệu</a></li>
											<li class="has-sub">

												<a href="partner.html" class="mobile-a">Đối tác</a>
												<ul >
													<li><a href="partner.html#chau-au">Đối tác tại Châu Âu</a></li>
													<li><a href="partner.html#nhat-ban">Đối tác tại Nhật Bản</a></li>
													<li><a href="partner.html#hoa-ky">Đối tác tại Hoa Kỳ</a></li>
													<li><a href="partner.html#thai-lan">Đối tác tại Thái Lan</a></li>
												</ul>
											</li>
											<li  class="has-sub">

												<a href="procedure.html" class="mobile-a">Thủ tục</a>
												<ul>
													<li><a href="procedure.html#quy-trinh">Quy trình dịch vụ </a></li>
													<!-- <li><a>Chi phí điều trị </a></li> -->
												</ul>
											</li>
											<li  class="has-sub">

												<a href="library.html" class="mobile-a">Thư viện</a>
												<ul >
													<li><a>Truyền thông </a></li>
													<li><a>Tư vấn sức khỏe  </a></li>
													<li><a>Tư vấn dinh dưỡng </a></li>
													<li><a>Điều khoản pháp lý </a></li>
													<li><a>Hình ảnh</a></li>
												</ul>
											</li>
											<li class="hidden-lg hidden-md hidden-sm">
												<div class="mobile-menu">
													<div class="menu-top">
														<ul class="eg">
															<li><a >Tin tức</a></li>
															<li><a href="faqs.html">Hỏi đáp</a></li>
															<li><a href="#footer">Liên hệ</a></li>
															<li class="has-sub">
																<a>English</a>
																<ul>
																	<li><a style="padding: 5px ;" href="index.html">VN</a></li>
																	<li><a style="padding: 5px 6px;" href="index.en.html">EN</a></li>
																</ul>
															</li>
															<li class="social social-mobile">
																<a ><i class="fa fa-facebook-f"></i></a>
																<a ><i class="fa fa-youtube"></i></a>
															</li>
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-3 hidden-xs hidden-sm" style="padding: 0">
								<div class="hotline">
									<span class="montserrat">
										<img src="content/img/telephone.png" class="tel">
										<img src="content/img/telephone (1).png" class="tel1">
										<a href="tel:19003053"> 1900 3053</a>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
</header>
