@extends('customer.layout')

@section('header')
    <div class="container hidden-xs">
        <div class="row ">
            <div class="header-top">
                <div class="col-md-3 col-sm-3 col-xs-6 col-md">
                    <div class="logo">
                        <a href="/">
                            <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                            <img class="img-logo" src="content/img/color_logo_2.png">
                        </a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9" style="padding-left: 0;padding-right: 0">
                    <div class="col-sm-3 hidden-xs hidden-lg hidden-md" style="padding: 0">
                        <div class="hotline">
                        <span class="montserrat">
                            <img src="content/img/telephone.png" class="tel">
                            <img src="content/img/telephone (1).png" class="tel1">
                            <a href="tel:19003053"> 1900 3053</a>
                        </span>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-9 col-sm-9 ms-right" style="padding: 15px 0 0">
                        <div class="menu-top hide-fix">
                            <ul>
                                <li><a href="/news">{{__('menus.news')}}</a></li>
                                <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.faq')}}</a></li>
                                <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                <li class="social">
                                    <a style="padding: 5px 10px;" href="https://www.facebook.com/infinityhealthcarevietnam/"><i
                                            class="fa fa-facebook-f"></i></a>
                                    <a style="padding: 5px 8px;" href="https://www.youtube.com/channel/UCgacemtU2bpQLcWTsTZhYWg/featured?view_as=subscriber"><i
                                            class="fa fa-youtube"></i></a>
                                    <a style="padding: 5px ;"
                                       href="{!! route('user.change-language', ['vi']) !!}">VN</a>
                                    <a style="padding: 5px 6px;"
                                       href="{!! route('user.change-language', ['en']) !!}">EN</a>
                                <!-- <a style="padding: 5px 6px;" href="{!! route('user.change-language', ['ja']) !!}">JA</a> -->
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-12" style="padding-left: 0;padding-right: 0">
                            <div id="sub-menu">
                                <div id="cssmenu">
                                    <ul class="nav-mb">
                                        <li><a href="/">{{__('menus.home')}}</a></li>

                                        @foreach($data as $menus)
                                            <li class="has-sub ">
                                                <a href="{{ $menus->links}}" class="mobile-a"
                                                   id="{{$menus->id}}">{{__($menus->name)}}
                                                </a>

                                                <ul>
                                                    @if($menus->links == '/about')
                                                        @foreach($menus->abouts as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.about.showAbout')}}#{{ $titles->links }}">{{$titles->title}}</a>
                                                            </li>
                                                        @endforeach
                                                        <li>
                                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                                            </a>
                                                        </li>
                                                    @endif


                                                    @if($menus->links == '/partners')
                                                        @foreach($menus->partners as $titles)
                                                            <li>
                                                                <a
                                                                    href="{{route('customer.partners.showPartners_detail')}}#{{$titles->id}}">{{$titles->title}}</a>
                                                            </li>
                                                        @endforeach
                                                    @endif

                                                    @if($menus->links == '/services')
                                                        @foreach($menus->services as $titles)
                                                            <li class="has-sub">
                                                                <a class="mobile-a">{{$titles->title}}</a>

                                                                <ul>

                                                                    @foreach($dv as $tt)
                                                                        @if($titles->id == $tt->services_id)
                                                                            <a
                                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                                        @endif
                                                                    @endforeach

                                                                </ul>

                                                            </li>
                                                        @endforeach
                                                    @endif


                                                </ul>

                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 hidden-xs hidden-sm" style="padding: 0">
                            <div class="hotline">
                            <span class="montserrat">
                                <img src="content/img/telephone.png" class="tel">
                                <img src="content/img/telephone (1).png" class="tel1">
                                <a href="tel:19003053"> 1900 3053</a>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hidden-lg hidden-sm hidden-md">
        <div class="header"></div>
        <div class="logo-mobile">
            <a href="/">
                <img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
                <img class="img-logo" src="content/img/color_logo_2.png">
            </a>
        </div>
        <input type="checkbox" class="openSidebarMenu" id="openSidebarMenu"/>
        <label for="openSidebarMenu" class="sidebarIconToggle">
            <div class="spinner diagonal part-1"></div>
            <div class="spinner horizontal"></div>
            <div class="spinner diagonal part-2"></div>
        </label>
        <div id="sidebarMenu">
            <nav id="sidebar-mobile" class="nav">
                <ul class="sidebarMenuInner list-unstyled components">
                    <li><a href="/">{{__('menus.home')}}</a></li>
                    @foreach($data as $menus)
                        <li>
                            @if(in_array($menus->id, [23,29]))
                                <a type="button" data-toggle="collapse" data-target="#homeSubmenu{{$menus->id}}"
                                   style="display: block;">
                                    {{__($menus->name)}}
                                    <i class="fas fa-angle-right"></i>
                                </a>
                                <ul class="collapse list-unstyled" id="homeSubmenu{{$menus->id}}">
                                    @foreach($menus->abouts as $title)
                                        <li>
                                            <a href="{{ url('/about') }}#{{ $title->links }}">{{$title->title}}</a>
                                        </li>
                                    @endforeach
                                    @if($menus->id === 23)
                                        <li>
                                            <a href="{{route('customer.procedure.showProcedure')}}">
                                                @if(session('website_language') == 'vi') Thủ tục @elseif(session('website_language') == 'en') Procedure @endif
                                            </a>
                                        </li>
                                    @endif
                                    @foreach($menus->services as $title)
                                        <li>
                                            <a data-target="#homeSubmenu{{$title->id}}" data-toggle="collapse"
                                               type="button">{{$title->title}}
                                                <i class="fas fa-angle-right"></i>
                                            </a>
                                            <ul class="collapse list-unstyled" id="homeSubmenu{{$title->id}}">
                                                @foreach($dv as $tt)
                                                    @if($title->id == $tt->services_id)
                                                        <li>
                                                            <a
                                                                href="{{route('customer.services.showServices')}}#{{$tt->links}}">{{$tt->title_con}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            @else
                                <a href="{{$menus->links}}" style="display: block;">{{__('menus.'.$menus->name)}}</a>
                            @endif
                        </li>
                    @endforeach
                    <li><a href="/faqs">{{__('menus.faq')}}</a></li>
                    <li class="hidden-lg hidden-md hidden-sm">
                        <div class="mobile-menu">
                            <div class="menu-top-mobile">
                                <ul class="eg">
                                    <li><a href="/news">{{__('menus.news')}}</a></li>
                                    <li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.Hỏi đáp')}}</a></li>
                                    <li><a href="/about#footer">{{__('menus.contact')}}</a></li>
                                    <li class="">
                                        <a data-target="#lang" data-toggle="collapse" type="button">English</a>
                                        <ul class="collapse list-unstyled" id="lang">
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.html">VN</a>
                                            </li>
                                            <li><a style="padding: 5px 6px 5px 45px;" href="index.en.html">EN</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </div>

                        </div>
                    </li>
                </ul>
            </nav>
            <div class="social social-mobile">
                <a><i class="fa fa-facebook-f"></i></a>
                <a><i class="fa fa-youtube"></i></a>
            </div>
        </div>
    </div>

@endsection

@section('menu')
    <div class="item-br">

        @foreach($image as $img)
            <img class="hidden-xs" src="uploads/menus/{{$img->image}}" style="width: 1920px; height: 580px">
        @endforeach
        <img class="hidden-lg hidden-md hidden-sm" src="content/img/mobile/Gioi thieu.jpg">
        <div class="title-br hidden-lg hidden-md hidden-sm">
            <h2>{{ __('about.Therapy package') }}</h2>
        </div>
        <div class="menu-line hidden-xs">
            <div class="container">
                <div class="row">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>{{ __('about.Therapy package') }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <!-- start menu bên trái -->
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 hidden-xs fadeInDown animated" id="sidebar">
                <div class="sidebar">
                    <div class="sidebar-h3">
                        <h3>{{ __('about.Therapy package') }}</h3>
                    </div>
                    <div class="list-sidebar">
                        <div class="wrapper">
                            <!-- Sidebar -->
                            <nav id="sidebar" class="nav">
                                <ul class="list-unstyled components">
                                    <li><a href="/">{{ __('menus.home') }}</a></li>
                                    @foreach($data as $menus)
                                        <li>
                                            <a type="button" data-toggle="collapse"
                                               data-target="#homeSubmenu{{$menus->id}}" style="display: block;"
                                               href="{{ $menus->links}}">{{$menus->name}}

                                            </a>

                                            <ul class="collapse list-unstyled" id="homeSubmenu{{$menus->id}}">

                                                @foreach($menus->abouts as $title)
                                                    <li>
                                                        <a href="{{route('customer.about.showAbout')}}#{{$titles->links}}">{{$title->title}}</a>

                                                    </li>
                                                @endforeach

                                                @foreach($menus->services as $title)
                                                    <li>
                                                        <a data-target="#homeSubmenu{{$title->id}}"
                                                           data-toggle="collapse" type="button">{{$title->title}}
                                                            <i class="fas fa-plus"></i>
                                                        </a>
                                                        <ul class="collapse list-unstyled"
                                                            id="homeSubmenu{{$title->id}}">
                                                            @foreach($dv as $tt)
                                                                @if($title->id == $tt->services_id)
                                                                    <li>

                                                                        <a href="">{{$tt->title_con}}
                                                                        </a>

                                                                    </li>
                                                                @endif
                                                            @endforeach

                                                        </ul>
                                                    </li>
                                                @endforeach

                                            </ul>
                                        </li>
                                    @endforeach
                                    <li><a href="/faqs">{{__('menus.faq')}}</a></li>
                                </ul>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>

            <!-- end menu bên trái -->

            <!-- Start Content -->

            <div class="col-md-9 col-sm-8 float">
                <div class="title-h2-bg">
                    <h2>{{ __('about.Therapy package') }}</h2>
                </div>
                <div style="margin-bottom: 120px">
                    <div class="mgr-40" id="Jean-Pierre-Naim">
                        <table class="table table-bordered">
                            <thead>
                            <tr class="blue">
                                <th width="35%">{{ __('about.package') }}</th>
                                <th width="15%">{{ __('about.Location') }}</th>
                                <th width="35%">{{ __('about.Contents') }}</th>
                                <th width="15%">{{ __('about.Duration') }}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($packages as $pack)

                                <tr class="success">
                                    <td id="{{$pack->id}}">{{$pack->name}}

                                        <br><a href="/services#{{$pack->link}}">Tham khảo</a>
                                    </td>
                                    <td>{{$pack->address}}</td>
                                    <td>{!!$pack->content!!} </td>
                                    <td>{{$pack->date}}</td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                        <div>
                            <p>
                                {{ __('about.Therapy package note') }}
                            </p>


                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
