@extends('customer.layout')

@section('header')
<div class="container">
	<div class="row">
		<div class="header-top">
			<div class="col-md-3 col-sm-3 col-xs-6 col-md">
				<div class="logo">
					<a href="/">
						<img class=" img-logo-color" src="content/img/color_logo_3 (1).png" style="display: none">
						<img class="img-logo" src="content/img/color_logo_2.png">
					</a>
				</div>
			</div>
			<div class="col-md-9 col-sm-9" style="padding-left: 0;padding-right: 0">
							<!-- <div class="col-lg-6 col-md-6 col-sm-3 hide-fix hidden-xs ms-hide" style="padding: 15px 0 0">
								<div class="logo-text">
									<span><i class="far fa-clock"></i>Chúng tôi mở cửa 24/7, bao gồm cả ngày lễ và cuối tuần</span>
								</div>
							</div> -->
							<div class="col-sm-3 hidden-xs hidden-lg hidden-md" style="padding: 0">
								<div class="hotline">
									<span class="montserrat">
										<img src="content/img/telephone.png" class="tel">
										<img src="content/img/telephone (1).png" class="tel1">
										<a href="tel:19003053">1900 3053</a>
									</span>
								</div>
							</div>
							<div class="col-lg-12 col-md-9 col-sm-9 hide-fix hidden-xs ms-right"  style="padding: 15px 0 0">
								<div class="menu-top">
									<ul>
										<li><a href="/news">{{__('menus.news')}}</a></li>
												<li><a href="/faqs#cau-hoi-thuong-gap">{{__('menus.faq')}}</a></li>
												<li><a href="/services#footer">{{__('menus.contact')}}</a></li>
										<li class="social">
											<a href="https://www.facebook.com/infinityhealthcarevietnam/" style="padding: 5px 10px;"><i class="fa fa-facebook-f"></i></a>
											<a href="https://www.youtube.com/channel/UCgacemtU2bpQLcWTsTZhYWg/featured?view_as=subscriber" style="padding: 5px 8px;"><i class="fa fa-youtube"></i></a>
											<a style="padding: 5px ;" href="{!! route('user.change-language', ['vi']) !!}">VN</a>
											<a style="padding: 5px 6px;" href="{!! route('user.change-language', ['en']) !!}">EN</a>
											<!-- <a style="padding: 5px 6px;" href="{!! route('user.change-language', ['ja']) !!}">JA</a> -->
										</li>
									</ul>
								</div>
							</div>
							<div class="col-lg-10 col-md-9 col-sm-12" style="padding-left: 0;padding-right: 0">
								<div id="sub-menu">
									<div id="cssmenu">
										<ul >
											<li><a href="/">{{__('menus.home')}}</a></li>

											@foreach($data as $menus)
											<li class="has-sub ">
												<a href="{{ $menus->links}}" class="mobile-a" id="{{$menus->id}}">{{__('menus.'.$menus->name)}}</a>

												<ul >
													@if($menus->links == '/about')
														@foreach($menus->abouts as $titles)
														<li><a href="{{route('customer.about.showAbout')}}#{{$titles->links}}">{{$titles->title}}</a></li>
														@endforeach
													@endif

													@if($menus->links == '/partners')
														@foreach($menus->partners as $titles)
														<li><a href="{{route('customer.partners.showPartners_detail')}}#{{$titles->id}}#{{$titles->id}}">{{$titles->title}}</a></li>
														@endforeach
													@endif
												</ul>

											</li>
											@endforeach


											<li class="hidden-lg hidden-md hidden-sm">
												<div class="mobile-menu">
													<div class="menu-top">
														<ul class="eg">
															<li><a >Tin tức</a></li>
															<li><a href="faqs.html">Hỏi đáp</a></li>
															<li><a href="#footer">Liên hệ</a></li>
															<li class="has-sub">
																<a>English</a>
																<ul>
																	<li><a style="padding: 5px ;" href="index.html">VN</a></li>
																	<li><a style="padding: 5px 6px;" href="index.en.html">EN</a></li>
																</ul>
															</li>
															<li class="social social-mobile">
																<a ><i class="fa fa-facebook-f"></i></a>
																<a ><i class="fa fa-youtube"></i></a>
															</li>
														</ul>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-2 col-md-3 hidden-xs hidden-sm" style="padding: 0">
								<div class="hotline">
									<span class="montserrat">
										<img src="content/img/telephone.png" class="tel">
{{--										<img src="content/img/telephone (1).png" class="tel1">--}}
										<a href="tel:19003053"> 1900 3053</a>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endsection

			@section('content')
            @endsection

