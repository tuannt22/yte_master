@extends('admin.layout')

@section('content')
<style>
	#blah img{
		width: 250px;
		height: 200px;
	}
</style>
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="#">Dashboard</a>
	</li>
	<li class="breadcrumb-item "><a href="{{route('admin.about_service.dashboard')}}">Dịch vụ y khoa</a></li>
	<li class="breadcrumb-item ">Thêm bài viết</li>
</ol>
<a href="{{route('admin.about_service.dashboard')}}" class="btn btn-info">Quay lại</a>
<pre></pre>
<div class="row">
	@if (Session::has('fail'))
	<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif
</div>
<form style="margin-bottom: 50px" action="{{route('admin.about_service.add')}}" method="POST" role="form"  enctype="multipart/form-data">

	<legend>Thêm gói dịch vụ: </legend>
	<pre></pre>
	@csrf
    <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
            @foreach ($lang_code as $key => $value)
                <option value="<?php echo $key; ?>" >{{$value}}</option>
            @endforeach 
        </select>
    </div>		
	<div class="form-group">
		<label for="">Tiêu đề:  
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('services_id') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		<select name="services_id" class="form-control">
			@foreach($service as $val)
				<option value="{{$val->id}}">{{$val->title}}</option>
			@endforeach
		</select>
	</div>
	
	<div class="form-group col-lg-4">
		
		<label for="imagePd">Chọn ảnh giới thiệu:(570 x 400)
			@if ($errors->any())
			<span class=" alert-danger">
				<ul>
					@foreach ($errors->get('image') as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</label>
		<input type="file" name="image" id="imagePd">
		<div id="blah"></div>
	</div>
		
	<div class="form-group">
		<label for="">Nội dung giới thiệu: 
			@if ($errors->any())
			<span class=" alert-danger">
				<ul>
					@foreach ($errors->get('content') as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</label>
		
		<textarea id="content" name="content" class="form-control"></textarea>
		<script src="ckeditor/ckeditor.js"></script>
		<script>
			config = {};
			config.entities_latin = false;
			config.language = 'vi';
			config.uiColor = '#AADC6E';

			CKEDITOR.replace( 'content',
			{
				filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
				filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				filebrowserBrowseUrl: '/browser/browse.php?type=Images',
				filebrowserUploadUrl: '/uploader/upload.php?type=Files'
			});

		</script>
	
 

	<div class="col-lg-4">
		<button type="submit" class="btn btn-primary">Thêm</button>
	</div>
</form>
<script>
    function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var img = $('<img>');
            img.attr('src', e.target.result);
            $('#blah').append(img);

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>	
@endsection