@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">Dashboard</a>
    </li>
    <li class="breadcrumb-item "><a href="{{route('admin.service.dashboard')}}">Service</a></li>
    <li class="breadcrumb-item "><a href="{{route('admin.about_service.dashboard')}}">Dịch vụ y khoa</a></li>
    <li class="breadcrumb-item ">Sửa nội dung</li>
</ol>
<a href="{{route('admin.about_service.dashboard')}}" class="btn btn-info">Quay lại</a>
<br>
<form style="margin-bottom: 50px" action="{{route('admin.about_service.update', $data->id)}}" method="POST" role="form"  enctype="multipart/form-data">
    <br>
    <legend>Chỉnh sửa dịch vụ y khoa: 
    </legend>
    <br>
    @csrf
    <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
          @foreach ($lang_code as $key => $value)
              <option value="<?php echo $key; ?>" <?php if($key == $data->lang_code) {echo 'selected';} ?> >{{$value}}</option>
          @endforeach 
        </select>
    </div>  
    <div class="row">
        <div class="form-group col-lg-6">
            <label for=""> <h4>Tiêu đề:</h4>  
            </label>
            <select name="optServies" class="form-control">
                @foreach($services as $val)
                <option {{ $val->id ==  $data->services_id ? 'selected' : ''}} value="{{$val->id}}">{{$val->title}}</option>
                @endforeach
            </select>
        </div>

    </div>
    
    <div class="form-group">
        <label for="imagePd">
            <h4>Chọn ảnh giới thiệu: (570 x 400)</h4>   
            @if ($errors->any())
            <div class="alert-danger">
                <ul>
                    @foreach ($errors->get('image') as $message)
                    <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </label>

        <div>
            <input type="file" name="image" id="imagePd"> 
        </div>
        <div style="margin-top: 10px" class="col-lg-6"><img id="blah" src="uploads/about_service/{{$data->image}}" width="250" height="200"></div>
    </div>
</div>
<div class="form-group col-lg-11" style="margin-top: -30px;">
    <label for="contentPd">
        <h4>Giới thiệu:</h4> 
        @if ($errors->any())
        <span class=" alert-danger">
            <ul>
             @foreach ($errors->get('content') as $message)
             <li>{{ $message }}</li>
             @endforeach
         </ul>
     </div>
     @endif
 </label>
 <textarea name="content" id="contentPd" name="content" class="form-control"><?= $data->content ?></textarea>
 <script src="ckeditor/ckeditor.js"></script>
 <script>
    config = {};
    config.entities_latin = false;
    config.language = 'vi';
    config.uiColor = '#AADC6E';
    config.toolbarGroups = [
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    { name: 'forms', groups: [ 'forms' ] },
    '/',
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'insert', groups: [ 'insert' ] },
    '/',
    { name: 'styles', groups: [ 'styles' ] },
    { name: 'colors', groups: [ 'colors' ] },
    { name: 'tools', groups: [ 'tools' ] },
    { name: 'others', groups: [ 'others' ] },
    { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'Flash,Table,Smiley,Iframe,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,About,Link,Unlink,Anchor,CreateDiv,Blockquote,Source,Save,NewPage,Preview,Print,Templates,ShowBlocks';
    CKEDITOR.replace( 'contentPd', config,
    {
        filebrowserBrowseUrl: '../../ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '../../ckfinder/ckfinder.html?type=Images',
        filebrowserUploadUrl:
        '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/',
        filebrowserImageUploadUrl:
        '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/'
    });
</script>
</div>
<input type="hidden" value="{{$data->services_id}}" name="services_id">
<div class="col-lg-4" style="margin-bottom: 20px">
    <button type="submit" class="btn btn-primary">Sửa</button>
</div>
</form>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imagePd").change(function(){
        readURL(this);
    });
</script>
@endsection