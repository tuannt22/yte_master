@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="/admin/dashboard">Dashboard</a>
	</li>
	<li class="breadcrumb-item "><a href="{{route('admin.service.dashboard')}}">Dich vụ và Dịch vụ y khoa</a></li>
	<li class="breadcrumb-item "><a href="{{route('admin.about_service.dashboard')}}">Dịch vụ y khoa</a></li>
	<li class="breadcrumb-item ">Xem nội dung</li>
</ol>
<div class="container-fluid">
	<a href="{{route('admin.about_service.dashboard')}}" class="btn btn-info">Quay lại</a>

	<hr>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif

	
	<div class="row">
		<div class="col-lg-4">
			<img class="img hidden-xs" src="uploads/about_service/{{$show->image}}">
		</div>
	</div>
	<div class="row" style="margin-top: -80px">
		<div class="col-lg-4">
			<div class=" text-left">
					<div class="title-h3  ">
						<h3 class="montserrat ">{{$show->title}}</h3>
					</div>
					<div>
						<?php echo $show->content ?>
					</div>
				
				</div>
		</div>
	</div>
	<a href="{{route('admin.about_service.edit', $show->id)}}" class="btn btn-warning">Edit</a>
	<a href="#" data-toggle="modal" data-target="#delete{{$show->id}}"class="btn btn-danger">Delete</a>

	<pre></pre>

	<div class="modal fade" id="delete{{$show->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a href="{{route('admin.about_service.destroy', $show->id)}}" class="btn btn-danger">Delete</a>
				</div>
			</div>
		</div>


	</div>
@endsection()