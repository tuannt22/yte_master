@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
                         <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active"><a href="{{route('admin.service.dashboard')}}">Dich vụ và Dịch vụ y khoa</a></li>
                          <li class="breadcrumb-item active">Dịch vụ y khoa</li>
                        </ol>
	
	<a href="{{route('admin.services_detail.dashboard')}}" class="btn btn-info">Dịch vụ</a>
	<a  class="btn btn-default" style="border-style: 1px; border-color: black">Dịch vụ y khoa</a>
	<br><br>
	@if($count < 4)
		<a href="{{route('admin.about_service.showadd')}}" class="btn btn-info">Thêm</a>
	@endif

	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	
		


<?php $count = 1;?>
	<table class="table table-hover table-bordered" style="margin-top: 10px">
	<thead>
		<tr>
			<th>STT</th>
			<th>Banner</th>
			<th>Tên tiêu đề và menu</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $val)
		<tr>
			<td>{{$count++}}</td>
			<td><img src="uploads/about_service/{{$val->image}}" alt="" width="250" height="200"></td>
			<td>{{$val->title}}</td>
			
			<td><a href="{{route('admin.about_service.show', $val->id)}}" class="btn btn-info">Xem</a></td>
			<td><a href="{{route('admin.about_service.destroy', $val->id)}}" class="btn btn-danger">Xóa</a></td>
		@endforeach
	
	</tbody>
</table>
<br>

<br>
@endsection()
