@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active"><a href="{{route('admin.service.dashboard')}}">Service</a></li>
                          <li class="breadcrumb-item active">Dịch vụ y khoa</li>
                        </ol>
                          
                        </ol>
	<a href="{{route('admin.about_service.showadd', $find->id)}}" class="btn btn-info">Thêm dịch vụ</a>
	<br> <br>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	
		


<?php $count = 1;?>
	<table class="table table-hover table-bordered">
	<thead>
		<tr>
			<th>STT</th>
			<th>Tên tiêu đề và menu</th>
			<th colspan="3">Giới thiệu dịch vụ</th>
			<th colspan="3">Chi tiết dịch vụ</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $val)
		<tr>
					<td>{{$count++}}</td>
					
					<td>{{$val->content}}</td>
					<td><a href="{{route('admin.about_service.show', $val->id)}}" class="btn btn-primary">Show</a></td>
					<td><a href="{{route('admin.about_service.edit', $val->id)}}" class="btn btn-warning">Edit</a></td>
					<td><a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a></td>
				</tr>


				<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<a href="{{route('admin.about_service.destroy', $val->id)}}" class="btn btn-danger">Delete</a>
							</div>
						</div>
					</div>									
				</div>
		
		@endforeach
	
	</tbody>
</table>
<br>

<br>
@endsection()