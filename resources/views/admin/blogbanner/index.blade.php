@extends('admin.layout')

@section('content')
    <style>
        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .btn-default .active{
            color: #333;
            background-color: #e6e6e6;
            border-color: #adadad;
        }
    </style>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('admin.dashboard')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Blog Banner</li>
    </ol>
    <a href="{{route('admin.blog_banner.create')}}" class="btn btn-info"><i class="fa fa-plus"></i> Tạo Banner</a>
    <pre></pre>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @else
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif

    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Banner</th>
            <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($banners as $val)
            <tr>
                <td>{{$loop->index + 1}}</td>
                <td>{!! $val->content !!}</td>
                <td>
                    <input type="checkbox" @if ($val->status == 1) checked @endif class="activeBanner" data-toggle="toggle" value="{{ $val->id }}">
                </td>
                <td>
                    <a href="{{route('admin.blog_banner.edit', $val->id)}}" class="btn btn-primary">Sửa</a>
                    <a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Xoá</a>
                </td>

            </tr>
            <div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <form action="{{route('admin.blog_banner.destroy', $val->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
        </tbody>
    </table>
    <div class="m-auto">
        {{ $banners->links() }}
    </div>
    <br>
    <br>
    <script type="application/javascript">
        $('.activeBanner').change(function () {
            var id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{ route('admin.activeBanner') }}',
                data: {id: id},
                success: function (res) {
                    location.reload();
                }
            })
        });
    </script>
@endsection

