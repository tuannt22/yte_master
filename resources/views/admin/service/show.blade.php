@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Giới thiệu dịch vụ</li>
                        </ol>
                        <a href="{{route('admin.service.dashboard')}}" class="btn btn-info">Quay lại</a>
                        <br><br>
<div style="display: flex; justify-content:flex-start">
			
			
			
		</div>
		
		<div class=" mgr-20 mgr-40 more" >
			<div class="title-h3 ">
				<img style="width: 50px; height: 50px; float: left;" class="img hidden-xs" src="uploads/services/{{$show->icon}}">
				<h3 class=" moveUp">{{$show->title}}</h3>
			</div>
			
		</div>										
	<br><br>
	<a href="{{route('admin.service.edit', $show->id)}}" class="btn btn-warning">Edit</a>

	<a href="#" data-toggle="modal" data-target="#delete{{$show->id}}"class="btn btn-danger">Delete</a>

	<hr  width="100%" size="10px" align="center" color="red" />
	<div class="modal fade" id="delete{{$show->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a href="{{route('admin.service.destroy', $show->id)}}" class="btn btn-danger">Delete</a>
				</div>
			</div>
		</div>
	</div>
		


<br>
<br>
@endsection()