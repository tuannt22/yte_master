@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Dich vụ và Dịch vụ y khoa</li>
                        </ol>
    <a href="{{route('admin.services_detail.dashboard')}}" class="btn btn-info">Dịch vụ</a>
	<a href="{{route('admin.about_service.dashboard')}}" class="btn btn-info">Dịch vụ y khoa</a>
	
	<br> <br> 
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>

	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	
		



@endsection()