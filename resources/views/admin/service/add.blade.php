@extends('admin.layout')

@section('content')
<style>
	#blah img {
		width: 250px;
		height: 200px;
	}
</style>
<a href="{{route('admin.services_detail.dashboard')}}" class="btn btn-info">Quay lại</a>
<pre></pre>
<div class="row">
	<div class="col-lg-12">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	@if (Session::has('fail'))
	<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif
</div>
<form style="margin-bottom: 50px" action="{{route('admin.service.add')}}" method="POST" role="form"  enctype="multipart/form-data">
	<legend>Thêm gói dịch vụ: </legend>
	@csrf
    <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
            @foreach ($lang_code as $key => $value)
                <option value="<?php echo $key; ?>" >{{$value}}</option>
            @endforeach 
        </select>
    </div> 		
	<div class="form-group">	
		<label for="">Tiêu đề lớn: </label>
		<input type="text" class="form-control" id="" placeholder="Tiêu đề lớn"  name="title_pa">
	</div>
	<div class="form-group">
		<label for="imagePd">Icon:(50 x 50) </label>
		<input type="file" name="image" id="imagePd" class="form-control" >
		 <div id="blah"></div>
	</div>


	<div class="form-group">
			@foreach ($data as $menu)    	    
			<input type="hidden" value="{{ $menu->id }}" name="menu_id">  
			@endforeach

	</div>
 	<div class="form-group">	
		<label for="">Link tiêu đề: </label>
		<input type="text" class="form-control" id="" placeholder="Link tiêu đề"  name="link">
	</div>
	<div class="form-group">	
		<label for="">Nội dung: </label>
		<textarea class="form-control" id="" placeholder="Thêm nội dung"  name="content"></textarea>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<script src="ckeditor/ckeditor.js"></script>
<script>
	
config = {};
config.entities_latin = false;
config.language = 'vi';
config.uiColor = '#AADC6E';

CKEDITOR.replace( 'content',
{
  filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
  filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
  filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
  filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
  filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
  filebrowserBrowseUrl: '/browser/browse.php?type=Images',
  filebrowserUploadUrl: '/uploader/upload.php?type=Files'
});
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var img = $('<img>');
            img.attr('src', e.target.result);
            $('#blah').append(img);

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>
@endsection