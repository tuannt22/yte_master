@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{route('admin.dashboard')}}">Dashboard</a>
	</li>
	<li class="breadcrumb-item active">Bài viết</li>
</ol>
<div class="container-fluid">
	<a href="{{route('admin.news.dashboard')}}" class="btn btn-info">Quay lại</a>

	<hr>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	
	<div id="thong-diep">
		<div class="title-h2-bg">
			<h2>
				
				<?php echo $show->title ?>
			</h2>
		</div>							
		<div>
			{!!$show->content!!}
		</div>
	</div>									
</div>
<a href="{{route('admin.news.edit', $show->id)}}" class="btn btn-warning">Edit</a>
<a href="#" data-toggle="modal" data-target="#delete{{$show->id}}"class="btn btn-danger">Delete</a>

<pre></pre>

<div class="modal fade" id="delete{{$show->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a href="{{route('admin.news.destroy', $show->id)}}" class="btn btn-danger">Delete</a>
			</div>
		</div>
	</div>

	
</div>
@endsection()