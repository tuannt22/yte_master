@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          
                          <li class="breadcrumb-item active">Tin tức</li>
                        </ol>
<div class="container-fluid">	

	<a href="{{route('admin.news.addNews')}}" class="btn btn-info">Thêm tin tức</a>
	<pre></pre>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@endif

	@if(session()->has('fail'))
	<div class="alert alert-danger">
		{{ session()->get('fail') }}
	</div>
	@endif

		<?php $count = 1?>
		<table class="table table-hover" style="margin-top: 10px">
			<thead>
				<tr>
					<th>STT</th>
					<th>Tiêu đề</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data as $val)
				<tr>
					<td>{{$count++}}</td>
					<td>{{$val->title}}</td>
					<td>
						<a href="{{route('admin.news.show', $val->id)}}" class="btn btn-warning">Show</a>
						<a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a>
					</td>

				</tr>
				<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<a href="{{route('admin.news.destroy', $val->id)}}" class="btn btn-danger">Delete</a>
							</div>
						</div>
					</div>									
				</div>
		
				@endforeach
			</tbody>
		</table>
		
	</div>

	@endsection()