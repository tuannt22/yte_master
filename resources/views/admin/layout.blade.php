<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <base href="{{asset('')}}">
  <title>Admin HealthCare - Dashboard</title>


  <!-- Custom fonts for this template-->
  <link href="admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <!-- Custom styles for this template-->
  <link href="admin/css/sb-admin.css" rel="stylesheet">
  <!-- Latest compiled and minified CSS -->

  <link rel="shortcut icon" type="image/x-icon" href="content/img/favicon.ico">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" >
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700&amp;subset=vietnamese" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700&display=swap" rel="stylesheet">
  <script type="text/javascript " src="plugins/jquery/jquery-3.4.1.min.js"></script>
  <link rel="stylesheet" type="text/css" href="plugins/menu/styles.css">
  <script type="text/javascript" src="plugins/menu/script.js"></script>
  <script type="text/javascript" src="plugins/owl-carausel/owl.carousel.min.js"></script>
  <script type="text/javascript" src="content/js/jquery.js"></script>
  <link rel="stylesheet" type="text/css" href="plugins/owl-carausel/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" href="plugins/owl-carausel/owl.theme.default.css">
  <link rel="stylesheet" type="text/css" href="content/css/styles.css">
  <link href="plugins/boostrap/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="plugins/boostrap/bootstrap-toggle.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="/admin/dashboard">HealthCare</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <div class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

    </div>
    <div class="input-group">
      <div class="nav-link " href="#" id="userDropdown" role="button" aria-haspopup="true" aria-expanded="false">
        Online: <span style="color: white; "> {{Auth::user()->name}}</span>
      </div>
    </div>

  <!-- Navbar -->
  <ul class="nav navbar-nav navbar-right">

    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user-circle fa-fw"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">

        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
      </div>
    </li>
  </ul>

</nav>

<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav" style="position: relative;">
    <li class="nav-item active">
      <a class="nav-link" href="/admin/dashboard">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Admin</span>
      </a>
    </li>


    <li class="nav-item">
      <a class="nav-link" href="/admin/slide">
        <i class="fa fa-camera-retro"></i>
        <span>Slide</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fab fa-adn"></i>
          <span>Giới thiệu</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a class="dropdown-item" href="/admin/about"> <i class="fab fa-adn"></i> Giới thiệu</a>
          <a class="dropdown-item" href="/admin/ourteam">
            <i class="  fas fa-address-card"></i>
          Đội ngũ</a>
          <a class="dropdown-item" href="/admin/customercomment">
            <i class="fa fa-folder-open"></i>
          Cảm nhận KH</a>
        </div>

      </li>


      <li class="nav-item">
        <a class="nav-link" href="/admin/service">
          <i class="fa fa-stethoscope"></i>
          <span>Dịch vụ</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/admin/package">
            <i class="fa fa-medkit"></i>
            <span>Gói trị liệu</span></a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/admin/partner">
              <i class="fas fa-marker"></i>
              <span>Đối tác</span></a>
            </li>


            <li class="nav-item">
              <a class="nav-link" href="/admin/library">
                <i class="fa fa-folder-open"></i>
                <span>Thư viện</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/admin/news">
                  <i class="far fa-comment-alt"></i>
                  <span>Tin tức</span></a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fab fa-adn"></i>
                    <span>Cài đặt</span>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="/admin/user"> <i class="fa fa-user-circle"></i>
                      <span>User</span></a>
                      <a class="dropdown-item" href="/admin/menu">
                        <i class="  fas fa-address-card"></i>
                      Banner</a>
                    </div>

                  </li>

                    <li class="nav-item">
                       <a class="nav-link" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           <i class="far fa-newspaper"></i>
                           <span>Blogs</span>
                       </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('admin.blogs.index')  }}">
                                <i class="fa fa-comment"></i>
                                <span>Posts</span>
                            </a>
                            <a class="dropdown-item" href="{{ route('admin.topics.index')  }}">
                                <i class="fa fa-folder-open"></i>
                                <span>Topics</span>
                            </a>
                            <a class="dropdown-item" href="{{ route('admin.tags.index')  }}">
                                <i class="fa fa-tags"></i>
                                <span>Tags</span>
                            </a>
                            <a class="dropdown-item" href="{{ route('admin.blog_banner.index')  }}">
                                <i class="  fas fa-ad"></i>
                                <span>Banners</span>
                            </a>
                        </div>
                    </li>

                </ul>

                    <div id="content-wrapper">

                      <div class="container-fluid">

                        <!-- Breadcrumbs-->


                        @yield('content')

                      </div>
                      <!-- /.container-fluid -->

                      <!-- Sticky Footer -->
                      <footer class="sticky-footer">
                        <div class="container my-auto">
                          <div class=" text-center my-auto">
                            <span style="color: white; font-size: 12px">Infinity HealthCare VietNam</span>
                          </div>
                        </div>
                      </footer>

                    </div>
                    <!-- /.content-wrapper -->

                  </div>
                  <!-- /#wrapper -->

                  <!-- Scroll to Top Button-->
                  <a class="scroll-to-top rounded" href="#page-top">
                    <i class="fas fa-angle-up"></i>
                  </a>

                  <!-- Logout Modal-->
                  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-primary" href="{{route('login.logout')}}">Logout</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <style>
                    .morecontent span {
                      display: none;
                    }
                    .morelink {
                      display: block;
                    }
                  </style>

                  <script>
                    $(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 400;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Xem thêm";
    var lesstext = "Rút gọn";


    $('.more').each(function() {
      var content = $(this).html();

      if(content.length > showChar) {

        var c = content.substr(0, showChar);
        var h = content.substr(showChar, content.length - showChar);

        var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

        $(this).html(html);
      }

    });

    $(".morelink").click(function(){
      if($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });
  });
</script>

<!-- Bootstrap core JavaScript-->
<script src="admin/vendor/jquery/jquery.min.js"></script>
<script src="admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="admin/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="admin/vendor/datatables/jquery.dataTables.js"></script>
<script src="admin/vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="admin/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="admin/js/demo/datatables-demo.js"></script>
<script src="admin/js/demo/chart-area-demo.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.dropdown-item').click(function(){
      $('.dropdown-menu.show').css('display','block');
    });
  })
</script>
</body>

</html>
