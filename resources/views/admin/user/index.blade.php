@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">User</li>
                        </ol>
	<a class="btn btn-info" href="{{route('admin.user.showAddUser')}}">Thêm User</a>
	<pre></pre>
	@if(session()->has('message'))
	    <div class="alert alert-success">
	        {{ session()->get('message') }}
	    </div>
      	@else
      		@foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
             @endforeach
      @endif
	<table class="table table-hover">
		<thead>
			<tr>
				<th>STT</th>
				<th>Name</th>
				<th>Email</th>				
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $count = 1 ?>
			@if(Auth::user()->id)
				<tr>
					<td><?php echo $count ?></td>
					<td>{{Auth::user()->name}}</td>
					<td>{{Auth::user()->email}}</td>		
					<td><a class="btn btn-warning" href="{{route('admin.user.edit', Auth::user()->id)}}">Edit</a></td>
				</tr>
			@endif
			@if(Auth::user()->id)
				@foreach($data as $key=>$val)
					<tr>
						<td><?php echo ++$count ?></td>
						<td><?php echo $val->name ?></td>
						<td><?php echo $val->email ?></td>		
						<td><a class="btn btn-warning" href="{{route('admin.user.edit', $val->id)}}">Edit</a></td>
						<td><a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a></td>
					</tr>

					<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				    <div class="modal-dialog" role="document">
				      <div class="modal-content">
				        <div class="modal-header">
				          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
				            <span aria-hidden="true">×</span>
				          </button>
				        </div>
				        <div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
				        <div class="modal-footer">
				          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				          <a class="btn btn-danger" href="{{route('admin.user.destroy', $val->id)}}">Delete</a>
				        </div>
				      </div>
				    </div>
				  </div>
				@endforeach
			
		@endif
		</tbody>
	</table>
	
@endsection