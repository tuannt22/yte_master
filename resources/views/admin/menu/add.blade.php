@extends('admin.layout')

@section('content')
<a class="btn btn-info" href="{{route('admin.menu.dashboard')}}">Quay lại</a>
<pre></pre>
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
<pre></pre>
<form action="{{route('admin.menu.addMenu')}}" method="POST" role="form" enctype="multipart/form-data">
  <legend>Thêm Menu</legend>
  @csrf
  <div class="form-group">
      <label for="namePd">Ngôn ngữ:</label>
      <select name="lang_code">
          @foreach ($lang_code as $key => $value)
              <option value="<?php echo $key; ?>" >{{$value}}</option>
          @endforeach 
      </select>
  </div>  
  <div class="form-group">
    <label for="nameMenu">Tên  menu </label>
    <input name="name" type="text" class="form-control" id="name" placeholder="Nhập tên">
  </div>
  <div class="form-group">
    <label for="linkMenu">Links</label>
    <input name="links" type="text" class="form-control" id="links" placeholder="links">
  </div> 

  <div class="form-group">
    <label for="image">Image</label>
    <input type="file" name="image" id="image" class="form-control" >
  </div>
  <button type="submit" class="btn btn-primary">Add</button>
</form>
@endsection