@extends('admin.layout')

@section('content')
<style type="text/css">
	table tr td{
		overflow: hidden;
	}
</style>
	<ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Menu</li>
                        </ol>
	
	<pre></pre>
	@if(session()->has('message'))
	    <div class="alert alert-success">
	        {{ session()->get('message') }}
	    </div>
      	@else
      		@foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
             @endforeach
      @endif
	<table class="table table-hover">
		<thead>
			<tr>
				<th>STT</th>
				<th>Name</th>
				<th>Links</th>
				<th>Image Menu</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
		
			@foreach($data as $val)
				<tr>
					<td>{{$loop->index+1}}</td>
					<td>{{(strlen($val->name) > 20) ? mb_substr($val->name, 0, 20) : $val->name}}</td>
					<td>{{$val->links}}</td>

					<td><img src="uploads/menus/{{$val->image}}" width="300px" height="100px"></td>


					<td><a class="btn btn-warning" href="{{route('admin.menu.edit', $val->id)}}">Edit</a></td>
				</tr>
				
			@endforeach
		</tbody>
	</table>
@endsection