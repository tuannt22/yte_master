@extends('admin.layout')

@section('content')
    <a class="btn btn-info" href="{{route('admin.menu.dashboard')}}">Quay lại</a>
  <pre></pre>
   @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
      @endif
      @if(session()->has('message'))
      <div class="alert alert-success">
          {{ session()->get('message') }}
      </div>
        @else
          @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
             @endforeach
      @endif
            <pre></pre>
  <form action="{{route('admin.menu.update', $data->id)}}" method="POST" role="form" enctype="multipart/form-data">
    <legend>Edit Menu</legend>  
    @csrf
     <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
            @foreach ($lang_code as $key => $value)
                <option value="<?php echo $key; ?>" <?php if($key == $data->lang_code) {echo 'selected';} ?> >{{$value}}</option>
            @endforeach 
        </select>
    </div>    
    <div class="form-group">
      <label for="nameMenu">Name </label>
      <input name="name" type="text" class="form-control" id="name" placeholder="Nhập tên" value="{{$data->name}}">
    </div>
    <div class="form-group">
      
      <input name="links" type="hidden" class="form-control" id="links" placeholder="links" value="{{$data->links}}">
    </div> 
    <div class="form-group">
    <label for="image">Image (1920 x 580)</label>
    <input type="file" name="image" id="image" class="form-control" >
    <img src="uploads/menus/{{$data->image}}" alt="" width="640px" height="193px">
  </div>
  
    <button type="submit" class="btn btn-primary">Update</button>
    <br>
    <br>
  </form>
@endsection