@extends('admin.layout')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('admin.dashboard')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('admin.topics.index')}}">Chủ đề</a>
        </li>
        <li class="breadcrumb-item active">Tạo mới</li>
    </ol>
    <pre></pre>
    <div class="row">
        @if (Session::has('fail'))
            <div class="alert alert-danger">{{ Session::get('fail') }}</div>
        @endif
    </div>
    <form action="{{route('admin.topics.store')}}" method="POST" role="form"  enctype="multipart/form-data">
        <legend>Thêm chủ đề</legend>
        <br>
        @csrf
        {{--        <div class="form-group">--}}
        {{--            <label for="namePd">Ngôn ngữ:</label>--}}
        {{--            <select name="lang_code">--}}
        {{--                @foreach ($lang_code as $key => $value)--}}
        {{--                    <option value="{{ $key  }}" @if($key == $blog->lang_code) selected @endif >{{$value}}</option>--}}
        {{--                @endforeach--}}
        {{--            </select>--}}
        {{--        </div>--}}
        <div class="form-group">
            <label for="">Chủ đề</label>
            <input type="text" class="form-control" id="" placeholder="Chủ đề"  name="name" value="{{ old('name') }}">
            @if($errors->has('name'))
                <div class="alert alert-danger">{{ $errors->first('name') }}</div>
            @endif
        </div>


        <button type="submit" class="btn btn-primary">Thêm</button>
        <a href="#" data-toggle="modal" data-target="#back"class="btn btn-danger">Hủy bỏ</a>
        <div class="modal fade" id="back" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">Bạn chắc chắn muốn thoát không lưu chứ? </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger" href="{{route('admin.topics.index')}}">OK</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br><br>

@endsection
