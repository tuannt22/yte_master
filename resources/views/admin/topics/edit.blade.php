@extends('admin.layout')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('admin.dashboard')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('admin.topics.index')}}">Chủ đề</a>
        </li>
        <li class="breadcrumb-item active">Sửa</li>
    </ol>
    <a href="{{route('admin.topics.create')}}" class="btn btn-info"><i class="fa fa-plus"></i> Tạo Chủ Đề</a>
    <pre></pre>
    <div class="row">
        @if (Session::has('fail'))
            <div class="alert alert-danger">{{ Session::get('fail') }}</div>
        @endif
    </div>
    <form action="{{route('admin.topics.update', $topic->id)}}" method="POST" role="form"  enctype="multipart/form-data">
        @method('PUT')
        <legend>Sửa chủ đề</legend>
        <br>
        @csrf
        {{--        <div class="form-group">--}}
        {{--            <label for="namePd">Ngôn ngữ:</label>--}}
        {{--            <select name="lang_code">--}}
        {{--                @foreach ($lang_code as $key => $value)--}}
        {{--                    <option value="{{ $key  }}" @if($key == $blog->lang_code) selected @endif >{{$value}}</option>--}}
        {{--                @endforeach--}}
        {{--            </select>--}}
        {{--        </div>--}}
        <div class="form-group">
            <label for="">Chủ đề</label>
            <input type="text" class="form-control" id="" placeholder="Chủ đề"  name="name" value="{{ old('name') ?? $topic->name }}">
            @if($errors->has('name'))
                <div class="alert alert-danger">{{ $errors->first('name') }}</div>
            @endif
        </div>


        <button type="submit" class="btn btn-primary">Sửa</button>
        <a href="#" data-toggle="modal" data-target="#back{{$topic->id}}"class="btn btn-danger">Hủy bỏ</a>
        <div class="modal fade" id="back{{$topic->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">Bạn chắc chắn muốn thoát không lưu chứ? </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger" href="{{route('admin.topics.index')}}">OK</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br><br>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imagePd").change(function(){
            readURL(this);
        });
    </script>
@endsection
