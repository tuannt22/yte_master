@extends('admin.layout')

@section('content')
<a class="btn btn-info" href="{{route('admin.customercomment.dashboard')}}">Quay lại</a>
<pre></pre>

@if(session()->has('message'))
<div class="alert alert-success">
	{{ session()->get('message') }}
</div>
@endif
<pre></pre>
<form action="{{route('admin.customercomment.add')}}" method="POST" role="form" enctype="multipart/form-data" style="margin-bottom: 50px">
	<legend>Thêm cảm nhận của khách hàng</legend>
	@csrf
	<div class="form-group">
		<label for="namePd">Ngôn ngữ:</label>
		<select name="lang_code">
			@foreach ($lang_code as $key => $value)
			  	<option value="<?php echo $key; ?>" >{{$value}}</option>
			@endforeach	
		</select>
	</div>	
	<div class="form-group">
		<label for="namePd">Tên khách hàng: 
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('name') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		<input name="name" type="text" class="form-control" id="namePd" placeholder="Nhập tên khách hàng">
	</div>
	<div class="form-group">
		<label for="namePd">Chức vụ:
		</label>
		<input name="position" type="text" class="form-control" id="namePd" placeholder="Nhập chức vụ của khách hàng">
	</div>
	<div class="form-group">
		<label for="contentPd">Nội dung: 
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('content') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		<textarea name="content" id="contentPd" name="content" class="form-control"></textarea>
		<script src="ckeditor/ckeditor.js"></script>
		<script>
			config = {};
			config.entities_latin = false;
			config.language = 'vi';
			config.uiColor = '#AADC6E';
			config.toolbarGroups = [
			{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
			{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
			{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
			{ name: 'forms', groups: [ 'forms' ] },
			'/',
			{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
			{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
			{ name: 'links', groups: [ 'links' ] },
			{ name: 'insert', groups: [ 'insert' ] },
			'/',
			{ name: 'styles', groups: [ 'styles' ] },
			{ name: 'colors', groups: [ 'colors' ] },
			{ name: 'tools', groups: [ 'tools' ] },
			{ name: 'others', groups: [ 'others' ] },
			{ name: 'about', groups: [ 'about' ] }
			];

			config.removeButtons = 'Flash,Table,Smiley,Iframe,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,About,Link,Unlink,Anchor,CreateDiv,Blockquote,Source,Save,NewPage,Preview,Print,Templates,ShowBlocks';
			CKEDITOR.replace( 'contentPd', config,
			{
				filebrowserBrowseUrl: '../../ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl: '../../ckfinder/ckfinder.html?type=Images',
				filebrowserUploadUrl:
				'../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/archive/',
				filebrowserImageUploadUrl:
				'../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/cars/'
			});
		</script>
	</div>
	
	<button type="submit" class="btn btn-primary">Thêm</button>
</form>
@endsection