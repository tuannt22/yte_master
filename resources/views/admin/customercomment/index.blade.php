@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="{{route('admin.dashboard')}}">Dashboard</a>
  </li>
  <li class="breadcrumb-item active">Cảm nhận khách hàng</li>
</ol>
<pre></pre>
<a href="{{route('admin.customercomment.showAdd')}}" class="btn btn-info">Thêm cảm nhận</a>
<div class="mrg-80 overflow bottom-8" id="cam-nhan">
	<div class="title-h2-bg">
		<h1>
			<img src="content/img/icon/Gioi thieu/1.4_KH noi ve chung toi.jpg">
		Cảm nhận của khách hàng</h1>
		<br>
	</div>
	@foreach($data as $val)
	<div class="mgr-40 list-cus">
		<div class="item-cus">
			<div class="content-cus">
				<h4>{{$val->name}} - {{$val->position}} </h4>
			</div>
			<div class="content-cus">
				
				<div>
					{!!$val->content!!}
				</div>
				
			</div>

		</div>
	</div>
	<a href="{{route('admin.customercomment.edit', $val->id)}}" class="btn btn-warning">Sửa</a>
	<a href="{{route('admin.customercomment.destroy', $val->id)}}" class="btn btn-danger">Xóa</a>
	<br><br><br>
	@endforeach	
</div>

@endsection
