@extends('admin.layout')

@section('content')
<a href="#" data-toggle="modal" data-target="#back"class="btn btn-primary">Quay lại</a>
<pre></pre>
<div class="row">
	@if (Session::has('fail'))
	<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif
</div>
<form action="{{route('admin.library.add')}}" method="POST" role="form"  enctype="multipart/form-data">
	<legend>Thêm tài liệu</legend>
	@csrf
	<br>
    <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
            @foreach ($lang_code as $key => $value)
                <option value="<?php echo $key; ?>" >{{$value}}</option>
            @endforeach 
        </select>
    </div>     
	<div class="form-group">
		<label for="">Tài liệu:  
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('name') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		<input type="text" class="form-control" id="" placeholder="Nhập tên tài liệu"  name="name">
	</div>
	<div class="form-group">
		<label for="file">File:
		@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('document') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif</label>
		<input type="file" name="document" id="filedoc" class="form-control" >
		<div id="blah"></div>
	</div>

        <div class="form-group">
        <label for="">Người đăng:  
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('sharers') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </label>
        <input type="text" class="form-control" id="" placeholder="Họ tên người đăng"  name="sharers">
    </div>

	<button type="submit" class="btn btn-primary">Thêm</button>
	<a href="#" data-toggle="modal" data-target="#back"class="btn btn-danger">Hủy bỏ</a>
	 <div class="modal fade" id="back" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">Bạn chắc chắn muốn thoát không lưu chứ? </div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-danger" href="{{route('admin.about.dashboard')}}">OK</a>
                        </div>
                      </div>
                    </div>
                  </div>
</form>
<br><br>
<script>
	function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
        	var img = $('<img>');
            img.attr('src', e.target.result);
            $('#blah').append(img);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});

</script>

@endsection