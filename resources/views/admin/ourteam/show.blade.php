@extends('admin.layout')

@section('content')
	<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{route('admin.dashboard')}}">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a href="{{route('admin.ourteam.dashboard')}}">Đội ngũ</a></li>
	<li class="breadcrumb-item active">Bài viết</li>
</ol>
	<a href="{{route('admin.ourteam.dashboard')}}" class="btn btn-info">Quay lại</a>
	<pre></pre>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	<div class="form-group">
		<div>
			<img src="uploads/ourteams/{{$show->image}}" width="300px" height="300px">
			<br><br>
			<h2><?php echo $show->name ?>-<?php echo $show->position ?></h2>
			
		</div>
		<div class=" mgr-20 mgr-40" >
			<p><?php echo $show->content ?></p>
		</div>										
	</div>
	<a href="{{route('admin.ourteam.edit', $show->id)}}" class="btn btn-warning">Edit</a>

	<a class="btn btn-danger" href="#" data-toggle="modal" data-target="#delete">Delete</a>
	<pre></pre>

<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Bạn chắc chắn muốn xóa không?</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a href="{{route('admin.ourteam.destroy', $show->id)}}" class="btn btn-danger">Delete</a>
			</div>
		</div>
	</div>
</div>
</div>
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Bạn chắc chắn muốn xóa không?</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a href="{{route('admin.ourteam.destroy', $show->id)}}" class="btn btn-danger">Delete</a>
			</div>
		</div>
	</div>
</div>
@endsection()