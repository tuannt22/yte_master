@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{route('admin.dashboard')}}">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a href="{{route('admin.ourteam.dashboard')}}">Đội ngũ</a></li>
	<li class="breadcrumb-item active">Thêm bài viết</li>
</ol>
<pre></pre>
<div class="row">
	<div class="col-lg-12">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	@if (Session::has('fail'))
   		<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif
</div>
<form action="{{route('admin.ourteam.add')}}" method="POST" role="form"  enctype="multipart/form-data">
	<legend>Thêm đội ngũ </legend>
	<br>
	@csrf
    <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
            @foreach ($lang_code as $key => $value)
                <option value="<?php echo $key; ?>" >{{$value}}</option>
            @endforeach 
        </select>
    </div> 	
	<div class="form-group">
		<label for="">Tên nhân viên: </label>
		<input type="text" class="form-control" id="" placeholder="Tên nhân viên"  name="name">
	</div>
	<div class="form-group">
		<label for="">Chức vụ: </label>
		<input type="text" class="form-control" id="" placeholder="Tên chức vụ"  name="position">
	</div>
	<div class="form-group">
		<label for="imagePd">Ảnh nhân viên( 280 x 250 )</label>
		<input type="file" name="image" id="imagePd" class="form-control" >
		<div id="blah"></div>
	</div>
	<script>
    function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var img = $('<img>');
            img.attr('src', e.target.result);
            $('#blah').append(img);

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>
	<div class="form-group">
		<label for="">Content</label>
		 <textarea id="contentPd" name="content" class="form-control"></textarea>
		 <script src="ckeditor/ckeditor.js"></script>
            <script>
      			config = {};
                config.entities_latin = false;
                config.language = 'vi';
                config.uiColor = '#AADC6E';

      			  CKEDITOR.replace( 'contentPd',
				 {
				     filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				     filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
				     filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				     filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				      filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				 		filebrowserBrowseUrl: '/browser/browse.php?type=Images',
    filebrowserUploadUrl: '/uploader/upload.php?type=Files'
				 });

            </script>
	</div>
	

		

		<button type="submit" class="btn btn-primary">Thêm</button>
		 <a href="#" data-toggle="modal" data-target="#back"class="btn btn-danger">Hủy bỏ</a>
     <div class="modal fade" id="back" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">Bạn chắc chắn muốn thoát không lưu chứ? </div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-danger" href="{{route('admin.ourteam.dashboard')}}">OK</a>
                        </div>
                      </div>
                    </div>
                  </div>
	</form>
	<br><br>

	@endsection