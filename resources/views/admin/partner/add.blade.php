@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="{{route('admin.dashboard')}}">Dashboard</a>
  </li>
  <li class="breadcrumb-item active"><a href="{{route('admin.ourteam.dashboard')}}">Đội ngũ</a></li>
  <li class="breadcrumb-item active">Thêm đối Tác</li>
</ol>
<a href="{{route('admin.partner.dashboard')}}" class="btn btn-info">Quay lại</a>
<pre></pre>
<div class="row">
	
	@if (Session::has('fail'))
   		<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif
</div>
<form style="margin-bottom: 50px" action="{{route('admin.partner.add')}}" method="POST" role="form"  enctype="multipart/form-data">
	<legend>Thêm đối tác</legend>
	@csrf
  <div class="form-group">
      <label for="namePd">Ngôn ngữ:</label>
      <select name="lang_code">
          @foreach ($lang_code as $key => $value)
              <option value="<?php echo $key; ?>" >{{$value}}</option>
          @endforeach 
      </select>
  </div>  
	<div class="form-group">
		<label for="">Tên đối tác: 
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('title') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		<input type="text" class="form-control" id="" placeholder="Enter name"  name="title">
	</div>
	<div class="form-group">
		<label for="">Tiêu đề đối tác trang chủ: 
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('hometitle') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		<input type="text" class="form-control" id="" placeholder="Enter name"  name="hometitle">
	</div>
	<div class="form-group">
		<label for="imagePd">Ảnh giới thiệu:(370 x 245)
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('image') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		<input type="file" name="image" id="imagePd" class="form-control" >
		<div id="blah"></div>
	</div>
	<script>
    function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var img = $('<img>');
            img.attr('src', e.target.result);
            $('#blah').append(img);

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>
		@foreach ($data as $menu)
			<input type="hidden" value="{{$menu->id}}" name="menu_id">
		@endforeach
	</div>
	<div class="form-group col-lg-11">
		<label for="">Nội dung: 
			@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('content') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
		</label>
		 <textarea id="contentPd" name="content" class="form-control"></textarea>
		 <script src="ckeditor/ckeditor.js"></script>
            <script>
      			config = {};
                config.entities_latin = false;
                config.language = 'vi';
                config.uiColor = '#AADC6E';

      			  CKEDITOR.replace( 'contentPd',
				 {
				     filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				     filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
				     filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				     filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				      filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				 		filebrowserBrowseUrl: '/browser/browse.php?type=Images',
    filebrowserUploadUrl: '/uploader/upload.php?type=Files'
				 });

            </script>
	</div>
	
		<div class="col-lg-11"><button type="submit" class="btn btn-primary">Thêm</button>
    <a href="#" data-toggle="modal" data-target="#back"class="btn btn-danger">Hủy bỏ</a></div>
	 <div class="modal fade" id="back" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">Bạn chắc chắn muốn thoát không lưu chứ? </div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-danger" href="{{route('admin.partner.dashboard')}}">OK</a>
                        </div>
                      </div>
                    </div>
                  </div>
	</form>
	<br><br>
	@endsection