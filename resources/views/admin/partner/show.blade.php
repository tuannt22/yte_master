@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{route('admin.dashboard')}}">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a href="{{route('admin.ourteam.dashboard')}}">Đội ngũ</a></li>
	<li class="breadcrumb-item active">thông tin đối tác</li>
</ol>
	<a href="{{route('admin.partner.dashboard')}}" class="btn btn-info">Quay lại</a>
	<pre></pre>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	
	<div class="form-group">
		
	<div class="col-md-9 col-sm-8 float">
						<br><br>
						<h4 class="h4">{{$show->title}}</h4>
						<br>
						<img src="uploads/partners/{{$show->image}}">
						<br>
						<div style="margin-bottom: 80px">
							<div  class="mgr-40" id="{{$show->id}}">

								
								<p>
									{!!$show->content!!}
								</p>	
							</div>
						</div>
						
					</div>
	<div style="padding: 20px 0 20px 0px">
		<a href="{{route('admin.partner.edit', $show->id)}}" class="btn btn-warning">Edit</a>
		<a href="#" data-toggle="modal" data-target="#delete{{$show->id}}"class="btn btn-danger">Delete</a>

	</div>	
	<div class="modal fade" id="delete{{$show->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a href="{{route('admin.partner.destroy', $show->id)}}" class="btn btn-danger">Delete</a>
				</div>
			</div>
		</div>
	</div>
	




@endsection()