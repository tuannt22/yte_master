@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{route('admin.dashboard')}}">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a>Đội ngũ</a></li>
</ol>
	<a href="{{route('admin.partner.addPartner')}}" class="btn btn-info">Thêm đối tác</a>
	<pre></pre>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	
	
	
	
	<?php $count = 1?>
	
	<table class="table table-hover">
		<thead>
			<tr>
				<th>STT</th>
				<th>Ảnh giới thiệu</th>
				<th>Nhà đối tác</th>
				<th colspan="2">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $val)
			<tr>
				<td>{{$count++}}</td>
				<td><img src="uploads/partners/{{$val->image}}"></td>
				<td>{{$val->title}}</td>
				<td><a class="btn btn-primary" href="{{route('admin.partner.show', $val->id)}}">Show</a></td>
				
				<td><a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a></td>
			</tr>
			<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a href="{{route('admin.partner.destroy', $val->id)}}" class="btn btn-danger">Delete</a>
				</div>
			</div>
		</div>
	</div>
			@endforeach
		</tbody>
	</table>
	
	
@endsection()