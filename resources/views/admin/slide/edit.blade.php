@extends('admin.layout')

@section('content')
<a class="btn btn-info" href="{{route('admin.slide.dashboard')}}">Quay lại</a>
<pre></pre>
@if(session()->has('message'))
<div class="alert alert-success">
	{{ session()->get('message') }}
</div>
@endif
<pre></pre>
<form action="{{route('admin.slide.update', $data->id)}}" method="POST" role="form" enctype='multipart/form-data'>
	<legend>Thêm Slide</legend>
	@csrf
	<div class="form-group">
		<label for="namePd">Ngôn ngữ:</label>
		<select name="lang_code">
			@foreach ($lang_code as $key => $value)
			  	<option value="<?php echo $key; ?>" <?php if($key == $data->lang_code) {echo 'selected';} ?> >{{$value}}</option>
			@endforeach	
		</select>
	</div>
	<div class="form-group">
		<label for="namePd">Tên slide: 
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('name') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
		 </label>
		<input name="name" type="text" class="form-control" id="namePd" placeholder="Nhập tên slide"
		value="<?php echo"$data->name"?>">
	</div>

	<div class="form-group">
		
		<label for="imagePd">Ảnh slide: <span>(1920x950px)</span>

			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('image') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
		</label>
		<div>
			<input type="file" name="image" id="imagePd"> 
		</div>
		<div>
			<img id="image" src="uploads/slides/{{$data->image}}" width="200px" height="150px">
			
		</div>
	</div>
	<div class="form-group col-lg-4" style="float: right;">
		<label for="imagePd">links: 
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('links') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
		</label>
		<input type="text" name="links" id="links" class="form-control" placeholder="Nhập links" value="<?php echo"$data->links"?>">
	</div>
	<div class="form-group col-lg-8">
		<label for="title">Title: 
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('title') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
		 </label>
		<input name="title" type="text" class="form-control" id="datePd" placeholder="Nhập Title" value="<?php echo"$data->title"?>">
	</div>


	<button type="submit" class="btn btn-primary">Sửa</button>
</form>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>
<pre><pre></pre></pre>
@endsection