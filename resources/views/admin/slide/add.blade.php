@extends('admin.layout')

@section('content')
<style>
	#blah img{
		width: 250px;
		height: 200px;
	}
</style>
		<a class="btn btn-info" href="{{route('admin.slide.dashboard')}}">Quay lại</a>
	<pre></pre>
	 
      @if(session()->has('message'))
	    <div class="alert alert-success">
	        {{ session()->get('message') }}
	    </div>
      	
      @endif
            <pre></pre>
	<form action="{{route('admin.slide.add')}}" method="POST" role="form" enctype='multipart/form-data'>
		<legend>Thêm Slide</legend>
		@csrf
		<div class="form-group">
			<label for="namePd">Ngôn ngữ:</label>
			<select name="lang_code">
				@foreach ($lang_code as $key => $value)
				  	<option value="<?php echo $key; ?>" >{{$value}}</option>
				@endforeach	
			</select>
		</div>		
		<div class="form-group">
			<label for="namePd">Tên slide:
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('name') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</label>
			<input name="name" type="text" class="form-control" id="namePd" placeholder="Nhập tên slide">
		</div>
		<div class="form-group col-lg-12">
		<label for="imagePd">Chọn ảnh slide:
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('image') as $message)
						<li>{{ $message }}</li>
						@endforeach
					</ul>
				</div>
				@endif
		</label>
		<span>(1920x950px)</span>
		<div style="display: flex;">

			<input type="file" name="image" id="imagePd" >
			<div id="blah"></div>
		</div>
	</div>
	<div class="form-group col-lg-4" style="float: right;">
			<label for="imagePd">links: 
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('links') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</label>
			<input type="text" name="links" id="links" class="form-control" placeholder="Nhập links">
		</div>
		<div class="form-group col-lg-8" >
			<label for="title">Tiêu đề: 
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->get('title') as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</label>
			<input name="title" type="text" class="form-control" id="datePd" placeholder="Nhập Title">
		</div>
		

		<button type="submit" class="btn btn-primary">Thêm</button>
	</form>
	<pre></pre>
	<pre></pre>
	<script>
    function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var img = $('<img>');
            img.attr('src', e.target.result);
            $('#blah').append(img);

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>	

@endsection