@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="/admin/dashboard">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Slide</li>
                        </ol>
	<a class="btn btn-info" href="{{route('admin.slide.addSlide')}}">Thêm </a>
	<pre></pre>
	@if(session()->has('message'))
	    <div class="alert alert-success">
	        {{ session()->get('message') }}
	    </div>
      	@else
      		@foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
             @endforeach
      @endif
	<table class="table table-hover">
		<thead>
			<tr>
				<th>STT</th>
				<th>Title</th>
				<th>Name</th>
				
				<th>Image</th>
				<th>Links</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $key=>$val)
				<tr>
					<td>{{$loop->index+1}}</td>
					<td><?php echo $val->title ?></td>
					<td style="width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 25px;
    -webkit-line-clamp: 3;
    height: 75px;
    display: -webkit-box;
    -webkit-box-orient: vertical;"><?php echo $val->name ?></td>
					<td><img src="uploads/slides/{{$val->image}}" width="200px" height="100px"></td>
					<td><?php echo $val->links ?></td>
					
					<td><a class="btn btn-warning" href="{{route('admin.slide.edit', $val->id)}}">Edit</a></td>
					<td><a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a></td>
				</tr>
				<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			    <div class="modal-dialog" role="document">
			      <div class="modal-content">
			        <div class="modal-header">
			          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
			          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
			            <span aria-hidden="true">×</span>
			          </button>
			        </div>
			        <div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
			        <div class="modal-footer">
			          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
			          <a class="btn btn-danger" href="{{route('admin.slide.destroy', $val->id)}}">Delete</a>
			        </div>
			      </div>
			    </div>
			  </div>
			@endforeach
		</tbody>
		{{ $data->links() }}
	</table>
		
@endsection