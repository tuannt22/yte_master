@extends('admin.layout')

@section('content')
    <style>
        .btn-default {
            color: #333;
            background-color: #fff;
            border-color: #ccc;
        }
        .btn-default .active{
            color: #333;
            background-color: #e6e6e6;
            border-color: #adadad;
        }
    </style>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('admin.dashboard')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Blogs</li>
    </ol>
    <a href="{{route('admin.blogs.create')}}" class="btn btn-info"><i class="fa fa-plus"></i> Tạo bài viết</a>
    <pre></pre>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @else
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif

    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th style="width: 15%">Tiêu đề</th>
            <th style="width: 40%;">Tóm tắt</th>
            <th>Chủ đề</th>
            <th>Ảnh đại diện</th>
            <th>Bài hot</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($blogs as $val)
            <tr>
                <td>{{$loop->index + 1}}</td>
                <td>{{$val->title}}</td>
                <td>{{$val->sumary}}</td>
                <td>{{$val->topics->name}}</td>
                <td>
                    @if($val->image_preview)<img width="200px" src="uploads/blogs/{{$val->image_preview}}">@endif
                </td>
                <td>
                    <input type="checkbox" @if ($val->hot_new == 1) checked @endif class="hotnew" data-toggle="toggle" value="{{ $val->id }}">
                </td>
                <td>
                    <a href="{{route('admin.blogs.edit', $val->id)}}" class="btn btn-primary">Sửa</a>
                    <a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Xoá</a>
                </td>

            </tr>
            <div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <form action="{{route('admin.blogs.destroy', $val->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
        </tbody>
    </table>
    <div class="m-auto">
        {{ $blogs->links() }}
    </div>
    <br>
    <br>
    <script type="application/javascript">
        $('.hotnew').change(function () {
            var id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{ route('admin.hotNews') }}',
                data: {id: id},
                success: function (res) {
                    location.reload();
                }
            })
        });
    </script>
@endsection

