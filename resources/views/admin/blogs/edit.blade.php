@extends('admin.layout')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('admin.dashboard')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{route('admin.blogs.index')}}">Blogs</a>
        </li>
        <li class="breadcrumb-item active">Sửa</li>
    </ol>
    <a href="{{route('admin.blogs.create')}}" class="btn btn-info"><i class="fa fa-plus"></i> Tạo bài viết</a>
    <pre></pre>
    <div class="row">
        @if (Session::has('fail'))
            <div class="alert alert-danger">{{ Session::get('fail') }}</div>
        @endif
    </div>
    <form action="{{route('admin.blogs.update', $blog->id)}}" method="POST" role="form"  enctype="multipart/form-data">
        @method('PUT')
        <legend>Sửa bài viết </legend>
        <br>
        @csrf
{{--        <div class="form-group">--}}
{{--            <label for="namePd">Ngôn ngữ:</label>--}}
{{--            <select name="lang_code">--}}
{{--                @foreach ($lang_code as $key => $value)--}}
{{--                    <option value="{{ $key  }}" @if($key == $blog->lang_code) selected @endif >{{$value}}</option>--}}
{{--                @endforeach--}}
{{--            </select>--}}
{{--        </div>--}}
        <div class="form-group">
            <label for="">Tiêu đề </label>
            <input type="text" class="form-control" id="" placeholder="Tiêu đề bài viết"  name="title" value="{{ old('title') ?? $blog->title }}">
            @if($errors->has('title'))
                <div class="alert alert-danger">{{ $errors->first('title') }}</div>
            @endif
        </div>
        <div class="form-group">
            <label for="imagePd">Ảnh preview</label>
            <input type="file" name="image_preview" id="imagePd" class="form-control">
            @if($blog->image_preview)<img id="blah" src="uploads/blogs/{{$blog->image_preview}}" /> @endif
        </div>
        <div class="form-group">
            <label for="">Tóm tắt</label>
            <input type="text" class="form-control" id="" placeholder="Tóm tắt"  name="sumary" value="{{old('sumary') ?? $blog->sumary}}">
            @if($errors->has('sumary'))
                <div class="alert alert-danger">{{ $errors->first('sumary') }}</div>
            @endif
        </div>

        <div class="form-group">
            <label for="">Chủ đề</label>
            <select name="topic" class="form-control">
                <option value="">Chọn chủ đề</option>
                @foreach ($topics as $key => $value)
                    <option value="{{ $value->id  }}" @if(old('topic') ? old('topic') == $value->id : $value->id == $blog->topics->id) selected @endif >{{$value->name}}</option>
                @endforeach
            </select>
            @if($errors->has('topic'))
                <div class="alert alert-danger">{{ $errors->first('topic') }}</div>
            @endif
        </div>

        <div class="form-group">
            <label for="">Tag</label>
            <div class="row px-3">
                @foreach($tags as $tag)
                    <div class="form-check col-2 float-left">
                        <input type="checkbox" class="form-check-input d-block" id="tag{{$tag->id}}" name="tag[]" value="
                            {{$tag->id}}" @if (old('tag') ? in_array($tag->id, old('tag')) : in_array($tag->id, $idTagArr)) checked @endif>
                        <label class="form-check-label" for="tag{{$tag->id}}">{{$tag->name}}</label>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="form-group">
            <label for="">Content</label>
            <textarea id="content" name="content" class="form-control">
            {{ old('content') ?? $blog->content}}
            </textarea>
            @if($errors->has('content'))
                <div class="alert alert-danger">{{ $errors->first('content') }}</div>
            @endif
            <script src="ckeditor/ckeditor.js"></script>
            <script>
                config = {};
                config.entities_latin = false;
                config.language = 'vi';
                config.uiColor = '#AADC6E';

                CKEDITOR.replace( 'content',
                    {
                        filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                        filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                        filebrowserBrowseUrl: '/browser/browse.php?type=Images',
                        filebrowserUploadUrl: '/uploader/upload.php?type=Files'
                    });

            </script>
        </div>


        <button type="submit" class="btn btn-primary">Sửa</button>
        <a href="#" data-toggle="modal" data-target="#back{{$blog->id}}"class="btn btn-danger">Hủy bỏ</a>
        <div class="modal fade" id="back{{$blog->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">Bạn chắc chắn muốn thoát không lưu chứ? </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-danger" href="{{route('admin.blogs.index')}}">OK</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br><br>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imagePd").change(function(){
            readURL(this);
        });
    </script>
@endsection
