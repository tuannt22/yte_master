@extends('admin.layout')

@section('content')
	 <ol class="breadcrumb">
                          <li class="breadcrumb-item">
                            <a href="{{route('admin.dashboard')}}">Dashboard</a>
                          </li>
                          <li class="breadcrumb-item active">Giới thiệu</li>
                        </ol>
	<a href="{{route('admin.about.addAbout')}}" class="btn btn-info">Thêm giới thiệu</a>
	<pre></pre>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	
	<?php $count = 1?>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>STT</th>
				<th>Ảnh giới thiệu</th>
				<th>Tiêu đề</th>
				<th colspan="3">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $val)
			<tr>
				<td>{{$count++}}</td>
				<td><img src="uploads/abouts/{{$val->image}}" width="50px" height="50px"></td>
				<td>{{$val->title}}</td>
				<td><a href="{{route('admin.about.show', $val->id)}}" class="btn btn-primary">Show</a></td>
				
				<td><a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a></td>
			</tr>
			<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				    <div class="modal-dialog" role="document">
				      <div class="modal-content">
				        <div class="modal-header">
				          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
				            <span aria-hidden="true">×</span>
				          </button>
				        </div>
				        <div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
				        <div class="modal-footer">
				          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				          <a class="btn btn-danger" href="{{route('admin.about.destroy', $val->id)}}">Delete</a>
				        </div>
				      </div>
				    </div>
				  </div>
	
			@endforeach
		</tbody>
	</table>

<br>
<br>
@endsection

