@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{route('admin.dashboard')}}">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a href="{{route('admin.about.dashboard')}}">Giới thiệu</a></li>
	<li class="breadcrumb-item active">Bài viết</li>
</ol>
	<a href="{{route('admin.about.dashboard')}}" class="btn btn-info">Quay lại</a>
	<pre></pre>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif
	

	
		<div class="container-fluid">
			<div class="title-h2-bg">
			<h2>
				<img src="uploads/abouts/{{$show->image}}" width="50px" height="50px">
					{{$show->title}}
				</h2>
		</div><br>
		<div>
			{!!$show->content!!}
		</div>
		<br><br>
		<a href="{{route('admin.about.edit', $show->id)}}" class="btn btn-warning">Edit</a>
		<a href="#" data-toggle="modal" data-target="#delete{{$show->id}}"class="btn btn-danger">Delete</a>
		</div>

		<div class="modal fade" id="delete{{$show->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				    <div class="modal-dialog" role="document">
				      <div class="modal-content">
				        <div class="modal-header">
				          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
				            <span aria-hidden="true">×</span>
				          </button>
				        </div>
				        <div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
				        <div class="modal-footer">
				          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				          <a class="btn btn-danger" href="{{route('admin.about.destroy', $show->id)}}">Delete</a>
				        </div>
				      </div>
				    </div>
				  </div>

<br>
<br>
@endsection

