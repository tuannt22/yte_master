@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <a href="{{route('admin.dashboard')}}">Dashboard</a>
  </li>
  <li class="breadcrumb-item active"><a href="{{route('admin.about.dashboard')}}">Giới thiệu</a></li>
  <li class="breadcrumb-item active">Sửa giới thiệu</li>
</ol>
<a href="#" data-toggle="modal" data-target="#back{{$data->id}}"class="btn btn-info">Quay lại</a>
<pre></pre>
<div class="row">
    <div class="col-lg-12">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
    @if (Session::has('fail'))
    <div class="alert alert-danger">{{ Session::get('fail') }}</div>
    @endif
</div>
<form action="{{route('admin.about.update', $data->id)}}" method="POST" role="form"  enctype="multipart/form-data">
    <legend>Thêm gói dịch vụ </legend>
    <br>
    @csrf
    <div class="form-group">
      <label for="namePd">Ngôn ngữ:</label>
      <select name="lang_code">
        @foreach ($lang_code as $key => $value)
            <option value="<?php echo $key; ?>" <?php if($key == $data->lang_code) {echo 'selected';} ?> >{{$value}}</option>
        @endforeach 
      </select>
    </div>      
    <div class="form-group">
        <label for="">Tiêu đề </label>
        <input type="text" class="form-control" id="" placeholder="Tiêu đề lớn"  name="title" value="<?php echo $data->title ?>">
    </div>
    <div class="form-group">
        <label for="imagePd">Icon: (50x50) </label>
        <input type="file" name="image" id="imagePd" class="form-control">
        <img id="blah" src="uploads/abouts/{{$data->image}}" alt="your image" />
    </div>
    <div class="form-group">
        <label for="">links</label>
        <input type="text" class="form-control" id="" placeholder="Nhập đường dẫn"  name="links" value="{{$data->links}}">
    </div>
        
            @foreach ($menu as $menu)
            <input type="hidden" name="menu_id" value="{{ $menu->id }}">                     
            @endforeach
        
    <div class="form-group">
        <label for="">Content</label>
        <textarea id="contentPd" name="content" class="form-control">
            {{$data->content}}
        </textarea>
        <script src="ckeditor/ckeditor.js"></script>
        <script>
            config = {};
            config.entities_latin = false;
            config.language = 'vi';
            config.uiColor = '#AADC6E';

            CKEDITOR.replace( 'contentPd',
            {
               filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
               filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
               filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
               filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
               filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
               filebrowserBrowseUrl: '/browser/browse.php?type=Images',
               filebrowserUploadUrl: '/uploader/upload.php?type=Files'
           });

       </script>
   </div>
   

   <button type="submit" class="btn btn-primary">Sửa</button>
   <a href="#" data-toggle="modal" data-target="#back{{$data->id}}"class="btn btn-danger">Hủy bỏ</a>
   <div class="modal fade" id="back{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body">Bạn chắc chắn muốn thoát không lưu chứ? </div>
                        <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                          <a class="btn btn-danger" href="{{route('admin.about.dashboard')}}">OK</a>
                        </div>
                      </div>
                    </div>
                  </div>
</form>
<br><br>
<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>
@endsection