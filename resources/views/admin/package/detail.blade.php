@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="#">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a href="{{route('admin.package.showPackage')}}">Gói trị liệu</a></li>
	<li class="breadcrumb-item active">Chi tiết</li>
</ol>
	<a class="btn btn-info" href="{{route('admin.package.showPackage')}}">Quay lại</a>
	<pre></pre>
	<legend>Chi tiết gói sản phẩm</legend>
	
		<div class="form-group">
			<label for=""> - Gói sản phẩm: </label>
			<div class="form-group">  <?php echo $data->name ?></div>
		</div>
		<div class="form-group">
			<label for="">Địa điểm: </label>
			<div class="form-group"> <?php echo $data->address ?></div>
		</div>
		<div class="form-group">
			<label for=""> - Thời gian: </label>
			<div class="form-group">  <?php echo $data->date ?></div>	
			</div>
		<div class="form-group">
			<label for=""> - Nội dung: </label>
			<div class="form-group">  <?php echo $data->content ?></div>
		</div>
		<a class="btn btn-warning" href="{{route('admin.package.ShowEditPackage', $data->id)}}">Edit</a>
		<a href="#" data-toggle="modal" data-target="#delete{{$data->id}}"class="btn btn-danger">Delete</a>

	<pre></pre>

	<div class="modal fade" id="delete{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-danger" href="{{route('admin.package.destroy', $data->id)}}">Delete</a>
					</div>
				</div>
			</div>
		</div>
	
@endsection