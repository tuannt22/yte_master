@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="/admin/dashboard">Dashboard</a>
	</li>
	<li class="breadcrumb-item active">Gói trị liệu</li>
</ol>
<a class="btn btn-info" href="{{route('admin.package.showAddPackage')}}">Thêm gói trị liệu</a>
@if($count ===0)
<a class="btn btn-default">Giới thiệu gói chữa bệnh</a>
<pre></pre>
<div class="alert-danger alert">
	<span>Bạn phải chưa có gói trị liệu nào cả</span>
</div>
@else
<a class="btn btn-info" href="{{route('admin.aboutpackage.dashboard')}}">Giới thiệu gói chữa bệnh</a>
@endif

<pre></pre>
@if(session()->has('message'))
<div class="alert alert-success">
	{{ session()->get('message') }}
</div>
@else
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
@endif
@if (\Session::has('fail'))
<div class="alert alert-danger">
	<ul>
		<li>{!! \Session::get('fail') !!}</li>
	</ul>
</div>
@endif
<table class="table table-hover">
	<thead>
		<tr>
			<th>STT</th>
			<th>Gói sản phẩm</th>

			<th>Nội dung</th>
			<th>Thời gian</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $key=>$val)
		
		<tr>
			<td>{{$loop->index+1}}</td>
			<td>{{(strlen($val->name) > 30) ? mb_substr($val->name, 0, 30) : $val->name}}</td>
			
			<td>{!!(strlen($val->content) > 50) ? mb_substr($val->content, 0, 50) : $val->content!!}</td>
			<td><?php echo $val->date ?></td>

			<td><a href="{{route('admin.package.detailPackage', $val->id)}}" class="btn btn-info">View</a></td>
			<td>
				<a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a>
			</td>
		</tr>
		<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-danger" href="{{route('admin.package.destroy', $val->id)}}">Delete</a>
					</div>
				</div>
			</div>
		</div>

		@endforeach
	</tbody>
	{{ $data->links() }}
</table>

@endsection