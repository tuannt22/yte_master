@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="{{route('admin.dashboard')}}">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a href="{{route('admin.services_detail.dashboard')}}">Dich vụ</a></li>
	<li class="breadcrumb-item active"><a href="{{route('admin.services_detail.dashboard')}}">Danh sách vài viết</a></li>
	<li class="breadcrumb-item active">Bài viết</li>
</ol>
<a href="{{route('admin.services_detail.list', $find->id)}}" class="btn btn-info">Quay lại</a>

<div class="row">
	<div class="col-lg-12">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		
	</div>
	@if (Session::has('fail'))
	<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@endif
</div>
<form action="{{route('admin.services_detail.add', $find->id)}}" method="POST" role="form"  enctype="multipart/form-data">
	<legend>Thêm chi tiết dịch vụ: </legend>
	@csrf
    <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
            @foreach ($lang_code as $key => $value)
                <option value="<?php echo $key; ?>" >{{$value}}</option>
            @endforeach 
        </select>
    </div>	
	<div class="form-group">
		<input type="hidden" value="{{$find->id}}" name="services_id">
	</div>

	<div class="form-group">
		<label for="">Tiêu đề con: </label>
		<input type="text" name="title_con" class="form-control" id="title_con" placeholder="Tiêu đề con">
	</div>
	<div class="form-group">
		<label for="">links: </label>
		<input type="text" name="links" class="form-control" id="" placeholder="links">
	</div>
	<div class="form-group">
		<label for="">Content</label>
		<textarea id="content" name="content" class="form-control"></textarea>
		<script src="ckeditor/ckeditor.js"></script>
		<script>
			config = {};
			config.entities_latin = false;
			config.language = 'vi';
			config.uiColor = '#AADC6E';

			CKEDITOR.replace( 'content',
			{
				filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
				filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				filebrowserBrowseUrl: '/browser/browse.php?type=Images',
				filebrowserUploadUrl: '/uploader/upload.php?type=Files'
			});

		</script>
	</div>

	
	

	

	<button style="margin-bottom: 30px;" type="submit" class="btn btn-primary">Thêm</button>
</form>
@endsection