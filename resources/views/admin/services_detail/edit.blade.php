@extends('admin.layout')

@section('content')
<a class="btn btn-info" href="{{route('admin.services_detail.show', $data->id)}}">Quay lại</a>
<pre></pre>
<div class="row">
	<div class="col-lg-12">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
	@if(session()->has('message'))
	<div class="alert alert-success">
		{{ session()->get('message') }}
	</div>
	@endif
	@if (Session::has('fail'))
	<div class="alert alert-danger">{{ Session::get('fail') }}</div>
	@endif
</div>
<form action="{{route('admin.services_detail.update',$data->id)}}" method="POST" role="form"  enctype="multipart/form-data">
	<legend>Sửa chi tiết dịch vụ: </legend>
	@csrf
	<div class="form-group">
	    <label for="namePd">Ngôn ngữ:</label>
	    <select name="lang_code">
	      @foreach ($lang_code as $key => $value)
	          <option value="<?php echo $key; ?>" <?php if($key == $data->lang_code) {echo 'selected';} ?> >{{$value}}</option>
	      @endforeach 
	    </select>
	</div>	
	<div class="form-group">
		<input type="hidden" value="{{$data->services_id}}" name="services_id">
	</div>

	<div class="form-group">
		<label for="">Tiêu đề con: </label>
		<input type="text" name="title_con" class="form-control" id="title_con" placeholder="Tiêu đề con" value="{{$data->title_con}}">
	</div>
	<div class="form-group">
		<label for="">links bài viết: </label>
		<input type="text" name="links" class="form-control" id="" placeholder="links" value="{{$data->links}}">
	</div>
	<div class="form-group">
		<label for="">Content</label>
		<textarea id="content" name="content" class="form-control">{{$data->content}}</textarea>
		<script src="ckeditor/ckeditor.js"></script>
		<script>
			config = {};
			config.entities_latin = false;
			config.language = 'vi';
			config.uiColor = '#AADC6E';

			CKEDITOR.replace( 'content',
			{
				filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
				filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
				filebrowserBrowseUrl: '/browser/browse.php?type=Images',
				filebrowserUploadUrl: '/uploader/upload.php?type=Files'
			});

		</script>
	</div>

	<button style="margin-bottom: 30px" type="submit" class="btn btn-primary">Sửa</button>
</form>
@endsection