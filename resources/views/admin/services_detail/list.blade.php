@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="/admin/dashboard">Dashboard</a>
	</li>
	
	<li class="breadcrumb-item active"><a href="{{route('admin.services_detail.dashboard')}}">Dich vụ</a></li>
	<li class="breadcrumb-item active">Danh sách bài viết</li>
</ol>
<div class="container-fluid">
	<a href="{{route('admin.services_detail.dashboard')}}" class="btn btn-info">Quay lại</a>
	<br><br>
	<a href="{{route('admin.services_detail.addServices_detail', $find->id)}}" class="btn btn-info">Thêm bài viết</a>

	@if(session()->has('message'))

	<div class="alert alert-success" style="margin-top: 10px">
		{{ session()->get('message') }}
	</div>
	@else
	@foreach ($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
	@endif

		<?php $count = 1?>
		<table class="table table-hover" style="margin-top: 10px">
			<thead>
				<tr>
					<th>STT</th>
					<th>Tiêu đề</th>
					<th colspan="3">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($data as $val)
				<tr>
					<td>{{$count++}}</td>
					
					<td>{{$val->title_con}}</td>
					<td><a href="{{route('admin.services_detail.show', $val->id)}}" class="btn btn-primary">Show</a></td>
					<td><a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a></td>
				</tr>


				<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
								<a href="{{route('admin.services_detail.destroy', $val->id)}}" class="btn btn-danger">Delete</a>
							</div>
						</div>
					</div>									
				</div>
		
				@endforeach
			</tbody>
		</table>
		
	</div>

	@endsection()