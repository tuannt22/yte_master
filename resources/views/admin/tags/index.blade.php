@extends('admin.layout')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('admin.dashboard')}}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Tags</li>
    </ol>
    <a href="{{route('admin.tags.create')}}" class="btn btn-info"><i class="fa fa-plus"></i> Tạo Tags</a>
    <pre></pre>
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @else
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    @endif

    <table class="table table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Tags</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tags as $val)
            <tr>
                <td>{{$loop->index + 1}}</td>
                <td>{{$val->name}}</td>
                <td>
                    <a href="{{route('admin.tags.edit', $val->id)}}" class="btn btn-primary">Sửa</a>
                    <a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Xoá</a>
                </td>

            </tr>
            <div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <form action="{{route('admin.tags.destroy', $val->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        @endforeach
        </tbody>
    </table>
    <div class="m-auto">
        {{ $tags->links() }}
    </div>
    <br>
    <br>
@endsection

