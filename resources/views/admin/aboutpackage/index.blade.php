@extends('admin.layout')

@section('content')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="/admin/dashboard">Dashboard</a>
	</li>
	<li class="breadcrumb-item active"><a href="{{route('admin.about.dashboard')}}">Gói trị liệu</a></li>
	<li class="breadcrumb-item active">Giới thiệu gói trị liệu</li>

</ol>
<a class="btn btn-info" href="{{route('admin.package.showPackage')}}">Quay lại bảng gói trị liệu</a>
<a class="btn btn-info" href="{{route('admin.aboutpackage.showAdd')}}">Thêm gói chữa bệnh</a>
<pre></pre>
@if (\Session::has('success'))
<div class="alert alert-success">
    <ul>
        <li>{!! \Session::get('success') !!}</li>
    </ul>
</div>
@endif
<pre></pre>
<table class="table table-hover">
	<thead>
		<tr>
			<th>STT</th>
			<th>Tên gói trị liệu</th>
			<th>Banner</th>
			<th colspan="2">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php $count = 1 ?>
		@foreach($data as $val)
		<tr>
			<td>{{$count++}}</td>
			<td><?php echo $val->name ?></td>
			<td><img height="125" width="185"  src="uploads/packages/{{$val->banner}}" alt=""></td>
			<td><a class="btn btn-warning" href="{{route('admin.aboutpackage.edit', $val->id)}}">Edit</a></td>
			<td><a href="#" data-toggle="modal" data-target="#delete{{$val->id}}"class="btn btn-danger">Delete</a></td>
		</tr>
		<div class="modal fade" id="delete{{$val->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">Bạn chắc chắn muốn xoá chứ? </div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-danger" href="{{route('admin.aboutpackage.destroy', $val->id)}}">Delete</a>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</tbody>
</table>
@endsection