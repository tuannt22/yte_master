@extends('admin.layout')

@section('content')
<style>
    #blah img{
        width: 250px;
        height: 200px;
    }
</style>
@if (\Session::has('success'))
<div class="alert alert-success">
    <ul>
        <li>{!! \Session::get('success') !!}</li>
    </ul>
</div>
@endif

<a href="{{route('admin.aboutpackage.dashboard')}}" class="btn btn-info">Quay lại</a>
<pre></pre>
<form style="margin-bottom: 50px" action="{{route('admin.aboutpackage.add')}}" method="POST" role="form" enctype="multipart/form-data">
	<legend>Thêm giới thiệu gói sản phẩm</legend>
	@csrf
    <div class="form-group">
        <label for="namePd">Ngôn ngữ:</label>
        <select name="lang_code">
            @foreach ($lang_code as $key => $value)
                <option value="<?php echo $key; ?>" >{{$value}}</option>
            @endforeach 
        </select>
    </div>     
	<div class="form-group">
		<label for="namePd">Tên gói sản phẩm: 
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->get('name') as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </label>
        <input name="name" type="text" class="form-control" id="namePd" placeholder="Nhập tên gói sản phẩm">
    </div>
    <div class="form-group">
      <label for="imagePd">Thêm ảnh giới thiệu: <span>(370x250)</span>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->get('image') as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </label>
    <div>
        <input type="file" name="image" id="imagePd">      
    </div>
    <div id="blah"></div>

    <div class="form-group">
        <label for="slcCat">Chọn địa điểm</label>
        <select  class="form-control" id="slcCat" name="packages_id">
            @foreach ($packages as $val)
            <option value="{{ $val->id }}"> {{ $val->address }}</option>  
            @endforeach
        </select>
    </div>
    
</div>

<button type="submit" class="btn btn-primary">Thêm</button>
</form>
<script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var img = $('<img>');
                img.attr('src', e.target.result);
                $('#blah').append(img);

            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imagePd").change(function(){
        readURL(this);
    });
</script>	
@endsection