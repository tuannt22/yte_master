@extends('admin.layout')

@section('content')

	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
	<a class="btn btn-primary" href="{{route('admin.aboutpackage.dashboard')}}">Quay lại</a>
	<br> <br>
	<form action="{{route('admin.aboutpackage.update', $data->id)}}" method="POST" role="form" enctype="multipart/form-data">
		<legend>Thêm giới thiệu gói sản phẩm</legend>
		@csrf
	   <div class="form-group">
	        <label for="namePd">Ngôn ngữ:</label>
	        <select name="lang_code">
	            @foreach ($lang_code as $key => $value)
	                <option value="<?php echo $key; ?>" <?php if($key == $data->lang_code) {echo 'selected';} ?> >{{$value}}</option>
	            @endforeach 
	        </select>
	    </div> 		
		<div class="form-group">
			<label for="namePd">Tên gói sản phẩm: </label>
			<input name="name" type="text" class="form-control" id="namePd" placeholder="Nhập tên gói sản phẩm" value="{{$data->name}}">
		</div>

		<div class="form-group">
        <label for="slcCat">Chọn địa điểm</label>
        <select  class="form-control" id="slcCat" name="packages_id">
            @foreach ($packages as $val)
            <option {{$val->id === $data->packagesid ? 'selected' : ''}} value="{{ $val->id }}"> {{ $val->address }}</option>  
            @endforeach
        </select>
    </div>
		
		<div class="form-group col-lg-12" >
			<label for="imagePd">Thêm ảnh giới thiệu: <span>(370x250)</span>
			</label>
			<div>
				<input type="file" name="image" id="imagePd" >
				
			</div>
			<div>
				<img width="250" height="200" id="blah" src="uploads/packages/{{$data->banner}}" alt="your image" />	
			</div>
		</div>	
		<br>
		
		<button type="submit" class="btn btn-primary">Sửa</button>
	</form>
	<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imagePd").change(function(){
    readURL(this);
});
</script>
<br><br>
@endsection