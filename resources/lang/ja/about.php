<?php 
	return
	[
		'Thông điệp từ CEO' => 'CEOからのメッセージ',
		'Tại sao lựa chọn IHC ?'=>'IHCを選ぶ理由',
		'Tầm nhìn - sứ mệnh - giá trị cốt lõi'=>'ビジョン - ミッション - コアバリュー',
		'Đội ngũ'=>'チーム',
		'Khách hàng nói về chúng tôi'=>'顧客は私達について話します',
	];
