<?php

// sentence.php

return [
	'news' => 'ニュース',
	'faq' => '質問と回答',
	'contact' => 'お問い合わせください
	',
	'home' => 'ホームページ',
	'Giới thiệu'=> 'はじめに',
	'Dịch vụ' => 'サービス',
	'Gói trị liệu' => 'セラピーパッケージ',
	'Đối tác' => 'パートナー',
	'Thủ tục' => '手続き',
	'Thư viện' => '図書館',
    'BLOG' => 'BLOG',
];
