<?php

// sentence.php

return [
	'news' => 'Tin tức',
  'faq' => 'Câu hỏi thường gặp',
  'contact' => 'Liên hệ',
  'home' => 'Trang chủ',
  'Giới thiệu'=> 'Giới thiệu',
  'Dịch vụ' => 'Dịch vụ',
  'Gói trị liệu' => 'Gói trị liệu',
  'Đối tác' => 'Đối tác',
  'Thủ tục' => 'Thủ tục',
  'Thư viện' => 'Thư viện',
  'Tin tức' => 'Tin tức',
  'Hỏi đáp' => 'Hỏi đáp',
  'Liên hệ' => 'Liên hệ',
  'PARTNERS' => 'Đối tác của ihc',
  'PROCEDURES' => 'quy trình làm việc',
  'Copyright' => '© Bản quyền thuộc Công ty TNHH Infinity Healthcare Việt Nam',
  'ReadMore' => 'Xem thêm',
  'EVERYDAY HEALTHY LIVING' => 'cùng bạn Sống khỏe mỗi ngày',
  'EVERYDAY HEALTHY LIVING CONTENT' => '
    <p>Đã bao giờ bạn từng nghe đến liệu pháp trị liệu bằng tế bào gốc, và bạn muốn hiểu rõ về cơ chế trị liệu, hiệu quả trị liệu của liệu pháp này?</p>
    <p>Đã bao giờ bạn từng nghe đến liệu pháp điều trị ung thư không xạ trị, không hóa trị mà vẫn đảm bảo chất lượng cuộc sống cho bệnh nhân? </p>
    <p>Đã bao giờ bạn từng lo lắng về biện pháp phòng ngừa, tầm soát các bệnh hiểm nghèo như ung thư, tai biến mạch máu não, đột quỵ?</p>
    <p>Đã bao giờ bạn mong muốn được khám chữa bệnh bởi các bác sỹ hàng đầu thế giới và để họ giải quyết các vấn đề sức khỏe của bạn?</p>
    <p>Tại Infinity Healthcare Việt Nam (IHC), chúng tôi sẽ cung cấp giải pháp đối với các vấn đề nêu trên. Sứ mệnh của chúng tôi là giúp hàng triệu người Việt Nam có cơ hội tiếp cận với y học hiện đại và y học thay thế, vì một cuộc sống khỏe mạnh hơn, hạnh phúc hơn!</p>',
  'EXPERIENCE THE TOP CLASS SERVICES' => 'TRẢI NGIỆM DỊCH VỤ HÀNG ĐẦU',
  'MEDICAL SERVICES' => 'DỊCH VỤ Y KHOA',
  'WHY CHOOSE IHC' => 'tại sao bạn nên chọn ihc ?',
  'WHY CHOOSE IHC LG' => 'tại sao bạn nên <br/> chọn ihc',
  'COMPREHENSIVE ADVICES' => 'TƯ VẤN CHUYÊN SÂU',
  'COMPREHENSIVE ADVICES CONTENT' => 'Khách hàng được cung cấp đầy đủ thông tin, phân tích các phương án điều trị, và lựa chọn phương án điều trị tốt nhất, phù hợp với đặc điểm bệnh lý và điều kiện <br> tài chính.',
  'TOP QUALITY' => 'CHẤT LƯỢNG HÀNG ĐẦU',
  'TOP QUALITY CONTENT' => 'IHC cam kết kết nối cho khách hàng gặp được các bác sỹ hàng đầu, các chuyên khoa hàng đầu chuyên về từng vấn đề sức khỏe cụ thể của khách hàng, nhằm đem lại kết quả điều trị tốt nhất.',
  'PROFESSIONAL SERVICES' => 'CHẤT LƯỢNG HÀNG ĐẦU',
  'PROFESSIONAL SERVICES CONTENT' => 'IHC cam kết cung cấp dịch vụ chu đáo và trách nhiệm nhất đến từng khách hàng từ tư vấn trị liệu, sắp xếp lịch trình cho đến khi hoàn tất cung cấp dịch vụ trị liệu tại nước ngoài.',
  'PRE-TREATMENT CONSULTANCY' => 'TƯ VẤN ĐIỀU TRỊ',
  'PRE-TREATMENT CONSULTANCY CONTENT' => 'Tham khảo hồ sơ y khoa <br>Xét nghiệm bổ sung  <br>Chẩn đoán sơ bộ  <br>Tư vấn điều trị',
  'PRE-DEPARTURE PREPARATION' => 'Chuẩn bị trước khởi hành',
  'PRE-DEPARTURE PREPARATION CONTENT' => 'Đặt lịch hẹn<br>Ký hợp đồng dịch vụ với IHC<br>Làm thủ tục xin visa<br>Đặt vé máy bay và khách sạn',
  'OVERSEAS TREATMENT' => 'Chuẩn bị trước khởi hành',
  'OVERSEAS TREATMENT CONTENT' =>
    'Xét nghiệm chuyên sâu lần 1 <br>
    Điều trị <br>
    Xét nghiệm chuyên sâu lần 2 <br>
    Đánh giá kết quả điều trị',
  'POST-TREATMENT MONITORING' => 'Theo dõi sau điều trị',
  'POST-TREATMENT MONITORING CONTENT' =>
    'Đánh giá kết quả sau 3 tháng  <br>
    Đánh giá kết quả sau 6 tháng <br>
    Kế hoạch điều trị bổ sung',
  'TESTIMONIALS' => 'CẢM NHẬN KHÁCH HÀNG',
  'TESTIMONIALS LG' => 'CẢM NHẬN CỦA <br> KHÁCH HÀNG',
  'ABOUT US' => 'VỀ CHÚNG TÔI',
    'BLOG' => 'BLOG',
];
