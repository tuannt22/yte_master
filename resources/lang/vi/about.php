<?php 
return[
	'Thông điệp từ CEO' => 'Thông điệp từ CEO',
	'Tại sao lựa chọn IHC ?'=> 'Tại sao lựa chọn IHC ?',
	'Tầm nhìn - sứ mệnh - giá trị cốt lõi'=> 'Tầm nhìn - sứ mệnh - giá trị cốt lõi',
	'Đội ngũ'=>'Đội ngũ',
	'Khách hàng nói về chúng tôi'=>'Khách hàng nói về chúng tôi',
	'COMPREHENSIVE ADVICES CONTENT' => 'IHC cam kết cung cấp dịch vụ tư vấn y khoa chuyên sâu. Đến với chúng tôi, khách hàng được cung cấp đầy đủ thông tin, phân tích các phương án điều trị, và lựa chọn phương án điều trị tốt nhất, phù hợp với đặc điểm bệnh lý và điều kiện tài chính của từng khách hàng. ',
	'TOP-CLASS QUALITY'=>'CHẤT LƯỢNG HÀNG ĐẦU',
	'TOP-CLASS QUALITY CONTENT'=>'IHC cam kết cung cấp dịch vụ tư vấn y khoa chuyên sâu. Đến với chúng tôi, khách hàng được cung cấp đầy đủ thông tin, phân tích các phương án điều trị, và lựa chọn phương án điều trị tốt nhất, phù hợp với đặc điểm bệnh lý và điều kiện tài chính của từng khách hàng. ',
	'PROFESSIONALISM'=>'Dịch vụ chuyên nghiệp',
	'PROFESSIONALISM CONTENT'=>'IHC cam kết cung cấp dịch vụ chu đáo và trách nhiệm nhất đến từng khách hàng từ tư vấn trị liệu, sắp xếp lịch trình cho đến khi hoàn tất cung cấp dịch vụ trị liệu tại nước ngoài; thực hiện tư vấn miễn phí các vấn đề sức khỏe của khách hàng trong suốt quá trình trước và sau khi điều trị. ',
	'ABSOLUTE PRIVACY'=>'BẢO MẬT TUYỆT ĐỐI',
	'ABSOLUTE PRIVACY CONTENT'=>'IHC cam kết tuân thủ quy tắc bảo mật nghiêm ngặt các thông tin cá nhân và sức khỏe của khách hàng. ',
	'OUR TEAM'=>'ĐỘI NGŨ',
	'TESTIMONIALS'=>'CẢM NHẬN CỦA KHÁCH HÀNG',
	'SERVICES'=>'DỊCH VỤ Y KHOA',
	'FAQ'=>'CÂU HỎI THƯỜNG GẶP',
	'Therapy package'=>'GÓI TRỊ LIỆU',
	'Therapy package note'=>'(*) Bao gồm chi phí trị liệu, ăn ở, phiên dịch, chưa bao gồm vé máy bay',
	'package'=>'Gói sản phẩm (mã sản phẩm)',
	'Location'=>'Địa điểm',
	'Contents'=>'Nội dung',
	'Duration'=>'Thời gian',	
	'Partners'=>'Đối tác',	
	'Procedure'=>'THỦ TỤC',
	'SERVICE PROCEDURE'=>'QUY TRÌNH DỊCH VỤ',	
	'Procedure_NOTICE'=>'MỘT SỐ LƯU Ý',	
	'Procedure_NOTICE_CONTENT'=>'								
		<p>
			(1) Ký hợp đồng và đặt cọc
		</p>
		<p>
			Để đặt lịch hẹn điều trị và hoàn tất các thủ tục visa, khách hàng sẽ được yêu cầu đặt cọc một khoản tiền thường là 50% chi phí điều trị.
		</p>
		<p>
			Khách hàng có thể hoàn thành chuyển khoản thanh toán trực tiếp bằng tiền mặt hoặc thẻ tín dụng hoặc bằng cách chuyển khoản vào tài khoản của IHC
		</p>
		<p>
			(2) Hoàn phí liên quan đến kết quả visa
		</p>
		<p>
			Sau khi hoàn tất thủ tục đặt cọc, IHC sẽ hỗ trợ khách hàng làm thủ tục xin visa khám chữa bệnh tại nước bạn chọn đến điều trị.
		</p>
		<p>
			Trong trường hợp khách hàng được cấp visa: IHC thanh toán toàn bộ chi phí điều trị cho cơ sở y tế; và việc hoàn tiền chi phí điều trị sẽ theo chính sách của IHC quy định ở mục THƯ VIỆN > Điều khoản pháp lý.
		</p>
		<p>
			Trong trường hợp khách hàng không được cấp visa: IHC hoàn trả toàn bộ tiền khách hàng đã đặt cọc. 
		</p>',
	'ANTI-AGING'=>'CHỐNG LÃO HÓA',		
	'1WHAT ANTI-AGING'=>'1.	Y HỌC CHỐNG LÃO HÓA LÀ GÌ?',		
	'1WHAT ANTI-AGING CONTENT'=>'
		<p>Y học chống lão hóa là một nhánh phát triển của y học khoa học và y học ứng dụng. Nó điều trị các nguyên nhân cơ bản của lão hóa và nhằm mục đích giảm bớt bất kỳ bệnh nào liên quan đến tuổi tác. Mục tiêu của nó là kéo dài tuổi thọ của con người một cách khỏe mạnh.</p>
		<p>Các quy tắc y học thông thường và y học thay thế được sử dụng theo cách tiếp cận tích hợp để đạt được kết quả tốt nhất có thể cho bệnh nhân. Đó là một cách tiếp cận toàn diện, tác động toàn bộ thể chất đến tinh thần bệnh nhân chứ không phải là điều trị một chứng bệnh độc lập.</p>',		
	'2WHAT ANTI-AGING'=>'2.	Điều gì gây ra lão hóa?',		
	'2WHAT ANTI-AGING CONTENT'=>'
		<p>Lão hóa là quá trình thất bại của trao đổi chất. Có một khái niệm về sự tạm dừng được đưa ra bởi Tiến sĩ Eric Braverman chỉ ra rằng mọi cơ quan đều có tuổi ở một tỷ lệ khác nhau. Ở một mức độ nhất định, nó dựa trên hormone.</p>
		<p>Có 3 quá trình sinh hóa chính liên quan đến lão hóa: oxy hóa, glycate hóa và methyl hóa. Ngoài ra, các quá trình liên quan khác là Viêm mãn tính và Suy giảm nội tiết tố.</p>',		
	'3WHAT ANTI-AGING'=>'3.	Ai cần thực hiện liệu trình Chống lão hóa?',		
	'3WHAT ANTI-AGING CONTENT'=>'Tất cả mọi người đều cần thực hiện liệu trình Chống lão hóa đặc biệt nếu bạn đã bước qua tuổi 50. Bởi vì sau tuổi 50, tốc độ lão hóa bắt đầu tăng dần và sức khỏe cơ thể thường thay đổi rất nhiều dẫn đến phát triển một số bệnh như: Tiểu đường, Huyết Áp, Mỡ Máu, Tim mạch, Ung thư…. Một trong những yếu tố chính trong quá trình này là sự suy giảm đáng kể các hormon cơ thể. Từ đó chất lượng cuộc sống sẽ giảm dần đi. ',		
	'4WHAT ANTI-AGING'=>'4.	Tại sao tôi cần thực hiện Chống lão hóa?',		
	'4WHAT ANTI-AGING CONTENT'=>'Nhờ trị liệu chống lão hóa, sự suy giảm kích thích tố và các tế bào của chúng ta có thể được điều chỉnh và ngăn ngừa. Tương tự, liệu pháp thay thế hormone sinh học và các công thức chống lão hóa cụ thể đã được phát triển ngày nay có thể đem đến chất lượng cuộc sống tốt hơn và kéo dài tuổi thọ hơn.',			
	'5WHAT ANTI-AGING'=>'5.	Bao lâu tôi phải thực hiện liệu trình Chống lão hóa một lần?',		
	'5WHAT ANTI-AGING CONTENT'=>'Tùy thuộc vào tình trạng sức khỏe và sự nhiễm độc của cơ thể, bạn có thể thực hiện liệu trình Chống lão hóa 1-2 lần/ năm.',				
	'6WHAT ANTI-AGING'=>'6.	Thành phần tiêm truyền trong liệu trình chống lão hóa theo chương trình trị liệu của IHC là gì, có tác dụng phụ không?',		
	'6WHAT ANTI-AGING CONTENT'=>'Việc tiêm truyền thải độc, chống lão hóa sử dụng các sản phẩm tự nhiên chống oxy hóa, chống glicate hóa và các loại vitamins, khoáng chất, axit amin…. không gây phản ứng phụ.',				
	'7WHAT ANTI-AGING'=>'7.	Liệu trình chống lão hóa có làm tôi trẻ đẹp hơn không?',		
	'7WHAT ANTI-AGING CONTENT'=>'Đây không phải là phương pháp làm đẹp mà là phương pháp giúp làm chậm quá trình lão hóa của cơ thể. Bạn sẽ cảm thấy một sự cải thiện về thể chất và tinh thần. Cơ thể bạn trở nên tươi trẻ hơn và tràn đầy sức sống hơn.',				
	'8WHAT ANTI-AGING'=>'8.	Có chống chỉ định cho trị liệu chống lão hóa không?',		
	'8WHAT ANTI-AGING CONTENT'=>'Nhìn chung không có chống chỉ định đặc biệt cho trị liệu chống lão hóa vì các thành phần tiêm truyền đều có thành phần tự nhiên và không có tác dụng phụ. Tuy nhiên với việc bổ sung hormon sinh học, nó chỉ được thực hiện sau khi đánh giá đầy đủ và theo hướng dẫn cụ thể của bác sĩ chống lão hóa.',
	'DETOXIFICATION'=>'THẢI ĐỘC',			
	'1DETOXIFICATION'=>'1.	Tại sao cần phải thải độc?',			
	'1DETOXIFICATION CONTENT'=>'Trong cuộc sống hiện đại, chúng ta tiếp xúc với không khí ô nhiễm, căng thẳng và cuộc sống thụ động, việc tiêu thụ thực phẩm chế biến, phụ gia và nhiều yếu tố khác dẫn đến sự tích tụ chất độc trong cơ thể. Do đó, chúng ta cần thải độc hai lần mỗi năm để loại bỏ chất độc. Chúng ta nên thải độc để: chậm lão hóa và khỏe mạnh hơn, giảm cân, tăng cường năng lượng, cải thiện chất lượng da, cải thiện chất lượng cuộc sống, tăng cường chức năng hệ miễn dịch, ngăn ngừa bệnh mãn tính và dĩ nhiên để có sự sáng suốt về tinh thần và cảm xúc.',			
	'2DETOXIFICATION'=>'2.	Tôi nên thực hiện chương trình bao nhiêu ngày?',			
	'2DETOXIFICATION CONTENT'=>'Bạn có thể thực hiện nhiều loại chương trình thải độc trong khoảng thời gian mà bạn muốn, tuy nhiên để có kết quả hiệu quả, chúng tôi đề nghị bạn thực hiện chương trình tối thiểu 3-4 đến 10 ngày.',			
	'3DETOXIFICATION'=>'3.	Chương trình thải độc có giúp tôi bỏ các thói quen không lành mạnh (hút thuốc, uống rượu, ăn vặt, …) không?',			
	'3DETOXIFICATION CONTENT'=>'Chương trình thải độc làm giảm đáng kể việc uống rượu và hút thuốc. Chương trình thải độc mà chúng tôi cung cấp với sự trợ giúp của quản lý chương trình và các liệu pháp bổ sung khác rất hiệu quả trong việc giúp bạn từ bỏ các thói quen không lành mạnh.',				
	'4DETOXIFICATION'=>'4.	Có bất kỳ chống chỉ định nào cho việc nhịn ăn và colema không?',			
	'4DETOXIFICATION CONTENT'=>'Nhịn ăn phù hợp cho tất cả mọi người với sự đồng ý của chuyên gia thải độc và sau khi tư vấn bác sĩ tại trung tâm của chúng tôi. Colema cũng rất hiệu quả và an toàn cho quá trình thải độc của bạn.',			
	'5DETOXIFICATION'=>'5.	Tôi bị bệnh và có một loại thuốc phải sử dụng thường xuyên. Tôi có thể thải độc không?',			
	'5DETOXIFICATION CONTENT'=>'Để giúp bạn quyết định, chúng tôi đề nghị bạn hỏi ý kiến bác sĩ của chúng tôi. Bạn có thể gọi điện thoại hoặc gửi email chi tiết về tình trạng sức khỏe của bạn và bác sĩ của chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.',			
	'6DETOXIFICATION'=>'6.	Tôi có được tiếp tục dùng thuốc trong khi thải độc không?',			
	'6DETOXIFICATION CONTENT'=>'Chúng tôi không bao giờ khuyến khích bạn ngừng dùng thuốc trong khi thải độc bởi vì điều này rất nguy hiểm. Quản lý chương trình thải độc nhiều năm kinh nghiệm của chúng tôi sẽ hướng dẫn bạn với các quyết định tốt nhất liên quan đến sức khỏe của bạn và các khuyến nghị toàn diện và khuyến nghị về chế độ ăn uống sẽ được thực hiện.',			
	'7DETOXIFICATION'=>'7.	Tôi có thể thải độc không nếu tôi bị tiểu đường?',			
	'7DETOXIFICATION CONTENT'=>'
		<p>
			Có, bạn có thể. Bệnh tiểu đường loại 2 là hậu quả trực tiếp của chế độ ăn uống kém và thói quen sống không lành mạnh. Chúng ta có thể chữa bệnh bằng cách thay đổi chế độ ăn uống và các thành phần khác của lối sống. Thời hạn (khoảng từ 7-21 ngày) tùy thuộc vào thiện chí của người thực hiện, 7 ngày là đủ để cân bằng hormon và mức đường trong máu, tuy nhiên cần ít nhất 14 ngày để tác động hoàn chỉnh. Nhịn ăn hoàn toàn hoặc nhịn ăn với nước ép là phương thuốc kỳ diệu cho bệnh tiểu đường loại 2.
		</p>
		<p>
			Quản lý chương trình thải độc nhiều năm kinh nghiệm và bác sĩ của chúng tôi sẽ hướng dẫn bạn với các khuyến nghị về chế độ ăn uống tốt nhất. Tại đây, chúng tôi thực hiện 3 chương trình khác nhau cho bệnh tiểu đường loại 2, tiền tiểu đường và tiểu đường loại 1. 
		</p>
	',			
	'8DETOXIFICATION'=>'8.	Thải độc có giúp giải quyết các vấn đề về da của tôi không?',			
	'8DETOXIFICATION CONTENT'=>'Ở khía cạnh sức khỏe toàn diện, chúng tôi khuyên bạn nên điều trị tất cả các bệnh về da bằng cách làm sạch đường tiêu hóa và gan, trong thời gian lưu trú, bạn sẽ gặp quản lý chương trình thải độc của chúng tôi, người có thể đưa ra đề xuất cho bạn để tiếp tục khi bạn rời khỏi chương trình.',			
	'9DETOXIFICATION'=>'9.	Nếu tôi bị huyết áp cao hoặc bị cholesterol cao thì sao?',			
	'9DETOXIFICATION CONTENT'=>'
		<p>
			Chúng tôi có rất nhiều khách hàng bị cholesterol cao và tăng huyết áp. Trong thực tế, nhiều người trong số họ đến với chúng tôi để được giải quyết các vấn đề này. Mỗi buổi sáng, bạn sẽ được đo huyết áp bởi chuyên gia tư vấn của chúng tôi. 
		</p>
		<p>
			Mặc dù nhịn ăn thường xuyên sẽ làm giảm huyết áp cao, nhưng chúng tôi không bao giờ khuyên mọi người nên ngừng dùng thuốc cho tình trạng này.
		</p>
		<p>
			Thải độc cũng là một biện pháp gián tiếp giúp giải quyết các vấn đề liên quan đến cholesterol cao bởi vì toàn bộ quy trình giúp thải độc máu và gan, do đó cải thiện khả năng tiêu hóa và phân hủy chất béo của cơ thể. Chúng tôi có sẵn đội ngũ nhân viên được đào tạo và có kinh nghiệm sẵn sàng trả lời bất cứ câu hỏi hay lo lắng nào của bạn liên quan đến những vấn đề này cũng như những dịch vụ cộng thêm sẽ giúp bạn lên một kế hoạch tập thể dục và chế độ ăn kiêng để mang lại sự cân bằng, sức khỏe và sức sống tốt hơn cho cuộc sống của bạn, nếu bạn cảm thấy một phân tích sâu hơn là cần thiết.
		</p>
	',			
	'10DETOXIFICATION'=>'10. Tôi có thể thực hiện thải độc không nếu tôi đang dùng thuốc chống trầm cảm?',			
	'10DETOXIFICATION CONTENT'=>'Có, tất nhiên. Chúng tôi không bao giờ khuyên bạn ngừng dùng thuốc được kê đơn trong khi thực hiện thải độc, nhất là thuốc chống trầm cảm.',			
	'11DETOXIFICATION'=>'11.	Tôi có thể tham gia chương trình không nếu tôi đang hành kinh?',			
	'11DETOXIFICATION CONTENT'=>'Không có vấn đề gì khi tham gia chương trình thải độc nếu bạn đang hành kinh. Và quá nhiều thay đổi trong chế độ ăn uống đều có thể ảnh hưởng đến chu kỳ kinh nguyệt của bạn, vì vậy đừng lo lắng nếu chu kỳ của bạn không đều hoặc hơi nhiều hơn bình thường.',			
	'12DETOXIFICATION'=>'12. Tôi có thể thực hiện thải độc không nếu tôi quá gầy?',			
	'12DETOXIFICATION CONTENT'=>'Bạn vẫn có thể thải độc nếu bạn gầy miễn là bạn hiện không bị rối loạn ăn uống hoặc đang bị suy dinh dưỡng hoặc bị thiếu hụt chất nhất định. Mục tiêu chính của thải độc là thải độc và loại bỏ chất thải bị nén trong đại tràng. Chúng tôi có rất nhiều khách hàng là người gầy và thông thường chúng tôi đối xử công bằng với họ như những người khác.
		Nếu bạn bị thiếu cân trầm trọng, bạn có thể chọn tham gia một trong những chương trình dinh dưỡng lành mạnh của chúng tôi.',			
	'13DETOXIFICATION'=>'13. Tôi có thể tham gia chương trình thải độc không nếu tôi mang thai?',			
	'13DETOXIFICATION CONTENT'=>'Chương trình chúng tôi cung cấp phù hợp cho phụ nữ mang thai. Bạn có thể đăng ký Chương trình dinh dưỡng lành mạnh, giúp bạn ăn thực phẩm sạch và nhiều dinh dưỡng. Khi bạn đến, quản lý chương trình sẽ chỉ dẫn bạn chọn lựa chương trình phù hợp và lên lịch trình.',			
	'14DETOXIFICATION'=>'14.	Việc cần làm và việc không nên làm trước khi thải độc là gì? Tôi nên ăn và uống gì?',			
	'14DETOXIFICATION CONTENT'=>'
		<p>
			Để có kết quả tốt nhất, trước khi tham gia chương trình thải độc, vui lòng thực hiện Chương trình tiền thải độc trong hai tuần, chương trình sẽ được gửi đến bạn sau khi bạn xác nhận đặt phòng. Điều này sẽ vừa kiềm hóa vừa chuẩn bị cơ thể bạn cho một chương trình thải độc sâu hơn. Nếu bạn không có đủ thời gian, bạn nên bắt đầu tối thiểu 3 ngày trước chương trình thải độc. Để có kết quả tối ưu trong giai đoạn tiền thải độc, không ăn thịt, các sản phẩm từ sữa, muối, đường hoặc thực phẩm có thêm đường.
		</p>
		<p>
			Ngừng uống cà phê, trà, rượu và cái loại đồ uống khác có chứa caffein. Thay vào đó, bạn có thể uống các loại trà thảo mộc. Uống nước càng nhiều càng tốt, nhưng không uống trong bữa ăn. Nước là một bổ sung quan trọng trong quá trình thải độc. Tốt nhất là uống nước ít nhất 6% trọng lượng cơ thể của bạn (ví dụ trọng lượng cơ thể là 70kg, uống 4,2 lít nước).
		</p>
		<p>
			Cắt bỏ caffeine và rượu, ăn nhiều trái cây và rau sống nhất mà cơ thể bạn có thể xử lý được. 
		</p>
	',			
	'15DETOXIFICATION'=>'15. Tại sao tôi cần phải thực hiện tiền thải độc?',			
	'15DETOXIFICATION CONTENT'=>'Tiền thải độc chuẩn bị cơ thể bạn cho quá trình thải độc sâu mà bạn sẽ trải nghiệm khi nhịn ăn hoàn toàn. Trong quá trình tiền nhịn ăn, cơ thể sẽ chuyển từ trạng thái axit sang độ kiềm cần thiết để thải độc. Bạn cũng sẽ tăng lưu trữ chất điện giải, cung cấp cho cơ thể sức mạnh cần thiết để thải độc. Nhịn ăn mà không chuẩn bị đúng cách, cơ thể của bạn có thể loại bỏ độc tố nhanh chóng, do đó, dẫn đến một số khó chịu bao gồm đau đầu, mệt mỏi, nôn mửa, tiêu chảy và chóng mặt.',			
	'16DETOXIFICATION'=>'16. Tôi không thực hiện tiền thải độc thì vẫn có thể bắt đầu thải độc được không?',			
	'16DETOXIFICATION CONTENT'=>'Tiền thải độc rất quan trọng trong việc làm cho quá trình chuyển đổi sang chương trình thải độc dễ dàng hơn. Nếu bạn bắt đầu thải độc mà không thực hiện tiền thải độc trước đó, bạn có thể cảm thấy các “triệu chứng thải độc” mạnh hơn. Các triệu chứng này còn được gọi  “triệu chứng chữa bệnh”, là các phản ứng thông thường mà cơ thể trải nghiệm được trong những ngày đầu thải độc như đau đầu, chóng mặt, cảm giác buồn nôn, có những giấc mơ kỳ lạ… Nếu bạn đến trung tâm mà không thực hiện tiền thải độc, chúng tôi có thể áp dụng chương trình thải độc xanh để giúp bạn làm tiền thải độc tại đây.',			
	'17DETOXIFICATION'=>'17. Tôi nên đem gì theo vật dụng gì?',			
	'17DETOXIFICATION CONTENT'=>'Bạn không cần phải đem theo gì đặc biệt cả, chỉ cần quần áo phù hợp cho các lớp yoga, thiền và tập thể dục. Giày thể thao để đi bộ, kem chống nắng, đồ bơi và một quyển sách hay cho những lúc bạn thư giãn trên bờ biển. Quần áo để luyện tập và đồ bơi phải đủ mặc trong suốt chương trình. (Cho các buổi đi bộ hàng ngày, lớp yoga, tập thể dục, tắm hồ bơi, tắm biển, xông hơi hồng ngoại, xông hơi…).',
	'ContentShare'=>'Nội dung tài liệu chia sẻ',		
	'Posted by'=>'Người đăng',		
	'Document file name'=>'Tên file tài liệu',		
	'Download File'=>'Tải File',		
	'References for customers'=>'(*) Tài liệu tham khảo dành cho khách hàng.',		
	'Download'=>'Tải',		
];
?>