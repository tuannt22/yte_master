<?php

// sentence.php

return [
	'news' => 'News',
  'faq' => 'FAQ',
  'contact' => 'Contact',
  'home' => 'Home',
  'Giới thiệu'=> 'About Us',
  'Dịch vụ' => 'Services',
  'Gói trị liệu' => 'Packages',
  'Đối tác' => 'Partners',
  'Thủ tục' => 'Procedure',
  'Thư viện' => 'Library',
  'Tin tức' => 'News',
  'Hỏi đáp' => 'FAQS',
  'Liên hệ' => 'Contact',
  'PARTNERS' => 'PARTNERS',
  'PROCEDURES' => 'PROCEDURES',
  'Copyright' => '© Copyright by Infinity Healthcare Vietnam Co., Ltd.',
  'ReadMore' => 'Read More',
  'EVERYDAY HEALTHY LIVING' => 'EVERYDAY HEALTHY LIVING',
  'EVERYDAY HEALTHY LIVING CONTENT' => '
    <p>Have you ever heard of stem cell therapy and wanted to understand its therapeutic mechanism and effect?</p>
    <p>Have you ever heard of non-radiotherapy and non-chemotherapy that can still ensure the quality of life in patients with cancer?</p>
    <p>Have you ever concerned about the prevention and screening for rare fatal diseases such as cancer, cerebrovascular accident or stroke?</p>
    <p>Have you ever wanted to receive medical treatment from top doctors around the world and have them deal with your medical problems?</p>
    <p>At Infinity Healthcare Vietnam (IHC), we provide solutions for these issues mentioned above. Our mission is to help millions of Vietnamese people to access to modern medicine and alternative medicine, with view to a healthier and happy life!</p>',
  'EXPERIENCE THE TOP CLASS SERVICES' => 'EXPERIENCE THE TOP CLASS SERVICES',
  'MEDICAL SERVICES' => 'MEDICAL SERVICES',
  'WHY CHOOSE IHC' => 'WHY CHOOSE IHC?',
  'WHY CHOOSE IHC LG' => 'WHY CHOOSE IHC?',
  'COMPREHENSIVE ADVICES' => 'COMPREHENSIVE ADVICES',
  'COMPREHENSIVE ADVICES CONTENT' => 'Clients are provided with sufficient information and analysis of treatment options to choose the best treatment option that is suitable for their pathological characteristics and financial ability.',
  'TOP QUALITY' => 'TOP QUALITY',
  'TOP QUALITY CONTENT' => 'IHC is committed to connecting our clients with top-notch doctors and specialists in each specific health issue of our clients, with view to delivering the best treatment results.',
  'PROFESSIONAL SERVICES' => 'PROFESSIONAL SERVICES',
  'PROFESSIONAL SERVICES CONTENT' =>'IHC is committed to providing each client with the most thoughtful and responsible services, from therapeutic counseling, scheduling to complete delivery of overseas therapeutic services.',
  'PRE-TREATMENT CONSULTANCY' => 'PRE-TREATMENT CONSULTANCY',
  'PRE-TREATMENT CONSULTANCY CONTENT' => 'Medical records consultancy <br>Additional testing  <br>Preliminary diagnosis  <br>Treatment advice',
  'PRE-DEPARTURE PREPARATION' => 'PRE-DEPARTURE PREPARATION',
  'PRE-DEPARTURE PREPARATION CONTENT' => 'Making an appointment <br>Signing a Service contract with IHC<br>Applying for a visa<br>Booking air tickets and hotels',
  'OVERSEAS TREATMENT' => 'OVERSEAS TREATMENT',
  'OVERSEAS TREATMENT CONTENT' =>
    'Comprehensive test 1 <br>
    Treatment <br>
    Comprehensive test 2 <br>
    Result evaluation after treatment',
  'POST-TREATMENT MONITORING' => 'POST-TREATMENT MONITORING',
  'POST-TREATMENT MONITORING CONTENT' =>
    'Result evaluation after 3 months <br>
    Result evaluation after 6 months <br>
    Additional treatment planning',
  'TESTIMONIALS' => 'TESTIMONIALS',
  'TESTIMONIALS LG' => 'TESTIMONIALS',
  'ABOUT US' => 'ABOUT US',
    'BLOG' => 'BLOG',
];
