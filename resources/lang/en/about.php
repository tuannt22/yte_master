<?php 
	return
	[
		'Thông điệp từ CEO' => 'Message from CEO',
		'Tại sao lựa chọn IHC ?'=>'Why choose IHC?',
		'Tầm nhìn - sứ mệnh - giá trị cốt lõi'=>'Vision – Mission – Core Values',
		'Đội ngũ'=>'Our Team',
		'Cảm nhận về khách hàng'=>'Testimonials',
		'COMPREHENSIVE ADVICES CONTENT' => 'Clients are provided with sufficient information and analysis of treatment options to choose the best treatment option that is suitable for their pathological characteristics and financial ability.',
		'TOP-CLASS QUALITY'=>'TOP-CLASS QUALITY',
		'TOP-CLASS QUALITY CONTENT'=>'IHC is committed to connecting our clients with top-notch doctors and specialists in each specific health issue of our clients, with view to delivering the best treatment results.',
		'PROFESSIONALISM'=>'PROFESSIONALISM',
		'PROFESSIONALISM CONTENT'=>'IHC is committed to providing each client with the most mindful and responsible services, from therapeutic counseling, scheduling to complete delivery of overseas therapeutic services; to providing free consultancy relating to client’s health issues during and after treatment.',
		'ABSOLUTE PRIVACY'=>'ABSOLUTE PRIVACY',
		'ABSOLUTE PRIVACY CONTENT'=>'IHC is committed to adhering to strict privacy guidelines on clients personal and health information.',
		'OUR TEAM'=>'OUR TEAM',
		'TESTIMONIALS'=>'TESTIMONIALS',
		'SERVICES'=>'SERVICES',
		'FAQ'=>'FAQ',
		'Therapy package'=>'Therapy package',
		'Therapy package note'=>'(*) Including treatment, accomodation and interpretation fees, excluding airfares',
		'package'=>'Package (Product code)',
		'Location'=>'Location',
		'Contents'=>'Contents',
		'Duration'=>'Duration',
		'Partners'=>'Partners',
		'Procedure'=>'Procedure',
		'SERVICE PROCEDURE'=>'SERVICE PROCEDURE',
		'Procedure_NOTICE'=>'NOTICE',
		'Procedure_NOTICE_CONTENT'=>'								
			<p>
				(1) Contract signing and deposit making
			</p>
			<p>
				To make an appointment and finish the VISA procedure, clients will be required to deposit normally 50% of the treatment expenses.
			</p>
			<p>
				Payments can be made directly with cash, credit card or bank transfer to IHC’s bank
			</p>
			<p>
				(2) Refund policy regarding VISA application result
			</p>
			<p>
				After clients make the required deposit, IHC will support clients to apply for a medical treatment VISA in the country clients choose to take their treatment.
			</p>
			<p>
				If clients are successfully issued with the required VISA: IHC will pay the medical facility all treatment expenses; and treatment expense refunds will adhere to our policies as prescribed in LIBRARY > Legal Conditions.
			</p>
			<p>
				If clients are not issued with the required VISA: IHC will refund the total amount deposited. 
			</p>',
		'ANTI-AGING'=>'ANTI-AGING',
		'1WHAT ANTI-AGING'=>'1.	What is anti-aging medicine?',
		'1WHAT ANTI-AGING CONTENT'=>'
			<p>Anti-aging medicine is an evolving branch of medical science and applied medicine. It treats the underlying causes of aging and aims at alleviating any age-related ailment. Its goal is to extend the healthy lifespan of humans having youthful characteristics.</p>
			<p>Conventional and alternative medical disciplines are used in an integrated approach to achieve the best possible result for the patient. It is a holistic discipline, seeing the patient as a whole and not as someone having an isolated disease.</p>',
		'2WHAT ANTI-AGING'=>'2.	What causes aging?',				
		'2WHAT ANTI-AGING CONTENT'=>'
			<p>Aging is a progressive failure of metabolic processes. There is a concept of pause as put forth by Dr. Eric Braverman indicating that every organ ages at a different rate. To a certain extent it is based on hormones.</p>
			<p>There are three main biochemical processes involved in aging. These are oxidation, glycation and methylation. Other relevant processes are chronic inflammation and hormonal deregulation.</p>',
		'3WHAT ANTI-AGING'=>'3.	Who needs Anti-aging treatment?',					
		'3WHAT ANTI-AGING CONTENT'=>'Everyone needs Anti-aging treatment, especially those over 50. Since the age of 50, our aging starts to speed up and there are often many changes in health, leading to the development of certain diseases such as Diabetes, Blood Pressure, Blood Fat, Cardiovascular, Cancer, etc. One of the main factors in this process is the significant decline in the hormones of the body, from which the quality of life will be gradually decreasing.',
		'4WHAT ANTI-AGING'=>'4.	Why do I need Anti-aging treatment?',					
		'4WHAT ANTI-AGING CONTENT'=>'Owing to anti-aging therapy, hormonal and cellular declines can be adjusted and prevented. Similarly, biological hormone replacement therapy and specific anti-aging formulas developed today can bring about a better quality of life and a longer life span.',
		'5WHAT ANTI-AGING'=>'5.	How often should I take Anti-aging treatment?',		
		'5WHAT ANTI-AGING CONTENT'=>'Depending on your health condition and body intoxication, you can take Anti-aging treatment once or twice a year.',				
		'6WHAT ANTI-AGING'=>'6.	What infusion ingredients are used in the Anti-aging treatment under the IHC therapy program and are there any side-effects?',		
		'6WHAT ANTI-AGING CONTENT'=>'The detoxification and anti-aging infusion ingredients includes natural antioxidant, anti-aging products, vitamins, minerals, amino acids, etc. that cause no side effects.',				
		'7WHAT ANTI-AGING'=>'7.	Can Anti-aging treatment enhance my beauty?',		
		'7WHAT ANTI-AGING CONTENT'=>'This is not a beauty method but a method to help slow down the aging process of the body. You will be improved physically and mentally improvement. Your body will become younger and more vibrant.',				
		'8WHAT ANTI-AGING'=>'8.	Are there contraindications for anti-aging treatment?',		
		'8WHAT ANTI-AGING CONTENT'=>'In general, there are no specific contraindications for anti-aging treatment because the infusion ingredients are natural and have no side effects. However, regarding the supplementation of biological hormones, it is only implemented following a full assessment and in compliance with specific instructions from anti-aging doctor.',								
		'DETOXIFICATION'=>'DETOXIFICATION',	
		'1DETOXIFICATION'=>'1.	Why should I detoxify?',			
		'1DETOXIFICATION CONTENT'=>'In this modern life, we are exposed to polluted air, stress and passive lifestyle, and the consumption of processed foods, additives and many other factors has led to an accumulation of toxins in our body. Therefore, we need to detoxify twice a year in order to get rid of those toxins. We should detoxify in order to: age slower and become more healthy, lose weight, boost our energy, improve our skin, improve our quality of life, enhance our immune system function, prevent chronic diseases and, of course, to have our mental and emotional clarity.',			
		'2DETOXIFICATION'=>'2.	How many days should I do the Program?',			
		'2DETOXIFICATION CONTENT'=>'You can do certain types of detox programs within any period of time that you wish; however, in order to have effective results, we recommend a minimum of 3-4 to 10 days.',			
		'3DETOXIFICATION'=>'3. Will I be able to quit my bad habits (smoking, alcohol, eating snacks, etc.)?',			
		'3DETOXIFICATION CONTENT'=>'The detoxification program considerably reduces the consumption of alcohol and smoking. The detoxification program we offer with the assistance our program managers and other extra therapies that you might have is very effective in helping you get rid of your unhealthy habits.',				
		'4DETOXIFICATION'=>'4.	Are there any contraindications for fasting and colema?',			
		'4DETOXIFICATION CONTENT'=>'Fasting is suitable for everyone under the consent of detoxification experts and following the doctor consultation at our centers.Colema is also very effective and safe for your detoxification process.',			
		'5DETOXIFICATION'=>'5.	I have a certain illness and a certain medication that I use regularly. Can I do a detox?',			
		'5DETOXIFICATION CONTENT'=>'In order for you to give the decision, we would like you to consult our doctor. You may call/send a detailed email regarding your health condition and our doctors will be in touch with you shortly.',			
		'6DETOXIFICATION'=>'6.	Can I continue taking my medication during detoxification?',			
		'6DETOXIFICATION CONTENT'=>'We never recommend that you stop taking medication during detoxification as it can be dangerous. Our highly experienced detox program manager will guide you with the best decisions regarding your health and holistic and dietary recommendations will be made.',			
		'7DETOXIFICATION'=>'7. Can I do a detoxification if I have Diabetes?',			
		'7DETOXIFICATION CONTENT'=>'<p>Yes you can. Type-2 diabetes emerges as a direct consequence of poor diet and unhealthy lifestyle habits, it can be reversed by modifying the diet and other lifestyle components. The duration (between 7-21 days) depends upon the person’s willingness, 7 days is good for balancing hormones and blood sugar values, however at least 14 days is needed for a complete impact. Full fasting or juice fasts can be a wonderful remedy for type-2 diabetes.</p><p>Our highly experienced Detox Program Manager & doctors will help guide you to the best dietary recommendations. We do have 3 different programs for Diabetes Type-2, Pre-Diabetes and Diabetes (Type-1).</p>',			
		'8DETOXIFICATION'=>'8.	Will detoxification help with my skin problems?',			
		'8DETOXIFICATION CONTENT'=>'In holistic health we recommend treating all skin conditions by cleaning the digestive tract and liver, during your stay you will meet with our detox program manager who can make recommendations for you to continue with once you leave the retreat.',			
		'9DETOXIFICATION'=>'9.	What if I have High Blood pressure or High Cholesterol?',			
		'9DETOXIFICATION CONTENT'=>'<p>We have many guests who come to our retreat with high cholesterol and hypertension; in fact, many of them come to fast specifically to help with these issues. Every morning, you will have your blood pressure checked by one of our specialized consultants.</p><p>Although fasting usually will drastically reduce high blood pressure, we never recommend that anybody should stop taking their medication for this condition.</p><p>Detoxification has also been known to indirectly help with issues regarding high cholesterol as the whole process helps to clean the blood and liver, thereby improving the body’s ability to digest and break down fats and lipids. We have experienced and trained staff on hand to help with any questions or concerns you may have regarding these issues as well as extra services to help you plan an individual exercise and dietary routine to bring about greater balance, health and vitality to your life, should you feel a more in-depth analysis is necessary.</p>',			
		'10DETOXIFICATION'=>'10. Can I do a detoxification if I’m taking Antidepressants?',			
		'10DETOXIFICATION CONTENT'=>'Yes, of course. We would never recommend that you stop taking any prescribed medication whilst doing a detoxification, especially antidepressants.',			
		'11DETOXIFICATION'=>'11. Can I join the program if I’m menstruating?',			
		'11DETOXIFICATION CONTENT'=>'It is no problem to participate in a cleansing program if you are menstruating. And any extreme change in your diet can affect your menstrual cycle so don’t worry if your period is irregular or slightly heavier than usual.',			
		'12DETOXIFICATION'=>'12. Can I do detox if I’m skinny?',			
		'12DETOXIFICATION CONTENT'=>'You can still do cleanse if you are skinny as long as you are not currently suffering with an eating disorder or are suffering with malnutrition or certain deficiencies. The main goal of cleansing is detoxification and removal of impacted waste within the colon. We have many skinny, non-overweight people visiting us for fasting and generally they fair just as well as other people.',			
		'13DETOXIFICATION'=>'13. Can I do a detoxification program if I am pregnant?',			
		'13DETOXIFICATION CONTENT'=>'The programs that we offer are suitable for pregnant guests as well. You may apply Healthy Nutritional Programs that would help you to eat clean and nutritional food. When you arrive at our center, the program manager will direct you for picking the suitable program and schedule.',			
		'14DETOXIFICATION'=>'14. What are the Dos and Don’ts before coming? What should I eat & drink?',			
		'14DETOXIFICATION CONTENT'=>'<p>For best results, before joining our detox program, follow the Pre-Detox Program for two weeks that will be sent to you after you confirm your booking. This will both alkalize and prepare your body for a deeper detoxification. If you do not have enough time, you should start a minimum of 3 days before the detox program. For optimum results in the pre-cleansing phase, do not eat meat, dairy products, salt, sugar or foods with added sugar.</p><p>Stop the intake of coffee, tea, alcohol and all other drinks with caffeine. You can drink all sorts of herbal teas instead. Drink water as often as possible, but not with a meal. Water is a critical addition to the cleansing process. It is best to drink at least 6% of your body weight (I.e. 70 kg body weight, drink 4.2 liters of water).</p><p>Cut off caffeine and alcohol, eat as many raw fruits and vegetables as your body can handle.</p>',			
		'15DETOXIFICATION'=>'15. Why do I need Pre-Detox?',			
		'15DETOXIFICATION CONTENT'=>'Pre-detoxification prepares your body for the deep cleansing you will experience on the full fast. On a pre-fast, your body will move from an acidic state towards the alkalinity needed for cleansing. You’ll also increase your storage of electrolytes, giving your body the strength needed for cleansing. Fasting without properly preparing your body can result in rapid toxin elimination which, in turn, can result in some discomfort including headaches, fatigue, vomiting, diarrhea and dizziness.',			
		'16DETOXIFICATION'=>'16. I did not do a pre-detox. Can I still start my detox?',			
		'16DETOXIFICATION CONTENT'=>'The pre-detox is significant in terms of easing the transition to a detox program. If you start your detox without doing the pre-detox beforehand, you might feel the “detox symptoms” stronger. These symptoms are also known  as the “healing symptoms” which are general reactions that the body can experience in the first couple of days of the detox such as headaches, dizziness, feeling nauseous, having strange dreams etc. If you arrive at the center without doing the pre-detox, we can apply the green detox programs to help you to do your pre-detox here.',			
		'17DETOXIFICATION'=>'17. What should I bring with me?',			
		'17DETOXIFICATION CONTENT'=>'There is nothing specific you need to bring, just comfortable clothing for yoga, meditation and exercise classes. Some sports shoes (trainers) for walking, sun cream, bikini and a good book for when you are chilling out on the beach. Sportswear and swimsuit would be sufficient to wear during your program. (For the daily walks, yoga classes, use of the fitness room, daily pool/sea/infrared sauna/steam room usages etc.)',
		'ContentShare'=>'Content of shared documents',
		'Posted by'=>'Posted by',		
		'Document file name'=>'Document file name',
		'Download File'=>'Download File',		
		'References for customers'=>'(*) References for customers.',		
		'Download'=>'Download',		
	];
