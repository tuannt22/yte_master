(function ($)
 {

    $.fn.menumaker = function (options) {

        var cssmenu = $(this),
            settings = $.extend({
                title: "",
                format: "dropdown",
                sticky: false
            }, options);

        return this.each(function () {
            cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
            $(this).find("#menu-button").on('click', function () {

                var mainmenu = $(this).next('ul');
                if (mainmenu.hasClass('open')) {
                    mainmenu.hide().removeClass('open');
                    $("#menu-button").removeClass('menu-opened');
                } else {
                    console.log("vao");
                    mainmenu.show().addClass('open');
                    $("#menu-button").addClass('menu-opened');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });

            cssmenu.find('li ul').parent().addClass('has-sub');

            multiTg = function () {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function () {

                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide();
                        $(this).removeClass('submenu-opened');
                    } else {
                        $(this).siblings('ul').addClass('open').show();
                        $(this).addClass('submenu-opened');
                    }
                });
                cssmenu.find('.has-sub a').on('click', function () {
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide();
                    } else {
                        $(this).siblings('ul').addClass('open').show();
                    }
                });
            };

            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');

            if (settings.sticky === true) cssmenu.css('position', 'fixed');

            resizeFix = function () {
                if ($(window).width() > 767) {
                    cssmenu.find('ul').show();
                }

                if ($(window).width() < 767) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);

        });
    };
})(jQuery);

(function ($) {
    $(document).ready(function () {

            $("#cssmenu").menumaker({
                title: "",
                format: "multitoggle"
            });

    });
})(jQuery);