// var assConfig = {
//   duration: 500,
//   easing: 'swing',
//   complete: null
// };

//Smooth scrolling with links
$('a[href*=\\#]:not([href$=\\#])').on('click', function (event) {
  var hash;
  if (event.target.pathname !== window.location.pathname || event.target.hostname !== window.location.hostname || event.target.protocol !== window.location.protocol || event.target.search !== window.location.search) {
      return;
  }
  event.preventDefault();
  hash = '#' + $(event.target).attr("href").split("#");
  if (typeof $(hash).offset() !== 'undefined') {
      $('html,body').animate({
          scrollTop: $(this.hash).offset().top
      }, 100);
  } else {
      console.log("Note: Scrolling to the anchor ist not possible as the anchor does not exist.");
  }
});

// Smooth scrolling when the document is loaded and ready

$(document).ready(function(){
  var pathname = window.location.pathname;
  $('ul > li > a[href="' + pathname + '"]').parent().addClass('active');
  // $('ul li a').click(function(){
  //   $('li a').removeClass("active");
  //   $(this).addClass("active");
  // });

  $(document).on('click', '#cssmenu a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 120 //Add this
    }, 100);
  });
  if (typeof $(location.hash).offset() !== 'undefined') {
    console.log(location.hash)
    $('html,body').animate({
        scrollTop: $(location.hash).offset().top
    }, 100);
  } else {
      console.log("Note: Scrolling to the anchor ist not possible as the anchor does not exist.");
  }
  $(window).scroll(function () {
    if ($(window).scrollTop() >= 600) {
      $("#header").addClass("fixed-menu-header fadeInDown animated");
      $(".img-logo-color").css("display","block");
      $(".img-logo").css("display","none");
      $(".tel").css("display","none");
      $(".tel1").css("display","block");
    } else {
      $("#header").removeClass("fixed-menu-header fadeInDown animated");
      $(".img-logo-color").css("display","none");
      $(".img-logo").css("display","block");
      $(".tel1").css("display","none");
      $(".tel").css("display","block");
    }
  });
  //slider
  $('.sliderbar').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    items: 1, //10 items above 1000px browser width
    autoplayTimeout: 7000,
    autoplayHoverPause: false,
    autoplay: false,
    dots: true,
    nav: true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
  });
  // owl
  $('.custom1').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    items: 1, //10 items above 1000px browser width
    autoplayTimeout: 7000,
    autoplayHoverPause: false,
    autoplay: false,
    dots: true,
    nav: true,
    navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
  });

  
  jQuery(function($) {
  
  // Function which adds the 'animated' class to any '.animatable' in view
  var doAnimations = function() {
    
    // Calc current offset and get all animatables
    var offset = $(window).scrollTop() + $(window).height(),
      $animatables = $('.animatable');
    
    // Unbind scroll handler if we have no animatables
    if ($animatables.length == 0) {
    $(window).off('scroll', doAnimations);
    }
    
    // Check all animatables and animate them if necessary
      $animatables.each(function(i) {
    var $animatable = $(this);
        if (($animatable.offset().top + $animatable.height() - 20) < offset) {
      $animatable.removeClass('animatable').addClass('animated');
        }
    });

    };
  
  // Hook doAnimations on scroll, and trigger a scroll
    $(window).on('scroll', doAnimations);
      $(window).trigger('scroll');
    });

    $("#back-to-top").click(function(){return $("body, html").animate({scrollTop:0},400),!1});
    $(function(){$('[data-toggle="tooltip"]').tooltip()});
    $("#sidebar a").click(function() {
      $(this).find(".fas").toggleClass("fa-minus");
    });



 
    // $(".title-footer").click(function() {
    //   if($(window).width() < 767){
    //     $(this).next().slideToggle();
    //     $(this).find(".fa").toggleClass("fa-angle-up fa-angle-down")
    //   }
    // }); 
    
    if($(window).width() < 767){
      $(".section-2-list").addClass("custom2 owl-carousel owl-theme");
      $(".section-2-list div").removeClass("col-md-4 col-sm-4");
      $(".list-owl-partner").addClass("custom3 owl-carousel owl-theme");
      $(".list-owl-partner div").removeClass("col-md-4 col-sm-4");
      // $(".title-footer").append("<i class='fa fa-angle-up'></i>");
      
      $(".list-dn-mobile").addClass("custom4 owl-carousel owl-theme");
      $(".list-dn-mobile div").removeClass("col-md-4 col-sm-6");
      $(".doi-ngu").addClass("row");
      // list-cus
      $(".list-cus").addClass("custom5 owl-carousel owl-theme");
      $(".list-cus div").removeClass("col-md-4 col-sm-4");
      $("#cam-nhan").addClass("row");
      // $("#cssmenu ul li a").click(function(){
      //   $("#cssmenu ul li ul").addClass("animatable bounceInRight");
      // })
    }
    
    if($(window).width() <= 1024){
      $(".mobile-a").removeAttr("href");
      $("#menu-button").removeClass("menu-opened")
    }
  $('.custom2').owlCarousel({
    center: true,
    items:2,
    merge:true,
    autoWidth:true,
    loop:true,
    margin:10,
    responsive:{
        600:{
            items:4
        }
    },
    dots: false,
    nav: false,
});
   // owl
   $('.custom3').owlCarousel({
    center: true,
    items:2,
    merge:true,
    autoWidth:true,
    loop:true,
    margin:10,
    responsive:{
        600:{
            items:4
        }
    },
    dots: false,
    nav: false,
});
 // owl
 $('.custom4').owlCarousel({
  center: true,
  items:2,
  merge:true,
  autoWidth:true,
  loop:true,
  margin:10,
  responsive:{
      600:{
          items:4
      }
  },
  dots: false,
  nav: false,
});
 // owl
 $('.custom5').owlCarousel({
  center: true,
  items:2,
  merge:true,
  autoWidth:true,
  loop:true,
  margin:10,
  responsive:{
      600:{
          items:4
      }
  },
  dots: false,
  nav: false,
});



});

