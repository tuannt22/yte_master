  $(window).scroll(function () {   
      function scrollNav() {
         $('.nav a').click(function(){  
         //Toggle Class
         $(".active").removeClass("active");      
         $(this).closest('li a').addClass("active");
         var theClass = $(this).attr("class");
         $('.'+theClass).parent('li a').addClass('active');
         //Animate
         $('html, body').stop().animate({
               scrollTop: $( $.attr(this, 'href') ).offset().top - 120
         }, 100);
         return false;
         });
         $('.scrollTop a').scrollTop();
      }
  
      scrollNav();

      if($(window).scrollTop() > 900) {
         if($(window).width() <= 1024){
            $('#sidebar').css({'position':'fixed','top':'115px','width':'241px'});
         }else{
            $('#sidebar').css({'position':'fixed','top':'115px','width':'292.5px'});
         }
      }else if ($(window).scrollTop() <= 600) {
         $('#sidebar').css('position','');
         $('#sidebar').css('top','');
      }if ($('#sidebar').offset().top + $("#sidebar").height() > $("#footer").offset().top) {
            $('#sidebar').css('top',-($("#sidebar").offset().top + $("#sidebar").height() - $("#footer").offset().top));
      }
  });